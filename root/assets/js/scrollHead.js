document.addEventListener("DOMContentLoaded", function(event) {

    //setTimeout(function () {
        var motto = document.getElementById("legend");
        var mottocontainer = document.getElementById("devise");
        motto.style.top = window.innerHeight - 125 +"px";
    //},2000);

    var header = document.getElementById('header');
    var searchBar = document.getElementById("recherche");
    var replaceSearch = document.getElementById("searchreplacement");
    var initialTop = searchBar.offsetTop-header.offsetHeight;
    var initialHeight = searchBar.offsetHeight;
    replaceSearch.style.display = "none";

    searchBar.style.boxShadow = "0px -2px 10px #000";

    var carousel = document.getElementById("devise");
    onscr = function () {
        carousel.style.position = "relative";
        carousel.style.top = window.scrollY + "px";
        carousel.style.zIndex = "-5";
        if (window.scrollY > 0) {
            header.style.borderBottom = "1px solid black";
            header.style.background = "white";
            header.style.color = "#922190";
            header.className = "blacklink";
            header.style.boxShadow = "0px 3px 10px #000";
            if (window.scrollY >= searchBar.offsetTop-header.offsetHeight && replaceSearch.style.display == "none") {
                initialTop = searchBar.offsetTop-header.offsetHeight;
                replaceSearch.style.display = "block";
                replaceSearch.style.height = initialHeight+"px";
                searchBar.style.position = "fixed";
                searchBar.style.top = header.offsetHeight + "px";
                searchBar.style.width = "100%";
                searchBar.style.background = "white";
                searchBar.style.boxShadow = "0px 2px 5px #000";
            } else if (replaceSearch.style.display == "block") {
                if (window.scrollY < initialTop) {
                    searchBar.style.position = "relative";
                    searchBar.style.top = "inherit";
                    searchBar.style.width = "inherit";
                    searchBar.style.background = "none";
                    searchBar.style.boxShadow = "0px -2px 10px #000";
                    replaceSearch.style.display = "none";
                    replaceSearch.style.height = 0;
                }
            }

        } else {
            header.style.borderBottom = "none";
            header.style.background = "none";
            header.style.color = "white";
            header.className = "";
            header.style.boxShadow = "0px 0px 0px #000";
        }
    };


    window.onscroll = onscr;
});