var imagePreview = {
    /**
     * Permet de créer une image cliquable pour l'upload d'image
     * @param fileInputId   Id de l'input[type=file]
     * @param imageId       Id de l'image de preview
     * @param buttonId      Id du bouton de remplacement pour l'upload facultatif
     * @param initSrc       source initiale de l'image facultative
     */
    init: function(fileInputId, imageId, buttonId, initSrc, callback) {
        var fileInput = document.getElementById(fileInputId);
        var image = document.getElementById(imageId);
        var initSrc = (typeof initSrc === "undefined") ? "" : initSrc;
        fileInput.style.display = "none";
        if (initSrc == "") {
            image.style.display = "none";
        } else {
            image.src = initSrc;
        }
        image.onclick = function () {
            imagePreview.triggerEvent(fileInput);
        };
        if (typeof buttonId !== "undefined" && buttonId != "") {
            var button = document.getElementById(buttonId);
            button.onclick = function () {
                imagePreview.triggerEvent(fileInput);
            };
        }
        fileInput.onchange = function (evt) {
            var file = evt.target.files[0];
            if (file == undefined) {
                image.style.display = "none";
            } else {
                image.src = URL.createObjectURL(file);
                image.style.display = "block";
                if (typeof callback !== "undefined" && callback != "") {
                    callback();
                }
            }
        };
    },
    triggerEvent: function (fileInput) {
        var evt = new MouseEvent("click");
        fileInput.dispatchEvent(evt);
    }
};