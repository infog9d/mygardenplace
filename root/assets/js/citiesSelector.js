var btnAddCity = document.getElementById("btnAddCity");
var citiesList = document.querySelector("#selectedCities ol");
var inputAddCity = document.getElementById("addCity");
var inputCities = document.getElementById("cities");
var selectedCities = [];
btnAddCity.onclick = function (evt) {
    evt.preventDefault();
    if (typeof inputAddCity.dataset.id !== "undefined") {
        if (selectedCities.indexOf(inputAddCity.dataset.id) == -1) {
            selectedCities.push(inputAddCity.dataset.id);
            if (inputCities.value != "") {
                inputCities.value += "," + inputAddCity.dataset.id;
            } else {
                inputCities.value = inputAddCity.dataset.id;
            }
            var element = document.createElement("li");
            element.innerHTML = inputAddCity.value;
            var btnRm = document.createElement("a");
            btnRm.className = "button add rm";
            btnRm.innerHTML = "-";
            btnRm.dataset.id = inputAddCity.dataset.id;
            btnRm.onclick = function (evt) {
                evt.preventDefault();
                var target = evt.target || evt.srcElement;
                citiesList.removeChild(target.parentNode);
                selectedCities.splice(selectedCities.indexOf(target.dataset.id),1);//remove index in selectedCities
                inputCities.value = "";
                for (var ij = 0; ij < selectedCities.length; ij++) {
                    if (inputCities.value != "") {
                        inputCities.value += "," + selectedCities[ij];
                    } else {
                        inputCities.value = selectedCities[ij];
                    }
                }
            };
            element.appendChild(btnRm);
            citiesList.appendChild(element);
            inputAddCity.value = "";
        } else {
            alert("Vous vous déplacez déjà dans cette ville");
        }
    } else {
        alert("Nous ne parvenons pas à trouver cette ville");
    }
};
