var gm = {
    map: {},
    cities: ["Paris","Issy-les-moulineaux"],
    glatlng: [],
    markers: [],
    element:"",
    geocoder: new google.maps.Geocoder(),
    init: function (id) {
        gm.element = document.getElementById(id);
        var map = new google.maps.Map(gm.element);

        var latlngbounds = new google.maps.LatLngBounds();

        for (var i=0;i<gm.cities.length;i++) {
            gm.geocoder.geocode( { "address":gm.cities[i]  }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                    var location = results[0].geometry.location;
                    latlngbounds.extend(location);
                    var marker = new google.maps.Marker({
                        position: location,
                        map: map
                    });
                    gm.markers.push(marker);
                    map.fitBounds(latlngbounds);
                }else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }
        google.maps.event.addListener(map, 'zoom_changed', function() {
            zoomChangeBoundsListener =
                google.maps.event.addListener(map, 'bounds_changed', function(event) {
                    if (this.getZoom() > 15 && this.initialZoom == true) {
                        this.setZoom(15);
                        this.initialZoom = false;
                    }
                    google.maps.event.removeListener(zoomChangeBoundsListener);
                });
        });
        map.initialZoom = true;
        gm.map = map;
    }
};

