/**
 * Created by jb on 02/05/15.
 */
document.addEventListener("DOMContentLoaded", function(event) {
    var tiles = document.getElementsByClassName("offer");
    for(var i=0;i<tiles.length;i++) {
        var tile = tiles[i];
        tile.onclick = function (evt) {
            var element = evt.target || evt.srcElement;
            var id= utils.getOfferId(element);
            window.location = viewOffer+id;
        }
    }
});