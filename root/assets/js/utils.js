/**
 * Created by jb on 11/05/15.
 */
var utils = {
    getOfferId: function (element) {
        if (typeof element.dataset["id"] !== "undefined") {
            return element.dataset["id"];
        } else {
            return utils.getOfferId(element.parentNode);
        }
    }
};