var elements = document.getElementsByClassName("openfaq");

for(var ifaq = 0; ifaq < elements.length; ifaq++) {
    elements[ifaq].onclick = function (evt) {
        evt.preventDefault();
        var element = evt.target||evt.srcElement;
        var contenu = document.getElementById("faq-"+element.dataset.id);
        if (contenu.style.opacity == 0) {
            //contenu.style.height = "initial";
            contenu.style.display = "block";
            setTimeout(function () {
                contenu.style.opacity = 1;
                contenu.style.transform = "none";
            },10);
        } else {
            contenu.style.opacity = 0;
            contenu.style.transform = "scale3d(0, 0, 0)";
            setTimeout(function () {
                contenu.style.display = "none";
            }, 400);
            //contenu.style.height = 0+"px";
        }
    };

}