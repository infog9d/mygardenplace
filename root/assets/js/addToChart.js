/**
 * Created by Patrick on 15/05/2015.
 */
var urls = {
    redirectToShoppingChart: "",
    addToShoppingChart: "",
    checkExchange: ""
};


var onclk = function (evt) {
    var element = evt.target || evt.srcElement;
    var id = element.dataset.id;
    modal.show("modaladdcart");
    var onSuccess = function (data) {
        var object = JSON.parse(data);
        console.log(object);
        var li = document.getElementById("listexchange");
        var validate = document.getElementById("add");
        var quantity = object["quantity"];

        if (object['is-exchange']) {
            console.log("tes");
            for (var j= 0;j<object["exchange"].length;j++) {
                var element = document.createElement("option");
                var inputExchange = document.getElementById("qtityExchange");
                inputExchange.max = quantity;
                console.log(object["exchange"][j]);
                element.value = object["exchange"][j]["id"];
                element.innerHTML = object["exchange"][j]["name"];
                li.appendChild(element);
            }
            var exchangeView = document.getElementById("exchangeView");
            exchangeView.style.display = "table-cell";

            /** AJOUT AU PANIER **/

            validate.dataset.id = id;
        } else {
            var exchangeView = document.getElementById("exchangeView");
            exchangeView.style.display = "none";
        }
        if (object["is-sold"]) {
            var input = document.getElementById("qtity");
            var price = document.getElementById("price");
            var priceByKilos = object.price;
            price.innerHTML = input.value*priceByKilos;
            input.max = quantity;
            input.onkeyup = function (evt) {
                price.innerHTML = input.value*priceByKilos;
            };
            input.onchange = function(evt) {
                price.innerHTML = input.value*priceByKilos;
            }
            var priceView = document.getElementById("priceView");
            priceView.style.display = "table-cell";


            validate.dataset.id = id;
        }else {
            var priceView = document.getElementById("priceView");
            priceView.style.display = "none";

        }

        var onSuccessAddToShoppingChart = function(data) {
            if (data=="ok") {
                window.location = urls.redirectToShoppingChart;
            } else if (data=="problem(3)") {
                alert("Vous devez vous connectez pour ajouter un produit à votre panier.");
                modal.show("connection");
            } else if(data == "problem (4)") {
                alert("La quantité que vous avez enseigné doit être inférieure à la quanité totale disponible.");
            } else {
                alert("Veuillez verifier la quantité que vous avez renseigné.");
            }
        };

        validate.onclick=function(event){
            var element = evt.target || evt.srcElement;
            var id = element.dataset.id;
            var li = document.getElementById("listexchange");
            var inputExchange = document.getElementById("qtityExchange");
            var inputBuy = document.getElementById("qtity");
            if (inputBuy.value > 0) {
                console.log("Achat");
                api.post(urls.addToShoppingChart,{id:id,quantity:inputBuy.value,exchange:[]},onSuccessAddToShoppingChart);
            } else {
                console.log("Echange");
                api.post(urls.addToShoppingChart,{id:id,quantity:inputExchange.value,exchange:li.value},onSuccessAddToShoppingChart);
            }
        }

    };
    api.get(urls.checkExchange+"/" + id, {}, onSuccess);

};