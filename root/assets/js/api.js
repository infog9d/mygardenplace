var api = {
    /**
     * It send an ajax request
     * @param type          POST|GET according to the method
     * @param url           send the request to this url
     * @param parameters    parameters object
     * @param onSuccess     callback function
     */
    xhr: function (type, url, parameters, onSuccess) {
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                onSuccess(xmlhttp.responseText);
            }
        };

        if (type == "GET") {
            url += api.convertParametersToString(type,parameters);
        }
        xmlhttp.open(type, url, true);

        if (type == "POST") {
            xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xmlhttp.send(api.convertParametersToString(type,parameters));
        } else {
            xmlhttp.send();
        }
    },
    /**
     * It send a GET request
     * @param url
     * @param parameters
     * @param onSuccess
     */
    get: function(url, parameters, onSuccess) {
        api.xhr("GET", url, parameters, onSuccess);
    },
    /**
     * It send a POST request
     * @param url
     * @param parameters
     * @param onSuccess
     */
    post: function(url, parameters, onSuccess) {
        api.xhr("POST", url, parameters, onSuccess);
    },
    /**
     * It convert a parameters object to a string
     * @param type
     * @param parameters
     * @returns {string}
     */
    convertParametersToString: function(type, parameters) {
        var result = "";
        var parameterKey;
        var firstIterate = true;
        for (parameterKey in parameters) {
            if (parameters.hasOwnProperty(parameterKey)) {
                if (type == "GET" && firstIterate) {
                    result += "?"+parameterKey+"="+parameters[parameterKey];
                } else if (type == "POST" && firstIterate) {
                    result += parameterKey+"="+parameters[parameterKey];
                } else {
                    result += "&"+parameterKey+"="+parameters[parameterKey];
                }
                firstIterate = false;
            }
        }
        return result;
    }
};
