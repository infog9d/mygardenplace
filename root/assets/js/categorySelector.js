var btnAddCategory = document.getElementById("btnAddCategory");
var categoryList = document.querySelector("#selectedCategories ol");
var inputAddCategory = document.getElementById("addCategory");
var inputQttCategory = document.getElementById("switchQuantity");
var inputCategory = document.getElementById("categories");
var selectedCategories = [];
var qtt = [];
btnAddCategory.onclick = function (evt) {
    evt.preventDefault();
    if (typeof inputAddCategory.dataset.id !== "undefined") {
        if (selectedCategories.indexOf(inputAddCategory.dataset.id) == -1) {
            if (inputQttCategory.value != "") {
                selectedCategories.push(inputAddCategory.dataset.id);
                qtt[inputAddCategory.dataset.id] = inputQttCategory.value;
                if (inputCategory.value != "") {
                    inputCategory.value += "," + inputAddCategory.dataset.id + ":" + inputQttCategory.value;
                } else {
                    inputCategory.value = inputAddCategory.dataset.id + ":" + inputQttCategory.value;
                }
                var element = document.createElement("li");
                element.innerHTML = inputAddCategory.value + "(1kg = "+ inputQttCategory.value +"kg)";
                var btnRm = document.createElement("a");
                btnRm.className = "button add rm";
                btnRm.innerHTML = "-";
                btnRm.dataset.id = inputAddCategory.dataset.id;
                btnRm.onclick = function (evt) {
                    evt.preventDefault();
                    var target = evt.target || evt.srcElement;
                    categoryList.removeChild(target.parentNode);
                    selectedCategories.splice(selectedCategories.indexOf(target.dataset.id), 1);//remove index in selectedCategories
                    inputCategory.value = "";
                    for (var ij = 0; ij < selectedCategories.length; ij++) {
                        if (inputCategory.value != "") {
                            inputCategory.value += "," + selectedCategories[ij] + ":" + qtt[selectedCategories[ij]];
                        } else {
                            inputCategory.value = selectedCategories[ij] + ":" + qtt[selectedCategories[ij]];
                        }
                    }
                };
                element.appendChild(btnRm);
                categoryList.appendChild(element);
                inputAddCategory.value = "";
                inputQttCategory.value = "";
            } else {
                alert("Vous devez entrez un équivalent en quantité pour l'echange")
            }
        } else {
            alert("Vous echangez déjà contre ce fruit ou légume");
        }
    } else {
        alert("Nous ne parvenons pas à trouver ce fruit ou légume");
    }
};

