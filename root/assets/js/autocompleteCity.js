var autocompleteCity = {
    init: function (idInput,url) {
        var input = document.getElementById(idInput);

        /** corrige un bug sous chrome **/
        if (navigator.userAgent.toLowerCase().indexOf('chrome') >= 0) {
            setTimeout(function () {
                console.log("fiwed ?");
                input.autocomplete = "off";
            }, 500);
        }
        /** ************************* **/
        var current = 0;
        var max = 0;

        var list;
        var trueList;

        input.onkeydown = function (evt) {
            var keyCode = ('which' in evt) ? evt.which : evt.keyCode;
            if (keyCode == 13) {
                //enter
                evt.preventDefault();//prevent submission
                var elemAuto = document.getElementById('option'+current);
                input.value = elemAuto.innerHTML;
                input.dataset.id = elemAuto.dataset.id;
                var triggeredEvt = new MouseEvent("focusout");
                input.dispatchEvent(triggeredEvt);
                current = 0;
                return false;//prevent submission
            } else if (keyCode == 39) {
                //right
                var elemAuto = document.getElementById('option'+current);
                input.value = elemAuto.innerHTML;
                input.dataset.id = elemAuto.dataset.id;
            } else if (keyCode == 38) {
                //up
                var elemAuto = document.getElementById('option'+current);
                elemAuto.style.background = "#fff";
                if (current !=0) {
                    current--;
                    list.scrollTop -= elemAuto.offsetHeight;
                } else {
                    current = max;
                    list.scrollTop = trueList.offsetHeight;
                }
                var elemAuto = document.getElementById('option'+current);
                elemAuto.style.background = "#ddd";


            } else if (keyCode == 40) {
                //down
                var elemAuto = document.getElementById('option'+current);
                elemAuto.style.background = "#fff";
                if (current != max) {
                    current++;
                    list.scrollTop += elemAuto.offsetHeight;
                } else {
                    current = 0;
                    list.scrollTop = 0;
                }
                var elemAuto = document.getElementById('option'+current);
                elemAuto.style.background = "#ddd";

            } else {
                var moreorless = ((keyCode == 8 || keyCode == 46)?-1:1); //back or suppr
                if (input.value.length+moreorless > 2) {
                    current = 0;
                    max = 0;
                    if (document.getElementById("autocomplete")) {
                        document.body.removeChild(document.getElementById("autocomplete"));
                    }
                    list = document.createElement("div");
                    list.className = "listAutocomplete";
                    list.style.zIndex = 1000;
                    list.style.maxHeight = 200 + "px";
                    list.style.overflow = "auto";
                    list.style.background = "#fff";
                    if (input.offsetParent.style.position == "fixed") {
                        list.style.position = "fixed";
                    } else {
                        list.style.position = "absolute";
                    }
                    list.style.top = input.offsetTop + input.offsetParent.offsetTop + input.offsetHeight + "px";
                    list.style.left = input.offsetLeft + input.offsetParent.offsetLeft + "px";
                    list.id = "autocomplete";
                    trueList = document.createElement("ol");
                    trueList.style.listStyle = "none";
                    trueList.style.padding = 0;
                    trueList.style.margin = 0;
                    var render = function (data) {
                        var cities = JSON.parse(data);
                        for (var iCity = 0; iCity < cities.length; iCity++) {
                            var elem = document.createElement("li");
                            elem.style.display = "block";
                            elem.style.padding = "5px";
                            elem.style.borderTop = "1px solid #dfdfdf";
                            elem.style.cursor = "pointer";
                            if (iCity == 0) {
                                elem.style.borderTop = "none";
                                elem.style.background = "#ddd";
                            }
                            elem.id = "option"+iCity;
                            elem.dataset.i = iCity;
                            elem.dataset.id = cities[iCity].idCity||cities[iCity].id;
                            elem.innerHTML = cities[iCity].name;
                            elem.onmousedown = function (evt) {
                                var target = evt.target || evt.srcElement;
                                var elemAuto = document.getElementById('option'+current);
                                elemAuto.style.background = "#fff";
                                target.style.background = "#ddd";
                                input.dataset.id = target.dataset.id;
                                input.value = target.innerHTML;
                                if (document.getElementById("autocomplete")) {
                                    document.body.removeChild(document.getElementById("autocomplete"));
                                }
                                current = 0;
                            };
                            elem.onmousemove = function (evt) {
                                var target = evt.target || evt.srcElement;
                                var elemAuto = document.getElementById('option'+current);
                                elemAuto.style.background = "#fff";
                                current = target.dataset.i;
                                target.style.background = "#ddd";
                            };
                            trueList.appendChild(elem);
                        }
                        max = cities.length-1;
                    };
                    var str = (keyCode == 8 || keyCode == 46)?"":(keyCode == 54)? "-" :(keyCode >= 96)?"":String.fromCharCode(keyCode);
                    api.get(url + "/" + input.value+str, {}, render);
                    list.appendChild(trueList);
                    document.body.appendChild(list);
                }
            }
        };

        var focusOut = function () {
            if (document.getElementById("autocomplete")) {
                document.body.removeChild(document.getElementById("autocomplete"));
            }
        };
        input.onfocusout = focusOut();
        input.addEventListener("focusout",focusOut);
    }
};
