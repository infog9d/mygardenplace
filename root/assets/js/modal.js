var modal = {
    init: function(id) {
        var html = document.getElementById(id);
        html.style.display = "none";
    },
    show: function(id) {
        var overlay = document.getElementById('overlay');
        var html = document.getElementById(id);
        html.style.display = "block";
        html.style.marginLeft = -html.offsetWidth/2 + "px";
        html.style.marginTop = window.innerHeight/2-html.offsetHeight/2 + "px";
        overlay.style.display = "block";
        overlay.onclick = function() { modal.hide(id) };
    },
    hide: function(id) {
        var overlay = document.getElementById('overlay');
        var html = document.getElementById(id);
        html.style.marginTop = -500+"px";
        setTimeout(function () {
            html.style.display = "none";
            overlay.style.display = "none";
        },400);
    },
    hideAll: function() {
        var overlay = document.getElementById('overlay');
        var modals = document.getElementsByClassName("modal");
        for (var i = 0; i < modals.length; i++) {
            modals[i].style.marginTop = -500+"px";
            modals[i].style.display = "none";
        }
        overlay.style.display = "none";
    },
    toggle: function(id) {
        var overlay = document.getElementById('overlay');
        var html = document.getElementById(id);
        if (html.style.display == "none") {
            modal.show(id);
        } else {
            modal.hide(id);
        }
    }
};
