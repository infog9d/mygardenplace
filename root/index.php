<?php
define("WEBROOT",dirname(__FILE__));
define("ROOT",dirname(WEBROOT));
define("DS", DIRECTORY_SEPARATOR);
define("BASE_URL",(dirname(dirname($_SERVER["SCRIPT_NAME"]))=="/")?"":dirname(dirname($_SERVER["SCRIPT_NAME"])));

require_once "../core/Requires.php";

$request = new Request();
new Router($request);
