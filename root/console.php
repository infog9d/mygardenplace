<?php
define("WEBROOT",dirname(__FILE__));
define("ROOT",dirname(WEBROOT));
define("DS", DIRECTORY_SEPARATOR);
define("BASE_URL", "http://mygardenplace.iseplive.fr");

require_once ROOT."/core/Requires.php";


if (isset($argv[1])&&$argv[1]=="send:alerts") {
    require_once ROOT."/services/Alerts.php";
    $alertsManager = new Alerts();
    $alertsManager->send();
}