<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 03/05/15
 * Time: 11:31
 */

class Alerts extends Service {

    /** @var null|user */
    private $userManager = null;
    /** @var null|Offers */
    private $offerManager = null;

    public function __construct() {
        $this->userManager = $this->loadModel("user");
        $this->offerManager = $this->loadModel("Offers");
        $this->loadService("Mailer");
    }

    private function getFormated($bySomething) {
        $offers = array();
        foreach ($bySomething as $offer) {
            $offers[] = $this->offerManager->getFormated($offer["id"]);
        }
        return $offers;
    }
    
    public function send() {
        $users = $this->userManager->listAlertsByUser();
        $date = new DateTime();
        $format = $date->format("Y-m-d");
        $today = DateTime::createFromFormat("Y-m-d", $format);
        echo "[$format] Alerts mail sent to : \n";
        foreach ($users as $user) {
            $alertsKeywords = $user["alerts"];
            $offersByKeyWords = array();
            $typesByKeywords = array();
            $needToBeSend = false;
            foreach ($alertsKeywords as $keyword) {
                $keyword = $keyword["keyword"];
                $byGardener = $this->offerManager->getByGardenerSince($keyword,$today);
                if (count($byGardener)==0) {
                    $bySubCategories = $this->offerManager->getBySubCategorySince($keyword,$today);
                    if (count($bySubCategories)==0) {
                        $byCategories = $this->offerManager->getByCategorySince($keyword,$today);
                        if (count($byCategories)==0) {
                            $byKeywords = $this->offerManager->getByKeywordSince($keyword,$today);
                            if (count($byKeywords)==0) {
                                $offersByKeyWords[$keyword] = array();
                                $typesByKeywords[$keyword] = "null";
                            } else {
                                $offersByKeyWords[$keyword] = $this->getFormated($byKeywords);
                                $typesByKeywords[$keyword] = "keyword";
                                $needToBeSend = true;
                            }
                        } else {
                            $offersByKeyWords[$keyword] = $this->getFormated($byCategories);
                            $typesByKeywords[$keyword] = "categories";
                            $needToBeSend = true;
                        }
                    } else {
                        $offersByKeyWords[$keyword] = $this->getFormated($bySubCategories);
                        $typesByKeywords[$keyword] = "subcategories";
                        $needToBeSend = true;
                    }
                } else {
                    $offersByKeyWords[$keyword] = $this->getFormated($byGardener);
                    $typesByKeywords[$keyword] = "gardener";
                    $needToBeSend = true;
                }
            }
            if ($needToBeSend) {
                $mailer = new Mailer($user["mail"], Config::$from);
                $mailer->setView("alerts", array(
                    "offersByKeyWords" => $offersByKeyWords,
                    "typesByKeyword" => $typesByKeywords
                ));
                $mailer->send("MyGardenPlace - De nouveaux produits correspondent à votre recherche");
                echo $user["firstname"]." ".$user["lastname"]." <".$user["mail"]."> \n";
            }
        }
        echo "[$format] End processing \n";
    }

}