<?php


class ImageUploader extends Uploader {

    protected function _isValid()
    {
        $format = $this->getFormat();
        $size = getimagesize($_FILES[$this->varName]["tmp_name"]);
        return in_array($format,$this->allowedFormats) && $size[0] <= Config::$maxWidth && $size[1] <= Config::$maxHeight;
    }
}