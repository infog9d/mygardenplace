<?php


abstract class Uploader extends Service {

    protected $allowedFormats = array( 'jpg' , 'jpeg' , 'gif' , 'png' );

    protected $varName;
    protected $dest;

    public function __construct($varName, $destination = ""){
        $this->varName = $varName;
        if ($destination == "") {
            $destination = WEBROOT.DS."assets".DS."uploads";
        }
        $this->dest = trim($destination,DS);
    }

    public function setAllowedFormats($formats){
        $this->allowedFormats = $formats;
    }

    protected function getFormat(){
        return strtolower(substr(strrchr($_FILES[$this->varName]['name'], '.'),1));
    }

    protected abstract function _isValid();

    private function isValid() {
        return $_FILES[$this->varName]['error'] == 0 && $_FILES[$this->varName]['size'] <= Config::$maxSize && $this->_isValid();
    }

    public function execute(){
        if ($this->isValid()) {
            $rname = sha1(uniqid()).".".$this->getFormat();
            $name = DS.$this->dest.DS.$rname;
            if (move_uploaded_file($_FILES[$this->varName]["tmp_name"],$name)) {
                return $rname;
            }
        }
        return false;
    }

} 