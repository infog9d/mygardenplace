<?php

if (Config::$useSmtp) {
    ini_set("SMTP", Config::$smtpHost);
    ini_set("sendmail_from", Config::$from);
    ini_set("smtp_port", Config::$smtpPort);
}
/**
 * Class Mailer
 */
class Mailer extends Service {


    private $to;
    private $from;
    private $header = "";
    private $content;

    public function __construct($to, $from, $type = "text/html; charset=iso-8859-1"){
        if (preg_match("/([A-Za-z-_.0-9]+@[A-Za-z-_.0-9]+)/",$to) && preg_match("/([A-Za-z-_.0-9]+@[A-Za-z-_.0-9]+)/",$from)) {
            $this->to = $to;
            $this->from = $from;
            //$this->addHeader("To", $to);
            //$this->addHeader("From", $from);
            $this->addHeader("Content-Type", $type);
        } else {
            throw new ErrorException("Please correct your mail format");
        }
    }

    public function addHeader($name, $value){
        $this->header .= $name.": ".$value."\r\n";
    }

    public function setView($view, $params = array()) {
        ob_start();
        require ROOT.DS."views".DS."mails".DS.$view.".php";
        $this->content = ob_get_clean();
    }

    public function setContent($content){
        $this->content = $content;
    }

    public function send($subject){
        $isSent = mail($this->to,$subject,$this->content,$this->header,$this->from);
        if (!$isSent) {
            throw new ErrorException("An error occurred while sending the mail");
        }
    }

} 