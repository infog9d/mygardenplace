<?php
/**
 * Created by IntelliJ IDEA.
 * User: Frost_000
 * Date: 19/05/2015
 * Time: 14:15
 */

class Recaptcha extends Model
{


    private $key_secret;

    private $key_site;


    /**
     * @param $key_secret
     * @param $key_site
     */
    function __construct($key_secret, $key_site)
    {
        $this->key_secret = Config::$key_secret;
        $this->$key_site = Config::$key_site;

    }

    /**
     * Generate the html captcha's code
     * @return string
     */
    public function html()
    {
        return '<div class="g-recaptcha" data-sitekey="' . Config::$key_site . '"></div>';
    }

    /**
     * @return string
     */
    public function script()
    {
        return '<script src="https://www.google.com/recaptcha/api.js"></script>';
    }


    /**
     * Check the response given by recaptcha
     * @param string $response
     * @return bool
     */
    public function isValid($response)
    {
        if (empty($response)) {
            return false;
        }

        $params = array(
            'secret' => $this->key_secret,
            'response' => $response
        );
        $url = "https://www.google.com/recaptcha/api/siteverify?" . http_build_query($params);

        $responses = file_get_contents($url);
        if (empty($responses) || is_null($responses)) {
            return false;
        }
        $json = json_decode($responses);
        return $json->success;
    }
}
