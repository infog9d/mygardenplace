<?php

/**
 * Configuration values
 */
class Config {

    /**
     * Database configuration
     * @var array
     */
    public static $database = array(
        "host"      => "127.0.0.1",
        "user"      => "root",
        "password"  => "root",
        "database"  => "mygardenplace"
    );

    /**
     * default controller name
     * @var string
     */
    public static $defaultController = "home";

    /**
     * Email adress
     * @var string
     */
    public static $from = "noreply@mygardenplace.com";

    public static $useSmtp = true;

    public static $smtpHost = "127.0.0.1";
    public static $smtpPort = "1025";

    /**
     * Uploaded files max size
     * @var int
     */
    public static $maxSize = 1048576; //1MO

    /**
     * Images max width
     * @var int
     */
    public static $maxWidth = 640;

    /**
     * Images max height
     * @var int
     */
    public static $maxHeight = 1000;

    public static $key_secret = "6Lc6AAgTAAAAAAAhgDb1CcMD2UeCWDaTYZTYY3fe";

    public static $key_site = "6Lc6AAgTAAAAAA-YpSuD8h4Db7EwRVLLyVk6LCs1";
    public static $debug = true;


}