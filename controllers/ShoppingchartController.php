<?php


class ShoppingchartController extends Controller {

    public function __construct($prefix,$name,Request $request)
    {
        parent::__construct($prefix, $name, $request);
        if (!Session::has("user")) {
            Session::setFlashbag("connectionError", "Vous devez vous connecter pour acceder a votre panier");
            $this->redirect(Router::generateUrl('home'));
        }

    }

    public function indexAction($params) {

        list($offers, $totalTTC, $totalHT) = $this->getInformations();


        $this->render("index",array("offers"=>$offers,"totalTTC"=>$totalTTC,"totalHT"=>$totalHT));
    }

    public function countOffer() {
        /** @var ShoppingCart $shoppingCartManager */
        $shoppingCartManager = $this->loadModel("ShoppingCart");
        $user = Session::get("user");
        $results = $shoppingCartManager->listOffersId($user["id"]);
        echo count($results);
    }

    public function validateAction($params) {
        list($offers, $totalTTC, $totalHT) = $this->getInformations();

        /** @var Booking $bookingManager */
        $bookingManager = $this->loadModel("Booking");

        /** @var User $userManager */
        $userManager = $this->loadModel("user");
        $user = Session::get("user");
        $bookingManager->add($totalTTC,$user["id"],$offers);
        $this->loadService("Mailer");
        $mailer = new Mailer($user["mail"], Config::$from);
        $mailer->setView("validateShoppingchart", array(
            "offers"=>$offers,
            "totalTTC"=>$totalTTC,
            "totalHT"=>$totalHT,
            "sentence"=>"Voici le récapitulatif de votre commande :"
        ));
        $mailer->send("MyGardenPlace - Confirmation de votre réservation de produits");

        $offersByGardener = $this->filterOffersByGardener($offers);
        foreach($offersByGardener as $gardener => $offers) {
            $agrdener = $userManager->get($gardener,"array");
            $mailer = new Mailer($agrdener["mail"], Config::$from);
            $mailer->setView("validateShoppingchart", array(
                "offers" => $offers,
                "totalTTC" => $totalTTC,
                "totalHT" => $totalHT,
                "sentence" => $user["firstname"]." ".$user["lastname"]." vient de vous commander les produits ci-dessous, nous vous invitons a prendre contact avec ce gardener a cette adresse : ".$user["mail"]
            ));
            $mailer->send("MyGardenPlace - Une nouvelle commande vous à été passée");
        }
        $this->clearShoppingChart();
        $this->redirect(Router::generateUrl("home"));
    }

    public function clearAction() {
        $this->clearShoppingChart();
        $this->redirect(Router::generateUrl("home"));
    }

    public function clearShoppingChart() {
        /** @var ShoppingCart $shoppingChartManager */
        $shoppingChartManager = $this->loadModel("ShoppingCart");
        $user = Session::get("user");
        $shoppingChartManager->clearByUser($user["id"]);
    }

    public function setShoppingChartAction($params) {
        if (isset($this->request->data->offerId)&&!empty($this->request->data->offerId)
            &&isset($this->request->data->quantity)&&!empty($this->request->data->quantity)) {
            /** @var ShoppingCart $shoppingChartManager */
            $shoppingChartManager = $this->loadModel("ShoppingCart");
            $shoppingChartManager->setOffer($this->request->data->offerId,$this->request->data->quantity);
            $this->redirect(Router::generateUrl("shoppingchart"));
        } else {
            $this->redirect(Router::generateUrl("shoppingchart"));
        }
    }

    public function deleteOfferAction($params) {
        if (isset($this->request->data->offerId)&&!empty($this->request->data->offerId)) {
            /** @var ShoppingCart $shoppingChartManager */
            $shoppingChartManager = $this->loadModel("ShoppingCart");
            $shoppingChartManager->deleteOffer($this->request->data->offerId);
            $this->redirect(Router::generateUrl("shoppingchart"));
        } else {
            $this->redirect(Router::generateUrl("shoppingchart"));
        }
    }

    /**
     * Get offers and total of the current shopping chart
     * @return array
     */
    public function getInformations()
    {
        /** @var ShoppingCart $shoppingCartManager */
        $shoppingCartManager = $this->loadModel("ShoppingCart");
        /** @var Offers $offerManager */
        $offerManager = $this->loadModel("Offers");
        /** @var Subcategory $subcategoryManager */
        $subcategoryManager = $this->loadModel("Subcategory");
        $user = Session::get("user");
        $results = $shoppingCartManager->listOffersId($user["id"]);
        $offers = array();
        $totalTTC = 0;
        foreach ($results as $result) {
            $offer = $offerManager->getFormated($result["offer_id"]);
            $offer["quantity"] = $result["quantity"];
            if ($result["type"] == "exchange" && $exchangeAgainst = $subcategoryManager->get($result["exchange_against"])) {
                $offer["exchangeAgainst"] = $exchangeAgainst["name"];
                $offer["price"] = "Echange";
            } else {
                $totalTTC += $offer["price"] * $offer["quantity"];
            }
            $offers[] = $offer;
        }
        $totalHT = $totalTTC / (1.07);
        return array($offers, $totalTTC, $totalHT);
    }

    public function filterOffersByGardener($offers) {
        $byGardener = array();
        foreach($offers as $offer) {
            if (array_key_exists($offer["accountId"],$byGardener)) {
                $byGardener[$offer["accountId"]][] = $offer;
            } else {
                $byGardener[$offer["accountId"]] = array($offer);
            }
        }
        return $byGardener;
    }

}