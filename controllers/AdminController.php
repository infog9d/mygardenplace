<?php


class AdminController extends Controller {

    public function __construct($prefix,$name,Request $request){
        parent::__construct($prefix,$name,$request);
        if (!Session::has("user")) {
            $this->requireMethod('errors',"notFoundAction");
            die();
        } else {
            $user = Session::get("user");
            if (!$user["is_admin"]) {
                $this->requireMethod('errors',"notFoundAction");
                die();
            } else {
                $this->setLayout("admin");
            }
        }

    }
    
    public function indexAction($params){
        $this->render("index");
    }
    
    public function usersAction($params){
        /** @var User $userManager */
        $userManager = $this->loadModel("user");
        $users = $userManager->listAll();
        $this->render("users", array("users" => $users));
    }

    public function updateUserAction($params){
        $user = Session::get("user");
        if (isset($params[0])&&isset($params[1])&&$user["id"] != $params[1]) {
            /** @var User $userManager */
            $userManager = $this->loadModel("user");
            $user = $userManager->get($params[1]);
            if ($user) {
                switch($params[0]) {
                    case "banish":
                        if ($user->valid) {
                            $userManager->set($params[1], array("valid" => 0));
                        } else {
                            $userManager->set($params[1], array("valid" => 1));
                        }
                        break;
                    case "mail":
                        if ($user->mailvalid) {
                            $userManager->set($params[1], array("mailvalid" => 0));
                        } else {
                            $userManager->set($params[1], array("mailvalid" => 1));
                        }
                        break;
                    case "admin":
                        if ($user->is_admin) {
                            $userManager->set($params[1], array("is_admin" => 0));
                        } else {
                            $userManager->set($params[1], array("is_admin" => 1));
                        }
                        break;
                    default:
                        $this->requireMethod('errors',"notFoundAction");
                }
            } else {
                $this->requireMethod('errors',"notFoundAction");
            }
        } else {
            $this->requireMethod('errors',"notFoundAction");
        }
    }

    public function deleteUserAction($params){
        $user = Session::get("user");
        if (isset($params[0])&&$user["id"] != $params[0]) {
            /** @var User $userManager */
            $userManager = $this->loadModel("user");
            $user = $userManager->get($params[0]);
            if ($user) {
                $userManager->rm($params[0]);
                $this->redirect(Router::generateUrl("admin",'users'));
            } else {
                $this->requireMethod('errors',"notFoundAction");
            }
        } else {
            $this->requireMethod('errors','notFoundAction');
        }
    }

    public function userBookingsAction($params) {
        if (isset($params[0])) {
            /** @var Booking $bookingManager */
            $bookingManager = $this->loadModel("Booking");
            $bookings = $bookingManager->listByUser($params[0]);

            /** @var Offers $offersManager */
            $offersManager = $this->loadModel("Offers");
            $offers = $offersManager->getByUserId($params[0]);

            $trueOffers = array();
            foreach($offers as $offer) {
                $trueOffers[] = $offersManager->getFormated($offer["id"]);
            }

            $this->render("bookings",array("bookings"=>$bookings,"offers"=>$trueOffers));
        } else {
            $this->requireMethod('errors',"notFoundAction");
        }
    }

    public function banOfferAction($params) {
        if (isset($params[0])) {
            /** @var Offers $offersManager */
            $offersManager = $this->loadModel("Offers");
            if($offersManager->get($params[0])->valid == 1) {
                $offersManager->set($params[0],array("valid"=>0));
            } else {
                $offersManager->set($params[0],array("valid"=>1));
            }
            echo "ok";
        } else {
            echo "ko";
        }
    }

    public function deleteOfferAction($params) {
        if (isset($params[0])) {
            /** @var Offers $offersManager */
            $offersManager = $this->loadModel("Offers");
            $offersManager->rm($params[0]);
            $this->redirect(Router::generateUrl("admin","users"));
        } else {
            $this->redirect(Router::generateUrl("admin","users"));
        }
    }
    
    public function faqAction($params) {
        /** @var Faq $faqManager */
        $faqManager = $this->loadModel("Faq");
        $faqs = $faqManager->findAll();
        $this->render("faq",array("faqs"=>$faqs));
    }

    public function addFaqAction($params) {
        if (isset($this->request->data->title)
            &&!empty($this->request->data->title)
            &&isset($this->request->data->content)
            &&!empty($this->request->data->content)) {
            /** @var Faq $faqManager */
            $faqManager = $this->loadModel("Faq");
            $faqManager->insert($this->request->data->title,$this->request->data->content);
            $this->redirect(Router::generateUrl("admin","faq"));
        }
    }

    public function setFaqAction($params) {
        if (isset($params[0])) {
            if (isset($this->request->data->title)
                &&!empty($this->request->data->title)
                &&isset($this->request->data->content)
                &&!empty($this->request->data->content)) {
                /** @var Faq $faqManager */
                $faqManager = $this->loadModel("Faq");
                $faqManager->set($params[0],array(
                    "title" => $this->request->data->title,
                    "content" => $this->request->data->content,
                ));
                $this->redirect(Router::generateUrl("admin","faq"));
            } else {
                $this->redirect(Router::generateUrl("admin","faq"));
            }
        } else {
            $this->redirect(Router::generateUrl("admin","faq"));
        }
    }

    public function deleteFaqAction($params) {
        if (isset($params[0])) {
            /** @var Faq $faqManager */
            $faqManager = $this->loadModel("Faq");
            $faqManager->rm($params[0]);
            $this->redirect(Router::generateUrl("admin","users"));
        } else {
            $this->redirect(Router::generateUrl("admin","users"));
        }
    }

    public function categoriesAction($params) {
        /** @var Category $categoryManager */
        $categoryManager = $this->loadModel("Category");
        $categories = $categoryManager->findAll();
        $this->render("categories",array("categories"=>$categories));
    }

    public function addCategoryAction($params) {
        if (isset($this->request->data->name)
            &&!empty($this->request->data->name)) {
            /** @var Category $categoryManager */
            $categoryManager = $this->loadModel("Category");
            $categoryManager->insert($this->request->data->name);
            $this->redirect(Router::generateUrl("admin","categories"));
        }
    }

    public function setCategoryAction($params) {
        if (isset($params[0])) {
            if (isset($this->request->data->name)
                &&!empty($this->request->data->name)) {
                /** @var Category $categoryManager */
                $categoryManager = $this->loadModel("Category");
                $categoryManager->set($params[0],array(
                    "name" => $this->request->data->name,
                ));
                $this->redirect(Router::generateUrl("admin","categories"));
            } else {
                $this->redirect(Router::generateUrl("admin","categories"));
            }
        } else {
            $this->redirect(Router::generateUrl("admin","categories"));
        }
    }

    public function deleteCategoryAction($params) {
        if (isset($params[0])) {
            /** @var Category $categoryManager */
            $categoryManager = $this->loadModel("Category");
            $categoryManager->rm($params[0]);
            $this->redirect(Router::generateUrl("admin","categories"));
        } else {
            $this->redirect(Router::generateUrl("admin","categories"));
        }
    }

    public function subcategoriesAction($params) {
        /** @var Subcategory $subcategoryManager */
        $subcategoryManager = $this->loadModel("Subcategory");
        $subcategories = $subcategoryManager->findAll();
        /** @var Category $categoryManager */
        $categoryManager = $this->loadModel("Category");
        $categories = $categoryManager->findAll();
        $this->render("subcategories",array(
            "subcategories"=>$subcategories,
            "categories" => $categories
        ));
    }

    public function addSubcategoryAction($params) {
        if (isset($this->request->data->name)
            &&!empty($this->request->data->name)) {
            /** @var Subcategory $subcategoryManager */
            $subcategoryManager = $this->loadModel("Subcategory");
            $subcategoryManager->insert($this->request->data->name,$this->request->data->categoryId);
            $this->redirect(Router::generateUrl("admin","subcategories"));
        }
    }

    public function setSubcategoryAction($params) {
        if (isset($params[0])) {
            if (isset($this->request->data->name)
                &&!empty($this->request->data->name)) {
                /** @var Subcategory $subcategoryManager */
                $subcategoryManager = $this->loadModel("Subcategory");
                $subcategoryManager->set($params[0],array(
                    "name" => $this->request->data->name,
                    "category_id" => $this->request->data->categoryId
                ));
                $this->redirect(Router::generateUrl("admin","subcategories"));
            } else {
                $this->redirect(Router::generateUrl("admin","subcategories"));
            }
        } else {
            $this->redirect(Router::generateUrl("admin","subcategories"));
        }
    }

    public function deleteSubcategoryAction($params) {
        if (isset($params[0])) {
            /** @var Subcategory $subcategoryManager */
            $subcategoryManager = $this->loadModel("Subcategory");
            $subcategoryManager->rm($params[0]);
            $this->redirect(Router::generateUrl("admin","subcategories"));
        } else {
            $this->redirect(Router::generateUrl("admin","subcategories"));
        }
    }

} 