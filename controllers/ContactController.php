<?php
/**
 * Class contactController
 */
class contactController extends Controller{

    /**
     * @param $params
     */

    public function indexAction($params)
    {
        $captcha = $this->addRecaptcha(Config::$key_secret,Config::$key_site);
        $this->render("index",
                      array("captcha"=> $captcha));

    }

    /**
     * @param $params
     * @throws ErrorException
     */
    public function sendAction($params)
    {

        $name= $this->request->data->name;
        $mail=$this->request->data->mail;
        $message=$this->request->data->message;
        if (
            isset($name)&&!empty($name)
            &&isset($mail)&&!empty($mail)
            &&isset($message)&&!empty($message)
            &&!empty($_POST['g-recaptcha-response'])
        )
        {
            $captcha = $this->addRecaptcha(Config::$key_secret,Config::$key_site);


            if (
                $captcha->isValid($_POST['g-recaptcha-response'])===true
            )
            {

                /** @var Mailer $mailer */
                $this->loadService("Mailer");
                $mailer = new Mailer(Config::$from,$mail);
                $mailer->setContent($message);
                $mailer->send("Nouveau message de ".$name);
                Session::setFlashbag("contactSuccess", "Votre message à bien été envoyé.");
                $this->redirect(Router::generateUrl("contact"));
            }
        }

        else {
            if (
                !(isset($name)&&!empty($name)
                &&isset($mail)&&!empty($mail)
                &&isset($message)&&!empty($message))){
                Session::setFlashbag("contactError", "Veuillez compléter tous les champs.");
                $this->redirect(Router::generateUrl("contact"));
            }
            else{
                Session::setFlashbag("contactError", "Veuillez compléter le captcha");
                $this->redirect(Router::generateUrl("contact"));
            }

        }

    }

    public function addRecaptcha($key_secret,$key_site){

        $this->loadService("Recaptcha");
        $captcha=new Recaptcha($key_secret,$key_site);
        return $captcha;

    }








}
