<?php
/**
 * Created by PhpStorm.
 * User: j-b
 * Date: 01/04/2015
 * Time: 10:40
 */

class SearchController extends Controller {

    /**
     * @param $params
     */
    public function indexAction($params){
        if ((isset($_GET["name"]))||
            (isset($_GET["gardener"]))||
            (isset($_GET["city"]))) {
            /** @var Subcategory $subcategoryManager */
            $subcategoryManager = $this->loadModel("Subcategory");
            $allSubCategories = $subcategoryManager->listAll();

            if (!empty($_GET["name"])) {
                $subcategory = $subcategoryManager->search($_GET["name"]);

                if (count($subcategory) > 0) {
                    $_GET["categories"] = $subcategory[0]["name"];
                    $_GET["name"] = "";
                }
            }
           //print_r($_GET);
            /** @var Offers $offerManager */
            $offerManager = $this->loadModel("Offers");
            $results = $offerManager->find($_GET["name"],(isset($_GET["categories"]))?$_GET["categories"]:"",$_GET["gardener"],$_GET["city"],isset($_GET["quantity"])?$_GET["quantity"]:"",Utils::determineExchangeOrSold());

            $cities = array();

            foreach($results as $result) {
                foreach($result["cities"] as $city) {
                    if (!in_array($city,$cities)) {
                        $cities[] = $city;
                    }
                }
            }

            if (count($cities)==0) {
                $cities[] = "22 rue notre-dame des champs, Paris, France";
            }

            $this->render("index", array("ads" => $results, "categories" => $allSubCategories, "cities" => $cities));

        }
    }

    public function resultsAction($params){

    }

} 