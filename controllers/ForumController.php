<?php
/**
 * Created by PhpStorm.
 * User: Okongo
 * Date: 2015-05-03
 * Time: 11:16 PM
 */

class ForumController extends Controller {

    public function __construct($prefix,$name,Request $request)
    {
        parent::__construct($prefix, $name, $request);
        if (!Session::has("user")) {
            Session::setFlashbag("connectionError","Vous devez vous connecter pour acceder au forum.");
            $this->redirect(Router::generateUrl("home"));
            die();
        }

    }
    public function indexAction()
    {
        /** @var Topics $topicsManager */
        $topicsManager = $this->loadModel("Topics");
        $topics = $topicsManager->listAll();

        $this->render('index_forum', array("topics" => $topics));
    }

    public function addTopicAction($params) {
        $user = Session::get("user");
        if ($user["is_admin"]&&isset($this->request->data->topic)&&!empty($this->request->data->topic)) {
            /** @var Topics $topicsManager */
            $topicsManager = $this->loadModel("Topics");
            $topicsManager->insert($this->request->data->topic);
            $this->redirect(Router::generateUrl("forum"));
        } else {
            $this->requireMethod('errors',"notFoundAction");
            die();
        }
    }

    public function getNumberOfMessagesByTopics($params) {
        if (isset($params["id"])&&is_numeric($params["id"])) {
            /** @var Topics $topicsManager */
            $topicsManager = $this->loadModel("Topics");
            $result = $topicsManager->numberPostAppearence($params["id"]);
            echo isset($result[0])?$result[0]:0;
        }
    }

    public function getNumberOfResponsesByTopics($params) {
        if (isset($params["id"])&&is_numeric($params["id"])) {
            /** @var Topics $topicsManager */
            $topicsManager = $this->loadModel("Topics");
            $result = $topicsManager->numberResponsesAppearence($params["id"]);
            echo isset($result[0])?$result[0]:0;
        }
    }

    public function getPictureUser($params){
        if(isset($params["id"]) &&is_numeric($params["id"])) {
            /** */
            $pictureManager = $this->loadModel("user");
            $result = $pictureManager->getPicture($params["id"]);
            echo isset($result[0])?$result[0]:"pas de picture";
        }
    }
    public function getNumberOfResponsesByPosts($params) {
        if (isset($params["id"])&&is_numeric($params["id"])) {
            /** @var Posts $postsManager */
            $postsManager = $this->loadModel("Posts");
            $result = $postsManager->numberResponsesAppearence($params["id"]);
            echo isset($result[0])?$result[0]:0;
        }
    }

    public function getLastResponseByPosts($params){
        if(isset($params["id"])&&is_numeric($params["id"])) {
            /** @var Response $responseManager */
            $responseManager = $this->loadModel("Response");
            $result = $responseManager->lastResponseByPosts($params["id"]);
            echo isset($result[0]) ?substr($result[0],0,50)."..." :"pas de message";
        }
    }

    public function getLastResponseByTopics($params){
        if(isset($params["id"])&&is_numeric($params["id"])) {
            /** @var Response $responseManager */
            $responseManager = $this->loadModel("Response");
            $result = $responseManager->LastResponseByTopics($params["id"]);
            echo isset($result[0]) ?substr($result[0],0,50)."..." :"pas de message";
        }
    }

    public function postAction($params) {
        if (isset($params[0])&&is_numeric($params[0])) {
            /** @var Topics $topicsManager */
            $topicsManager = $this->loadModel("Topics");
            $topic = $topicsManager->get($params[0]);




            /** @var Topics $topicsManager */
            $topicsManager = $this->loadModel("Posts");
            $answer = $topicsManager->getPost($params[0]);


            /** @var Posts $postsManager */
            $postsManager = $this->loadModel("Posts");
            $posts = $postsManager->listPostsByTopic($params[0]);

            $this->render('post', array("posts" => $posts,'topic'=> $topic,'post'=> $answer));
        } else {
            Controller::requireMethod("errors","notFoundAction");
        }
    }

    public function banPostAction($params) {
        if (Session::has("user")) {
            $user = Session::get("user");
            if ($user["is_admin"]) {
                if (isset($params[0])) {
                    /** @var Posts $postsManager */
                    $postsManager = $this->loadModel("Posts");
                    if($postsManager->get($params[0])->valid == 1) {
                        $postsManager->set($params[0],array("valid"=>0));
                    } else {
                        $postsManager->set($params[0],array("valid"=>1));
                    }
                    echo "ok";
                } else {
                    echo "ko";
                }
            }
        }
    }

    public function banResponsesAction($params) {
        if (Session::has("user")) {
            $user = Session::get("user");
            if ($user["is_admin"]) {
                if (isset($params[0])) {
                    /** @var Response $responsesManager */
                    $responsesManager = $this->loadModel("Posts");
                    if ($responsesManager->get($params[0])->valid == 1) {
                        $responsesManager->set($params[0], array("valid" => 0));
                    } else {
                        $responsesManager->set($params[0], array("valid" => 1));
                    }
                    echo "ok";
                } else {
                    echo "ko";
                }
            }
        }
    }

    public function deletePostAction($params) {
        if (Session::has("user")) {
            $user = Session::get("user");
            if ($user["is_admin"]) {
                if (isset($params[0])) {
                    /** @var Posts $postsManager */
                    $postsManager = $this->loadModel("Posts");
                    $postsManager->rm($params[0]);

                } else {
                    $this->redirect(Router::generateUrl("forum","index"));
                }
            }
        }
    }

    public function deleteResponseAction($params) {
        if (Session::has("user")) {
            $user = Session::get("user");
            if ($user["is_admin"]) {
                if (isset($params[0])) {
                    /** @var Response $responseManager */
                    $responseManager = $this->loadModel("Response");
                    $responseManager->rm($params[0]);

                } else {
                    $this->redirect(Router::generateUrl("forum","index"));
                }
            }
        }
    }

    public function deleteTopicAction($params) {
        if (Session::has("user")) {
            $user = Session::get("user");
            if ($user["is_admin"]) {
                if (isset($params[0])) {
                    /** @var Topics $topicManager */
                    $topicManager = $this->loadModel("Topics");
                    $topicManager->rm($params[0]);

                } else {
                    $this->redirect(Router::generateUrl("forum","index"));
                }
            }
        }
    }

    public function responseAction ($params) {
        if (isset($params[0])&&is_numeric($params[0])) {

            /** @var Posts $postsManager */
            $postsManager = $this->loadModel("Posts");
            $post = $postsManager->getPost($params[0]);

            /** @var Response $responseManager*/
            $responseManager = $this->loadModel("Response");
            $nbResponses = $responseManager->numbResponsesByPost($params[0]);
            $response = $responseManager->listResponseByPost($params[0]);

            $this->render('responses', array("response" => $response,  'nbResponses' => $nbResponses, "post" => $post));
        } else{
            Controller::requireMethod("errors","notFound");
        }
    }

    public function insertPostAction(){
        $message = $this->request->data->message;
        $topicId = $this->request->data->topicId;
        $title = $this->request->data->title;

        if(isset($message)&&isset($title)&&isset($topicId)&&!empty($message)&&!empty($title)&&!empty($topicId)){

            /** @var Posts $insertPostManager */
            $insertPostManager =$this->loadModel("Posts");
            $user = Session::get("user");
            $insertPostManager->insertPosts($title,$message,$user['id'],$topicId);
            $this->redirect(Router::generateUrl("forum","post",array("id"=> $topicId)));
        }
    }

    public function insertResponsesAction(){
        $msg = $this->request->data->msg;
        $postId = $this->request->data->postId;
        if(isset($msg)&&isset($postId)&&!empty($msg)&&!empty($postId)){
            /**@var Response $insertResponsesManager */
            $insertResponsesManager = $this->loadModel("Response");
            $user = Session::get("user");
            $insertResponsesManager->insertResponses($msg,$postId,$user['id']);
            $this->redirect(Router::generateUrl("forum","response",array("id"=> $postId)));
        }
    }


}