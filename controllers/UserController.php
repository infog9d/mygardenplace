<?php

/**
 * Class UserController
 * This controller handle all user actions. This include :
 *  - The registerAction
 *  - The connectionAction
 *  - The accountAction
 *  - All other stuff that needs to be done on user management
 */
class UserController extends Controller{

    /**
     * registers an user
     * @param $params
     */
    public function registerAction($params){
        if (isset($this->request->data->firstname)&&
            isset($this->request->data->lastname)&&
            isset($this->request->data->birthdate)&&
            isset($this->request->data->mail)&&
            isset($this->request->data->password) &&
            isset($this->request->data->phone)&&
            isset($this->request->data->city)&&
            !empty($this->request->data->firstname)&&
            !empty($this->request->data->lastname)&&
            !empty($this->request->data->birthdate)&&
            !empty($this->request->data->mail)&&
            !empty($this->request->data->password) &&
            !empty($this->request->data->phone)&&
            !empty($this->request->data->city)) {
            /** @var City $cityManager */
            $cityManager = $this->loadModel("City");
            $cities = $cityManager->search($this->request->data->city);

            if (count($cities)>0) {
                /** @var User $user */
                $user = $this->loadModel("user");
                $user->register(
                    $this->request->data->firstname,
                    $this->request->data->lastname,
                    $this->request->data->birthdate,
                    $this->request->data->mail,
                    $cities[0]["idCity"],
                    $this->request->data->password,
                    $this->request->data->phone
                );
                Session::setFlashbag("registrationSuccess", "Votre demande d'inscription à bien été prise en compte. Un e-mail de validation vous a été envoyé pour confirmer votre inscription.");
                $this->redirect(Router::generateUrl("home"));
            } else {
                Session::setFlashbag("registrationError","Une erreur est survenue lors de la tentative d'inscription. Votre ville ne semble pas exister.");
                $this->redirect(Router::generateUrl("home"));
            }
        } else {
            Session::setFlashbag("registrationError","Une erreur est survenue lors de la tentative d'inscription. Veuillez completer tous les champs.");
            $this->redirect(Router::generateUrl("home"));
        }

    }

    /**
     * validates an email address
     * @param $params
     * @throws ErrorException
     */
    public function checkMailAction($params){
        $token = $params[0];
        /** @var User $user */
        $user = $this->loadModel("user");
        if ($user->validateMail($token)) {
            $this->redirect(Router::generateUrl("home"));
        } else {
            Controller::requireMethod("errors", "tokenInvalidAction");
        }
    }

    /**
     * require registration's form view
     */
    public function registrationForm(){
        require "..".DS."views".DS."user".DS."registrationForm.php";
    }

    /**
     * require connection's form view
     */
    public function connectionForm(){
        require "..".DS."views".DS."user".DS."connectionForm.php";
    }

    /**
     * require contact's form view
     */
    public function contactForm($params){
        $this->params = $params;
        require "..".DS."views".DS."user".DS."contactForm.php";
    }

    /**
     * contact by mail an user with his id
     */
    public function contactAction($id){
        $message = $_POST["Message"];

        if (Session::has("user")) {
            /** @var User $userManager */
            $userManager = $this->loadModel("user");
            $seller = $userManager->get($id[0]);
            if ($seller) {
                $mailContact = $seller->mail;

                $this->loadService("Mailer");
                $user = Session::get("user");
                $senderMail = $user["mail"];
                $mailer = new Mailer($mailContact, $senderMail);
                $mailer->setContent("$message");
                $mailer->send("[MyGardenPlace] Nouveau message de ".$user["firstname"]." ".$user["lastname"]);
            }

            $this->redirect(Router::generateUrl("home"));

        }else{
            Session::setFlashbag("connectionError","Vous devez être connecté pour contacter un membre.");
            $this->redirect(Router::generateUrl("home"));
        }


    }


    /**
     * connects an user
     * @param $params
     */
    public function connectionAction($params){
        /** @var User $user */
        $user = $this->loadModel("user");
        if (isset($this->request->data->mail)&&isset($this->request->data->password)) {
            $isValid = $user->isValid($this->request->data->mail,$this->request->data->password);
            if (is_array($isValid)) {
                Session::add("user", $isValid);
            } else {
                Session::setFlashbag("connectionError","Une erreur est survenue lors de la tentative de connection. Veuillez verifier vos identifiants.");
            }
        } else {
            Session::setFlashbag("connectionError","Tous les champs sont obligatoires.");
        }
        $this->redirect(Router::generateUrl("home"));
    }

    /**
     * disconnects an user
     * @param $params
     */
    public function logoutAction($params){
        if (Session::has("user")) {
            Session::rm("user");
        }
        $this->redirect(Router::generateUrl("home"));
    }


    /**
     * View my Account
     * @param $params
     */
    public function accountAction($params){
        if (Session::has("user")) {
            if (isset($params[0])) {
                $this->redirect(Router::generateUrl("home"));
            } else {

                /** @var User $userManager */
                $userManager = $this->loadModel("user");
                /** @var Offers $offerManager */
                $offerManager= $this->loadModel("Offers");
                $user = Session::get("user");
                $myAlerts = $userManager->listAlerts($user["id"]);
                $offer=$offerManager->getLast($user["id"]);

                $this->render("myAccount",array("myAlerts"=>$myAlerts,
                    "offers"=>$offer));
            }
        } else {
            $this->redirect(Router::generateUrl("home"));
        }
    }

    /**
     * Add an alert
     * @param $params
     */
    public function postAlertAction($params) {
        if (Session::has("user")) {
            if(isset($this->request->data->keyword)&&!empty($this->request->data->keyword)) {
                /** @var User $userManager */
                $userManager = $this->loadModel("user");
                $user = Session::get("user");
                if($userManager->insertAlert($user["id"],$this->request->data->keyword)) {
                    Session::setFlashbag("successAlert","Votre alerte a bien été enregistrée");
                } else {
                    Session::setFlashbag("errorAlert","Une erreur est survenue lors de l'enregistrement de votre alerte");
                }
                $this->redirect(Router::generateUrl("user","account"));
            }
        }
    }

    public function rmAlertAction($params) {
        if (Session::has("user")) {
            /** @var User $userManager */
            $userManager = $this->loadModel("user");
            $alert = $userManager->getAlert($params[0]);
            $user = Session::get("user");
            if ($user["id"] == $alert["user_id"]) {
                $userManager->rmAlert($params[0]);
            }
        }
        $this->redirect(Router::generateUrl("user","account"));
    }

    /**
     * Search an user and return json encoded results
     * @param $params
     */
    public function searchAction($params){
        /** @var User $userManager */
        $userManager = $this->loadModel("user");
        $users = $userManager->search($params[0]);
        for($i=0;$i<count($users);$i++) {
            $users[$i]["name"] = $users[$i]["firstname"]." ".$users[$i]["lastname"];
        }
        echo json_encode($users);
    }

    /**
     * Add or set user's avatar
     * @param $params
     * @throws Exception
     */
    public function postImageAction($params) {
        if (Session::has("user")) {
            if (!empty($_FILES["picture"])) {
                /** @var ImageUploader $uploader */
                $this->loadService("Uploader");
                $this->loadService("ImageUploader");
                $uploader = new ImageUploader("picture");
                $name = $uploader->execute();
                if ($name != false) {
                    /** @var User $userManager */
                    $userManager = $this->loadModel("user");
                    $user = Session::get("user");
                    $userManager->set($user["id"],array(
                        "picture" => $name
                    ));
                    Session::add("user",$userManager->get($user["id"],"array"));
                    $this->redirect(Router::generateUrl("user","account"));
                } else {
                    $this->redirect(Router::generateUrl("user","account"));
                }
            }
        } else {
            $this->redirect(Router::generateUrl("user","account"));
        }
    }

//addnoteAction
} 