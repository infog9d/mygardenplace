<?php

/**
 * Class HomeController
 */
class HomeController extends Controller {

    public function indexAction($params) {

        /** @var Offers $offerManager */
        $offerManager = $this->loadModel("Offers");
        $lastAds = $offerManager->getLast();

        $this->render("index",array(
            "lastAds" => $lastAds
        ));
    }

    public function testCamelAction($params) {
        $this->render("index",array(
            "name" => "test camelCase"
        ));
    }

    public function testIncludeMethod(){
        echo 'test';
    }

    public function testMaillingAction($params){
        $mail = mail("to@mail.com","subject","message",null,"from@mail.com");
    }

} 