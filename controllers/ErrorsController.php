<?php

/**
 * Class ErrorsController
 */
class ErrorsController extends Controller {

    /*public function __construct($prefix,$name,Request $request) {
        parent::__construct($prefix,$name,$request);
        $this->setLayout("errors");
    }*/

    public function notFoundAction($params){
        $this->render("404");
    }

    public function invalidTokenAction($params){
        $this->render("invalidToken");
    }

} 