<?php

/**
 * Class CityController
 */
class CityController extends Controller {

    public function searchAction($params){
        /** @var City $cityManager */
        $cityManager = $this->loadModel("City");
        $cities = $cityManager->search($params[0]);
        echo json_encode($cities);
    }

} 