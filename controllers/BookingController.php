<?php

class BookingController extends Controller {

    public function __construct($prefix,$name,Request $request)
    {
        parent::__construct($prefix, $name, $request);
        if (!Session::has("user")) {
            Session::setFlashbag("connectionError", "Vous devez vous connecter pour acceder a votre panier");
            $this->redirect(Router::generateUrl('home'));
        }

    }



}