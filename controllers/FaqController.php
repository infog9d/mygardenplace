<?php

class FaqController extends Controller {

    public function indexAction($params)
    {
        /** @var Faq $faqManager */
        $faqManager = $this->loadModel("Faq");
        $faqs = $faqManager->findAll();
        $this->render("index", array("faqs"=>$faqs));
    }

}