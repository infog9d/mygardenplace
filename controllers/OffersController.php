<?php


class OffersController extends Controller {

    public function indexAction($params){

    }

    public function viewAction($params){
        if (isset($params[0])&& is_numeric($params[0])) {
            /** @var Offers $offersManager */
            $offersManager = $this->loadModel("Offers");

            $offer = $offersManager->getFormated($params[0]);
            if ($offer&&$offer["valid"]) {
                $this->render("view",array("ad"=>$offer));
            } else {
                echo $this->requireMethod("errors","notFoundAction");
            }
        } else {
            echo $this->requireMethod("errors","notFoundAction");
        }

    }
    public function sendMailAction($params)
    {
        if (Session::has("user")) {
            $quantity = $_POST["quantity"];

            $idAd = $params[0];

            $ad = $this->loadModel("offers");
            $ad = new Offers();
            $ad = $ad->get($idAd);

            $quantityReal = $ad -> quantity;


            if ($quantity <= $quantityReal){


                /** @var Mailer $mailer */

                $idSeller = $ad -> owner_id;
                $seller = $this->loadModel("user");
                $seller = new User();
                $seller = $seller->get($idSeller);
                $mailSeller = $seller -> mail;

                $mailer = $this->loadService("mailer");
                $user = Session::get("user");
                $senderMail = $user["mail"];
                $mailer = new Mailer($mailSeller, $senderMail);
                $mailer->setContent("$quantity");
                $mailer->send("");


            } else {
                echo "La réservation a échoué";

            }
        }
    }


    public function tileView($params){
        require ROOT.DS."views".DS."offers".DS."tileView.php";
    }

    public function addToChart($params){
        require ROOT.DS."views".DS."offers".DS."addToChart.php";
    }

    public function addAction($params){
        if (Session::has("user")) {
            /** @var Subcategory $subcatoriesManager */
            $subcatoriesManager = $this->loadModel("Subcategory");

            //$subcatoriesManager->insert();
            $subcategories = $subcatoriesManager->listAll();

            $this->render("add",array(
                "subcategories" => $subcategories
            ));
        } else {
            $this->redirect(Router::generateUrl("home"));
        }
    }
    public function getByGardenerIdOfferSinceAction($id,$lastname,$firstname){
        if (!isset($lastname)||!isset($firstname)|| !empty($lastname) || !empty($firstname)){
            $user = $this->loadModel('user');
            $user = $user->get($id);
            $lastname = $user -> lastname;
            $firstname =$user -> firstname;
        }
        $gardener = array($firstname,$lastname);
        $date = new DateTime('-1 years');


        $offers = $this->loadModel('offers');
        $offers = $offers->getByGardenerSince($gardener, $date);

        print_r($offers);




    }

    public function postAction($params){
        if (Session::has("user")) {
            $datas = $this->request->data;
            if (isset($datas->title)&&
                !empty($datas->title)&&
                isset($datas->subcategory)&&
                !empty($datas->subcategory)&&
                isset($datas->cities)&&
                !empty($datas->cities)&&
                isset($datas->quantity)&&
                !empty($datas->quantity)) {

                if (is_numeric($datas->quantity)&&$datas->quantity>0) {

                    if (!empty($_FILES["picture"])) {
                        /** @var ImageUploader $uploader */
                        $this->loadService("Uploader");
                        $this->loadService("ImageUploader");
                        $uploader = new ImageUploader("picture");
                        $name = $uploader->execute();
                        if ($name != false) {
                            $picture = $name;
                        } else {
                            $picture = "";
                        }
                    } else {
                        $picture = "";
                    }

                    /** @var Offers $offerManager */
                    $offerManager = $this->loadModel("Offers");
                    $user = Session::get("user");
                    $result = $offerManager->createOffer(
                        $datas->title,
                        $datas->subcategory,
                        $datas->cities,
                        $datas->quantity,
                        $user['id'],
                        $datas->description,
                        $datas->price,
                        $picture,
                        $datas->categories
                    );

                    if (strpos($result, "erreur :") !== false) {
                        //erreur
                        Session::setFlashbag("errorPostOffer", $result);
                        $this->redirect(Router::generateUrl("offers", "add"));
                    } else {
                        Session::setFlashbag("successPostOffer", $result);
                        $this->redirect(Router::generateUrl("user", "account"));
                    }

                } else {
                    Session::setFlashbag("errorPostOffer", "La quantité saisie n'est pas valide.");
                    $this->redirect(Router::generateUrl("offers", "add"));
                }
            } else {
                Session::setFlashbag("errorPostOffer", "Pour inserer une offre vous devez saisir un titre, une catégorie, une ville et une quantitée.");
                $this->redirect(Router::generateUrl("offers", "add"));
            }
        } else {
            $this->redirect(Router::generateUrl("home"));
        }
    }

    public function checkExchangeAction($params){
        if(isset($params[0])&&is_numeric($params[0])){
            /** @var Offers $offerMnager */
            $offerMnager = $this->loadModel("Offers");
            $isSold = $offerMnager->isSold($params[0]);
            $isExchange = $offerMnager->isExchange($params[0]);
            $quantity = $offerMnager->getQuantity($params[0]);
            $price = $offerMnager->getPrice($params[0]);
            $exchange = $offerMnager->getExchange($params[0]);
            $results = array(
                "is-exchange" => $isExchange,
                "is-sold" => $isSold,
                "quantity" => $quantity
            );

            if ($isExchange) {
                $results['exchange'] = $exchange;
            }

            if ($isSold) {
                $results["price"] = $price;
            }
            echo json_encode($results);
        }
        else {
            $this->requireMethod('errors',"notFoundAction");
        }
    }

    public function addToShoppingChartAction($params)
    {
        if (Session::has("user")) {
            $rquest = $this->request;
            $offerId = $rquest->data->id;
            $quantity = $rquest->data->quantity;


            if (is_numeric($quantity)) {
                /** @var ShoppingCart $shoppingChartManger */
                $shoppingChartManger = $this->loadModel("ShoppingCart");
                $user = Session::get("user");
                /** @var Offers $offersManager */
                $offersManager = $this->loadModel("Offers");
                $offerQuantity = $offersManager->get($offerId)->quantity;
                if ($quantity<=$offerQuantity) {
                    if ($shoppingChartManger->addOffer($offerId, $quantity, $user["id"], (isset($rquest->data->exchange)) ? $rquest->data->exchange : "")) {
                        echo "ok";
                    } else {
                        echo "problem (1)";
                    }
                } else {
                    echo "problem (4)";
                }
            } else {
                echo "problem (2)";
            }
        } else {
            echo "problem(3)";
        }
    }

    public function deleteOfferAction($params) {
        if (Session::has("user")) {
            $user = Session::get("user");
            if (isset($params[0])) {
                /** @var Offers $offersManager */
                $offersManager = $this->loadModel("Offers");
                $ownerId = $offersManager->get($params[0])->owner_id;
                if ($ownerId = $user["id"]) {
                    $offersManager->rm($params[0]);
                }
                $this->redirect(Router::generateUrl("user","account"));
            } else {
                $this->redirect(Router::generateUrl("user","account"));
            }
        }
    }

} 