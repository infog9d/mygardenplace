<?php
/**
 * Created by PhpStorm.
 * User: jaswinder
 * Date: 01/04/2015
 * Time: 10:21
 */

class ProfilController extends Controller {

    public function indexAction($params){
        /** @var Offers $offersManager */


        $offersManager = $this->loadModel("Offers");
        $offre = $offersManager->getLast();
        $this->render("index",array("ads" => $offre));


    }


    public function offerView($params)
    {
        require "..".DS."views".DS."profil".DS."offer.php";
    }
    public function reRegisterAction($params){

        if (isset($this->request->data->firstname)&&
            isset($this->request->data->lastname)&&
            isset($this->request->data->birthdate)&&
            isset($this->request->data->mail)&&
            isset($this->request->data->phone)&&
            isset($this->request->data->cityname)&&
            !empty($this->request->data->firstname)&&
            !empty($this->request->data->lastname)&&
            !empty($this->request->data->birthdate)&&
            !empty($this->request->data->mail)&&
            !empty($this->request->data->phone)&&
            !empty($this->request->data->cityname)) {
            $user=Session::get('user');
            $id = $user["id"];
            /** @var City $cityManager */
            $cityManager = $this->loadModel("City");
            $cities = $cityManager->search($this->request->data->cityname);

            if (count($cities)>0) {
                /** @var User $userManager */
                $userManager = $this->loadModel("user");
                $userManager->set($id,array(
                    "firstname" => $this->request->data->firstname,
                    "lastname"  => $this->request->data->lastname,
                    "birthdate" => $this->request->data->birthdate,
                    "mail"      => $this->request->data->mail,
                    "phone" => $this->request->data->phone,
                    "City_idCity"=> $cities[0]["idCity"],
                ));
                Session::add("user",$userManager->get($user["id"],"array"));
                $this->redirect(Router::generateUrl("user","account"));
            }
        }

    }
    public function reRegisterOnAction($params){
        Session::setFlashbag("registrationSuccess", "Votre demande d'inscription à bien été prise en compte. Un e-mail de validation vous a été envoyé pour confirmer votre inscription.");
        $this->redirect(Router::generateUrl("home"));

    }

}