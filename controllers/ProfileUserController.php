<?php
/**
 * Created by IntelliJ IDEA.
 * Date: 26/05/2015

 * Time: 14:54
 */
class ProfileUserController extends Controller{

    /**
     * @var array
     */

    public $param;


    /**
     * @param $params
     */
    public function indexAction($params){

        if (
            !empty($params[0])&&
            is_numeric($params[0])&&
            $this->checkParam($params[0]))
        {
            $this->param=$params[0];
            /** @var User $userData */
            $userData = $this->loadModel("user");
            $user = $userData->get($params[0]);
            $comments = $userData->getComments($params[0]);
            $grade = $userData->getGrade($comments);

            /** @var Offers $offersManager */
            $offersManager = $this->loadModel("Offers");
            $offers = $offersManager->getLast($params[0]);

            /** @var City $cityManager */
            $cityManager=$this->loadModel("City");
            $idCity=$user->City_idCity;
            $result = $cityManager->search($this->request->data);
            $city=$result[$idCity-1];


            $this->render("index",array(
                "user"=>$user,
                "params"=>$params,
                "city"=>$city,
                "comments"=>$comments,
                "grade"=>$grade,
                "offers"=>$offers
            ));
        }
        else echo $this->requireMethod("errors","notFoundAction");


    }


    /**
     * @param $params
     * @return bool
     */
    public function checkParam($params){

        /** @var User $users */

        $users = $this->loadModel("user");

        $users->listAll();
        if($users->get($params)==false){return false;}
        else return true;
    }


    public function sendAction($params){
        if (Session::has("user")&&isset($this->request->data->message)&&!empty($this->request->data->message)) {
            $content = $this->request->data->message;
            $grade = isset($this->request->data->grade) ? $this->request->data->grade : 0;

            $user = Session::get("user");
            $ownerId = $user["id"];
            $commentedUserId = $this->request->data->ownerId;

            if ($commentedUserId != $ownerId) {
                /** @var Comments $comments */
                $comments = $this->loadModel("Comments");
                $comments->insertComment($grade, $content, $commentedUserId, $ownerId);
                $this->redirect(Router::generateUrl("ProfileUser", "index", array("id" => $commentedUserId)));
            } else {
                Session::setFlashbag("errorComment","Vous ne pouvez pas vous attribuer un commentaire.");
                $this->redirect(Router::generateUrl("ProfileUser", "index", array("id" => $commentedUserId)));
            }
        } else {
            $userId = $this->request->data->ownerId;
            Session::setFlashbag("errorComment","Vous devez être connecté et renseigner un message pour commenter un utilisateur.");
            $this->redirect(Router::generateUrl("ProfileUser", "index", array("id" => $userId)));
        }
    }

}
