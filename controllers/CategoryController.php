<?php


class CategoryController extends Controller {

    public function searchAction($params){
        /** @var Subcategory $categoryManager */
        $categoryManager = $this->loadModel("Subcategory");
        $categories = $categoryManager->search($params[0]);
        echo json_encode($categories);
    }
    
    public function searchCategoriesSubcatagoriesGardenerAction($params) {
        /** @var Subcategory $subcategoryManager */
        $subcategoryManager = $this->loadModel("Subcategory");
        $subcategories = $subcategoryManager->search($params[0]);
        /** @var Category $categoryManager */
        $categoryManager = $this->loadModel("Category");
        $categories = $categoryManager->search($params[0]);
        /** @var User $userManager */
        $userManager = $this->loadModel("user");
        $users = $userManager->search($params[0]);
        for($i=0;$i<count($users);$i++) {
            $users[$i]["name"] = $users[$i]["firstname"]." ".$users[$i]["lastname"];
        }
        echo json_encode(array_merge($subcategories,$categories,$users));
    }

} 