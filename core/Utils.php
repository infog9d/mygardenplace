<?php

/**
 * This class provides methods to convert string and generate csrf tokens.
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 */
class Utils {

    /**
     * convert dashes separate words to camelCase
     * @param $string   string variable to be converted
     * @return string   converted variable
     */
    public static function dashesToCamelCase($string) {
        $explode = explode("-",$string);
        $countExplode = count($explode);
        if ($countExplode>1) {
            for ($i = 1; $i < $countExplode; $i++) {
                $explode[$i] = ucfirst(strtolower($explode[$i]));
            }
            return implode("",$explode);
        }
        return $string;
    }

    /**
     * check if a csrf token is valid
     * @param $name     string  token's name
     * @param $data     string  token's value
     * @param $referrer  string  token's referrer
     * @return boolean   token is valid
     */
    public static function isValidCSRFToken($name, $data, $referrer = "") {
        $csrfname = 'csrf_'.$name;
        if(isset($_SESSION[$name.'_token']) && isset($_SESSION[$name.'_token_time']) && isset($data->$csrfname)) {
            if($_SESSION[$name.'_token'] == $data->$csrfname) {
                if($_SESSION[$name.'_token_time'] >= (time() - 900)) {
                    if($referrer != "") {
                        if ($_SERVER['HTTP_REFERER'] == $referrer) {
                            return true;
                        }
                        return false;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * generate a token
     * @param $name         string token's name
     * @return string       input
     */
    public static function generateCSRFToken($name) {
        $html = "";

        $token = uniqid(rand(), true);
        $_SESSION[$name.'_token'] = $token;
        $_SESSION[$name.'_token_time'] = time();
        $html .= "<input type='hidden' name='csrf_".$name."' value='".$token."' >";

        return $html;
    }

    public static function quoteEach($array){
        function quote($value) {
            return "'".$value."'";
        }
        return array_map("quote",$array);
    }

    public static function checkSelectedCategory($name,$id) {
        if (isset($_GET["categories"])) {
            if (is_array($_GET["categories"])) {
                return in_array($id,$_GET["categories"]);
            } else {
                return $_GET["categories"] == $name;
            }
        }
        return false;
    }

    public static function determineExchangeOrSold() {
        if (isset($_GET["exchange"])||isset($_GET["sold"])) {
            if (isset($_GET["exchange"])&&isset($_GET["sold"])) {
                return Offers::BOTH;
            } else if (isset($_GET["exchange"])&&!isset($_GET["sold"])) {
                return Offers::EXCHANGE;
            } else if (!isset($_GET["exchange"])&&isset($_GET["sold"])) {
                return Offers::SOLD;
            }
        }
        return Offers::BOTH;
    }


    public static function formatSqlDate($date,$format) {
        $date = DateTime::createFromFormat('Y-m-d H:i:s',$date);
        return $date->format($format);
    }

} 