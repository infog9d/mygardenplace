<?php

/**
 * This class provides methods to common features needed in services.
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 */
class Service {

    /**
     * Load a model
     * @param $name string  model's name
     */
    public function loadModel($name) {
        $file = ROOT.DS.'models'.DS.$name.'.php';
        require_once $file;
        return new $name();
    }

    /**
     * Load a service
     * @param $name
     */
    public function loadService($name){
        $file = ROOT.DS.'services'.DS.$name.'.php';
        require_once $file;
    }

    /**
     * redirect to an url
     * @param $url  string url to redirect to
     */
    public function redirect($url) {
        header("Location: ".$url);
    }

} 