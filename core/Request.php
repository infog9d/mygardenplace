<?php

/**
 * Request object
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 */
class Request {

    /**
     * Request url
     * @var string
     */
    public $url;

    /**
     * Post data
     * @var boolean|array
     */
    public $data = false;

    public function __construct() {
        $this->url = isset($_SERVER["REQUEST_URI"])?$_SERVER["REQUEST_URI"]:"/";
        if(!empty($_POST)) {
            $this->data = new stdClass();
            foreach($_POST as $k=>$v) {
                $this->data->$k = $v;
            }
        }
    }
} 