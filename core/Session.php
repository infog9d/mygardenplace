<?php

/**
 * This class provides methods to common features needed in session.
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 */
class Session {

    /**
     * instantiate a session
     */
    public function __construct(){
        session_start();
    }

    /**
     * set flashbag variable
     * @param $name string variable name
     * @param $var  string variable value
     */
    public static function setFlashbag($name,$var) {
        if (!isset($_SESSION["flashbag"])) {
            $_SESSION["flashbag"] = array();
        }
        $_SESSION["flashbag"][$name] = $var;
    }

    /**
     * get flashbag variable
     * @param $name string variable name
     * @return null|string variable value
     */
    public static function getFlashbag($name){
        if (isset($_SESSION["flashbag"][$name])) {
            $var = $_SESSION["flashbag"][$name];
            unset($_SESSION["flashbag"][$name]);
            return $var;
        }
        return null;
    }

    /**
     * check if this session has a flashbag variable
     * @param $name     string variable name
     * @return null|string     variable value
     */
    public static function hasFlashbag($name){
        return isset($_SESSION["flashbag"][$name]);
    }

    /**
     * add a variable
     * @param $name     string variable name
     * @param $value    string variable value
     */
    public static function add($name,$value) {
        $_SESSION[$name] = $value;
    }

    /**
     * check if this session has a variable
     * @param $name     string variable name
     * @return null|string     variable value
     */
    public static function has($name) {
        return isset($_SESSION[$name]);
    }

    /**
     * get a variable
     * @param $name     string variable name
     * @return null|string     variable value
     */
    public static function get($name) {
        if (isset($_SESSION[$name])) {
            return $_SESSION[$name];
        }
        return null;
    }

    /**
     * delete a variable
     * @param $name     string variable name
     */
    public static function rm($name) {
        if (isset($_SESSION[$name])) {
            unset($_SESSION[$name]);
        }
    }

    /**
     * destroy a session
     */
    public static function destroy(){
        session_destroy();
    }
} 