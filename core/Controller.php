<?php

/**
 * This class provides methods to common features needed in controllers.
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 */
class Controller {

    /**
     * Base Url
     * @var string
     */
    public $prefix = DS;

    /**
     * Controller name
     * @var string
     */
    public $name = "";

    /**
     * Html layout
     * @var string
     */
    public $layout = "base";

    /**
     * Path to the layout
     * @var string
     */
    private $layoutFile;

    /**
     * Request object
     * @var Request
     */
    protected $request;

    /**
     * Layout's title
     * @var string
     */
    protected $title;

    /**
     * Css
     * @var array
     */
    protected static $css = array();

    /**
     * Js
     * @var array
     */
    protected static $js = array();

    /**
     * @var string
     */
    protected static $blocks = "";

    /**
     * Initialize some required values
     * @param $prefix           string base url
     * @param $name             string controller name
     * @param Request $request  Request object
     */
    public function __construct($prefix,$name,Request $request){
        $this->prefix = $prefix;
        $this->name = $name;
        $this->request = $request;
        $this->setLayout($this->layout);
    }

    /**
     * Set the layout to render
     * @param $layout   string layout name
     */
    public function setLayout($layout) {
        $this->layoutFile = ROOT.DS."views".DS."layout".DS.$layout;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * Load a model
     * @param $name string model's name
     */
    public function loadModel($name) {
        $file = ROOT.DS.'models'.DS.$name.'.php';
        require_once $file;
        return new $name();
    }

    /**
     * Load a service
     * @param $name
     */
    public function loadService($name){
        $file = ROOT.DS.'services'.DS.$name.'.php';
        require_once $file;
    }

    /**
     * Render a layout
     * @param $content  string
     */
    public function renderLayout($content) {
        $title = $this->title;
        $css = self::requireAdditionalCss();
        $js = self::requireAdditionalJs();
        $blocks = self::$blocks;
        if (file_exists($this->layoutFile.".php")) {
            require $this->layoutFile.".php";
        } else {
            header("HTTP/1.1 404 Not Found");
        }
    }

    /**
     * Render a view
     * @param string $view      view's name
     * @param array $params     parameters
     */
    public function render($view, $params = array()) {
        $name = ROOT.DS."views".DS.strtolower($this->name).DS.$view;
        //$fileName = BASE_URL.DS."views".DS.strtolower($this->name).DS.$view;
        //$fileName = ROOT.DS."views".DS.strtolower($this->name).DS.$view;
        $file = $name.".php";
        if (file_exists($file)) {
            ob_start();
            require $file;
            $content = ob_get_clean();
            $this->renderLayout($content);
        }
    }

    /**
     * Render a particular method
     * @param $controller   string  controller's name
     * @param $method       string  method's name
     * @param array $params         parameters
     */
    public static function requireMethod($controller, $method, $params = array()){
        if (file_exists(ROOT.DS."controllers".DS.ucfirst($controller)."Controller.php")) {
            require_once ROOT.DS."controllers".DS.ucfirst($controller)."Controller.php";
            $name = ucfirst($controller)."Controller";
            $controller = new $name("",$controller,new Request());

            if (method_exists($controller,$method)) {
                $controller->$method($params);
            }
        }
    }

    public static function addCss($name){
        self::$css[] = $name;
    }

    public static function requireAdditionalCss(){
        $html = "";
        foreach (self::$css as $css) {
            $html .= self::requireCss($css);
        }
        return $html;
    }

    public static function requireCss($css){
        if (strpos($css,".css")===false) {
            $css .= ".css";
        }
        return '<link rel="stylesheet" href="'.BASE_URL.'/assets/css/'.$css.'">'." \n";
    }

    public static function addJs($name){
        self::$js[] = $name;
    }

    public static function requireAdditionalJs(){
        $html = "";
        foreach (self::$js as $js) {
            $html .= self::requireJs($js);
        }
        return $html;
    }

    public static function requireJs($js){
        if (strpos($js,".js")===false) {
            $js .= ".js";
        }
        return '<script type="text/javascript" src="'.BASE_URL.'/assets/js/'.$js.'"></script>'." \n";
    }

    public static function beginJsBlock(){
        ob_start();
    }

    public static function endJsBlock(){
        self::$blocks .= ob_get_clean()." \n";
    }

    /**
     * Redirect
     * @param $url  string  path to redirect
     */
    public function redirect($url) {
        header("Location: ".$url);
    }

} 