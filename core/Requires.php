<?php
/**
 * Requires classes
 */
require_once "Session.php";
new Session();
require_once ROOT.DS."config".DS."config.php";
require_once "Utils.php";
require_once "Request.php";
require_once "Router.php";
require_once "Controller.php";
require_once "Model.php";
require_once "Service.php";
