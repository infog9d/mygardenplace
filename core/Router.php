<?php

/**
 * This class instantiate a controller and call the right method according to the url path
 * Examples :
 * /controller/action/parameter0/parameter1
 * Default :
 * Route        Method
 * /            /home/index
 * /controller  /controller/index
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 */
class Router {

    /**
     * Base url
     * @var string
     */
    public $prefix = DS;

    /**
     * request object
     * @var string
     */
    private $request;

    /**
     * @param Request $request  the request object
     */
    public function __construct(Request $request){
        $this->request = $request;
        $this->dispatch($request);
    }

    /**
     * Analyze url path
     * @param Request $request  the request object
     */
    public function dispatch(Request $request){
        $url = $request->url;

        $hasGet = strpos($url, "?");
        if ($hasGet !== false) {
            $url = substr($url, 0, $hasGet);
        }
        $url = trim($url, "/");
        $find = explode("/",$url);

        if (count($find)>=1) {
            if (file_exists("..".DS."controllers".DS.ucfirst($find[0])."Controller.php")) {
                $this->parseArray($find);
            } else {
                $this->prefix .= $find[0].DS;
                array_shift($find);
                if (count($find)==0) {
                    $find = array();
                    $find[0] = Config::$defaultController;
                }
                $this->parseArray($find);
            }
        } else {
            if (file_exists("..".DS."controllers".DS.ucfirst(Config::$defaultController)."Controller.php")) {
                $find = array();
                $find[0] = Config::$defaultController;
                $this->parseArray($find);
            } else {
                $this->triggerError("404");
            }
        }

    }

    /**
     * instantiate and call the action
     * @param $find array   url components
     */
    public function parseArray($find) {
        if (file_exists("..".DS."controllers".DS.ucfirst($find[0])."Controller.php")) {
            require "..".DS."controllers".DS.ucfirst($find[0])."Controller.php";
            $name = ucfirst($find[0])."Controller";
            $controller = new $name($this->prefix,$find[0],$this->request);
            if (count($find)>=2) {
                $saveIfParams = $find[1];
                $find[1] = Utils::dashesToCamelCase($find[1]);
                $name = $find[1]."Action";
                if (method_exists($controller,$name)) {
                    array_shift($find);
                    array_shift($find);

                    $controller->$name($find);
                } else {
                    $find[1] = $saveIfParams;
                    array_shift($find);
                    $controller->indexAction($find);
                }
            } else {
                if (method_exists($controller,"indexAction")) {
                    $controller->indexAction(array());
                } else {
                    $this->triggerError("404");
                }
            }
        } else {
            $this->triggerError("404");
        }
    }

    /**
     * generate an error
     * @param $code int error code
     */
    public function triggerError($code){
        switch($code) {
            case "404":
                header("HTTP/1.1 404 Not Found");
                Controller::requireMethod("errors", "notFoundAction");
                break;
        }
    }

    /**
     * generate an url
     * @param string $controller    controller name
     * @param string $action        action name
     * @param array $params         parameters
     * @return string               url path
     */
    public static function generateUrl($controller, $action = "", $params = array()){
        if ($action != "" || $action == "index") {
            if (count($params)>0) {
                $more = implode("/",$params);
                return BASE_URL."/".$controller."/".$action."/".$more;
            }

            return BASE_URL."/".$controller."/".$action;
        } else {
            if (count($params)>0) {
                $more = implode("/",$params);
                return BASE_URL."/".$controller."/".$more;
            }

            return BASE_URL."/".$controller;
        }
    }

} 