<?php

/**
 * This class provides methods to common features needed in models.
 * @author Jean-Baptiste WATENBERG <a href="mailto:jeanbaptiste.watenberg@gmail.com">jeanbaptiste.watenberg@gmail.com</a>
 */
class Model {

    /**
     * connection between PHP and a database server
     * @var PDO object
     */
    protected $dbs;

    /**
     * initiates the connection
     */
    public function __construct() {
        $database = Config::$database;
        $pdo = new PDO('mysql:host=' . $database['host'] . ';dbname=' . $database['database'] . ';',
            $database["user"],
            $database['password'],
            array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')
        );

        if (Config::$debug) {
            $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        }

        $this->dbs = $pdo;
    }

    /**
     * Transform a string in valid url string
     * @param $str              string      variable to sanitize
     * @return mixed|string     sanitized variable
     */
    public function slugIt($str) {
        $str = utf8_decode($str);
        $str = strtolower(strtr($str, utf8_decode('ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ()[]\'"~$&%*@ç!?;,:/\^¨€{}<>|+.- '),  'aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn    --      c  ---    e       --'));
        $str = str_replace(' ', '', $str);
        $str = str_replace('---', '-', $str);
        $str = str_replace('--', '-', $str);
        $str = trim($str,'-');

        return $str;
    }

    /**
     * Sanitize variable
     * @param $var      string variable to sanitize
     * @return string   sanitized variable
     */
    public function secureVar($var){
        $var = strip_tags($var);
        return addslashes($var);
    }

    /**
     * Load a service
     * @param $name
     */
    public function loadService($name){
        $file = ROOT.DS.'services'.DS.$name.'.php';
        require_once $file;
    }

    /**
     * Load a model
     * @param $name string model's name
     */
    public function loadModel($name) {
        $file = ROOT.DS.'models'.DS.$name.'.php';
        require_once $file;
        return new $name();
    }


} 