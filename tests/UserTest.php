<?php
require_once "/home/jb/local.dev/mygardenplace/config/config.php";
require_once "/home/jb/local.dev/mygardenplace/core/Model.php";
require_once "/home/jb/local.dev/mygardenplace/models/user.php";

class UserTest extends PHPUnit_Framework_TestCase {

    private $userManager;

    public function __construct() {
        $this->userManager = new User();
    }

    public function testGet() {
        $user = $this->userManager->get(1,"array");
        $this->assertArrayHasKey("firstname",$user);
        $this->assertContains("Jean-Baptiste",$user["firstname"]);
        $user = $this->userManager->get(3,"array");
        $this->assertFalse($user);
    }

    public function testGetComments() {
        $comments = $this->userManager->getComments(1);
        if (count($comments)>0) {
            $this->assertArrayHasKey("grade",$comments[0]);
            $this->assertArrayHasKey("content",$comments[0]);
            $this->assertArrayHasKey("date",$comments[0]);
            $this->assertArrayHasKey("commented_user_firstname",$comments[0]);
            $this->assertArrayHasKey("commented_user_lastname",$comments[0]);
            $this->assertArrayHasKey("commented_user_picture",$comments[0]);
            $this->assertArrayHasKey("owner_firstname",$comments[0]);
            $this->assertArrayHasKey("owner_lastname",$comments[0]);
            $this->assertArrayHasKey("owner_picture",$comments[0]);
        }
    }

    public function testGrade() {
        $comments = array(
            array("id"=>1,"content"=>"","grade"=>1),
            array("id"=>2,"content"=>"","grade"=>2),
            array("id"=>3,"content"=>"","grade"=>3),
        );

        $this->assertEquals((1+2+3)/3,$this->userManager->getGrade($comments));
    }

    /**
     * @expectedException Exception
     */
    public function testGradeException() {
        $comments = array(
            array("id"=>1,"content"=>"","grade"=>1),
            array("id"=>2,"content"=>"","grade"=>2),
            array("id"=>3,"content"=>"")
        );

        $this->userManager->getGrade($comments);
    }

}