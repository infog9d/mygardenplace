<?php

/**
 * Class user
 */
class User extends Model {

    public function listAll(){
        $sql = 'SELECT * FROM user';
        $getUsers = $this->dbs->prepare($sql);
        $getUsers->execute(array());
        return $getUsers->fetchAll();
    }

    public function listAlertsByUser() {
        $users = $this->listAll();
        $getAlerts = "SELECT alerts.keyword FROM alerts WHERE user_id = :userId";
        $requestGetAlerts = $this->dbs->prepare($getAlerts);
        for ($i=0; $i<count($users);$i++) {
            $userId = $users[$i]['id'];
            $requestGetAlerts->execute(array("userId"=>$userId));
            $users[$i]['alerts'] = $requestGetAlerts->fetchAll();
        }
        return $users;
    }

    public function listAlerts($userId) {
        $getAlerts = "SELECT alerts.keyword, alerts.id FROM alerts WHERE user_id = :userId";
        $requestGetAlerts = $this->dbs->prepare($getAlerts);
        $requestGetAlerts->execute(array("userId"=>$userId));
        return $requestGetAlerts->fetchAll();
    }

    public function insertAlert($userId, $alert) {
        $getAlert = "SELECT * FROM alerts WHERE user_id = $userId AND keyword = '$alert'";
        $insertAlert = "INSERT INTO alerts VALUES (NULL,'$alert',$userId)";
        $requestGet = $this->dbs->prepare($getAlert);
        $requestInsert = $this->dbs->prepare($insertAlert);
        try {
            $requestGet->execute(array());
            $result = $requestGet->fetch();
            if ($result) {
                return true;
            } else {
                $requestInsert->execute(array());
                return true;
            }
        } catch (PDOException $e) {
            return false;
        }
    }

    public function search($name){
        $this->secureVar($name);
        $name = explode(" ", $name);
        if (count($name)>1) {
            $sql = 'SELECT user.firstname, user.lastname, user.id FROM user WHERE user.firstname LIKE "%'.$name[0].'%" OR user.lastname LIKE "%'.$name[1].'%"';
        } else {
            $sql = 'SELECT user.firstname, user.lastname, user.id FROM user WHERE user.firstname LIKE "%'.$name[0].'%" OR user.lastname LIKE "%'.$name[0].'%"';
        }
        $categories = $this->dbs->prepare($sql);
        $categories->execute(array());
        return $categories->fetchAll();
    }

    public function isValid($mail, $password){
        $sql = 'SELECT u.*, City.name cityname FROM user AS u INNER JOIN City ON City.idCity=u.City_idCity WHERE u.mail = :mail AND u.password = :password';
        $getUser = $this->dbs->prepare($sql);
        $getUser->execute(array(
            "mail" => $mail,
            "password" => sha1($password)
        ));
        $results = $getUser->fetchAll();
        if (count($results) == 1 && $results[0]["valid"] && $results[0]["mailvalid"]) {
            return $results[0];
        }
        return null;
    }

    public function isAdmin($mail, $password){
        $sql = 'SELECT * FROM user AS u WHERE u.mail = :mail AND u.password = :password';
        $getUser = $this->dbs->prepare($sql);
        $getUser->execute(array(
            "mail" => $mail,
            "password" => sha1($password)
        ));
        $results = $getUser->fetchAll();
        return count($results) == 1 && $results[0]["valid"] && $results[0]["mailvalid"] && $results[0]["is_admin"];
    }

    public function mailExist($mail){
        $sql = 'SELECT mail FROM user WHERE mail = :mail';
        $getUser = $this->dbs->prepare($sql);
        $getUser->execute(array(
            "mail" => $mail
        ));
        $results = $getUser->fetchAll();
        return count($results) == 1;
    }

    public function register($firstname, $lastname, $birthdate, $mail, $cityId, $password, $phone){
        if (preg_match("/([A-Za-z-_.0-9]+@[A-Za-z-_.0-9]+)/",$mail)
            &&!$this->mailExist($mail)
            &&preg_match("/^((\\d{2}){5})|((\\d{2}).){4}(\\d{2})$/",$phone)
            &&preg_match("/^\\d{1,2}\\/\\d{1,2}\\/\\d{4}$/",$birthdate)
        ) {
            $sql = 'INSERT INTO user (
                firstname,
                lastname,
                password,
                mail,
                  phone,
                  birthdate,
                  valid,
                  token,
                  mailvalid,
                  is_admin,
                  City_idCity,
                  registred_on
              ) VALUES (
                  :firstname,
                  :lastname,
                  :password,
                  :mail,
                  :phone,
                  :birthdate,
                  :valid,
                  :token,
                  :mailvalid,
                  :is_admin,
                  :cityId,
                  :registredOn
              )';

            $newUser = $this->dbs->prepare($sql);
            $dateTime = DateTime::createFromFormat("d/m/Y",$birthdate);
            $now = new DateTime();
            $token = sha1(uniqid());
            $newUser->execute(array(
                "firstname"      => self::secureVar($firstname),
                "lastname"       => self::secureVar($lastname),
                "password"       => sha1($password),
                "mail"           => $mail,
                "phone"          => self::secureVar($phone),
                "birthdate"      => $dateTime->format("Y-m-d H:i:s"),
                "valid"          => true,
                "token"          => $token,
                "mailvalid"      => false,
                "is_admin"       => false,
                "cityId"         => $cityId,
                "registredOn"    => $now->format("Y-m-d H:i:s")
            ));
            $this->loadService("Mailer");
            $mailer = new Mailer($mail,Config::$from);
            $mailer->setView("registerConfirmation",array(
                "firstname" => $firstname,
                "lastname"  => $lastname,
                "token"     => $token
            ));
            $mailer->send("[mygardenplace] Confirmation de votre inscription");
        }
        return null;
    }
    public function validateMail($token){
        $sql = 'SELECT * FROM user WHERE token = :token';
        $validIt = $this->dbs->prepare($sql);
        try {
            $validIt->execute(array(
                "token" => $token
            ));
        } catch(PDOException $e) {
            throw new ErrorException("an error occurred");
        }
        $results = $validIt->fetchAll();
        if (count($results) == 1) {
            $registredOn = new DateTime($results[0]["registred_on"]);
            $now = new DateTime();
            //token is valid for 76 hours
            if ($now->getTimestamp() <= $registredOn->getTimestamp() + 76*60*60) {
                $sqlUp = 'UPDATE user SET user.mailvalid = :mailValid, user.token = "" WHERE token = :token';
                $valid = $this->dbs->prepare($sqlUp);
                try {
                    $valid->execute(array(
                        "mailValid" => true,
                        "token" => $token
                    ));
                    return true;
                } catch (PDOException $e) {
                    throw new ErrorException("an error occurred");
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function get($id,$type="object") {
        $sql = 'SELECT user.*, City.name cityname FROM user INNER JOIN City ON City.idCity = user.City_idCity WHERE user.id = :id';
        $request = $this->dbs->prepare($sql);
        $request->execute(array(
            "id" => $this->secureVar($id)
        ));
        if ($type=="object") {
            return $request->fetchObject();
        } else {
            return $request->fetch();
        }
    }

    public function set($id, $array) {
        if (is_array($array)) {
            if (count($array)>0) {
                $sql = 'UPDATE user SET ';
                $numberOfields = count($array);
                $currentIteration = 1;
                foreach ($array as $column => $value) {

                    $sql .= $column.' = "'.$this->secureVar($value).'"';
                    if ($currentIteration < $numberOfields) {
                        $sql .= ", ";
                    } else {
                        $sql .= " ";
                    }
                    $currentIteration++;
                }
                $sql .= 'WHERE id = '.$id;

                $this->dbs->exec($sql);
                return true;
            }
        }
        throw new Exception("Error : wrong parameters");
    }


    public function getAlert($id) {
        $deleteUser = 'SELECT * FROM alerts WHERE id = '. $id;
        try {
            $request = $this->dbs->query($deleteUser);
            return $request->fetch();
        } catch(PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }

    public function rmAlert($id) {
        $deleteUser = 'DELETE FROM alerts WHERE id = '. $id;

        try {
            $this->dbs->exec($deleteUser);
        } catch(PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }

    public function rm($id) {
        $deleteUser = 'DELETE FROM user WHERE id = '. $id;

        try {
            $this->dbs->exec($deleteUser);
        } catch(PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }

    public function getPicture($pictureResponse){
        $sql = "SELECT user.picture FROM user  WHERE response.user_id = : id ";
        try{
        $request = $this->dbs->prepare($sql);
            $request->execute(array(
                "id"=>$pictureResponse));

        }
    catch (PDOException $e) {

        return $request->fetch();}
    }

    public function getPicureByPost($picturePost){
        $sql = "SELECT user.picture FROM user
        WHERE post.user_id = :id";
        $request = $this->dbs->prepare($sql);
        $request = $this->excute(array(
        "id" =>$picturePost
        ));
        return $request->fetch();

    }

    public function getComments($id) {
        $sql = "SELECT comments.*,commentedUser.firstname commented_user_firstname,commentedUser.lastname commented_user_lastname,commentedUser.picture commented_user_picture,owner.firstname owner_firstname,owner.lastname owner_lastname,owner.picture owner_picture FROM comments INNER JOIN user AS commentedUser ON commentedUser.id = comments.commented_user_id INNER JOIN user AS owner ON owner.id = comments.owner_id WHERE commented_user_id = ".$id." ORDER BY date DESC LIMIT 0,5";
        $request = $this->dbs->query($sql);
        return $request->fetchAll();
    }

    /**
     * @param $comments
     * @return float|int
     * @throws Exception
     */
    public function getGrade($comments) {
        $total = 0;
        foreach($comments as $comment) {
            if(isset($comment["grade"])) {
                $total += (int)$comment["grade"];
            } else {
                throw new Exception("Invalid array");
            }
        }
        return (count($comments)>0)?$total/count($comments):0;
    }

} 