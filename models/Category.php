<?php

class Category extends Model {

    public function search($name){
        $sql = 'SELECT id,name FROM category WHERE name LIKE "%'.$name.'%"';
        $categories = $this->dbs->prepare($sql);
        $categories->execute(array());
        return $categories->fetchAll();
    }

    public function findAll(){
        $sql = 'SELECT * FROM category';
        $getUsers = $this->dbs->prepare($sql);
        $getUsers->execute(array());
        return $getUsers->fetchAll();
    }

    public function insert($title) {
        $title = $this->secureVar($title);
        $sql = "INSERT INTO category VALUES (NULL,'".$title."',1)";
        $this->dbs->exec($sql);
        return true;
    }

    public function get($id){
        $sql = "SELECT * FROM category WHERE category.id = :id";
        $request = $this->dbs->prepare($sql);
        $request->execute(array(
            "id" => $id
        ));
        return $request->fetchObject();
    }

    public function set($id, $array) {
        if (is_array($array)) {
            if (count($array)>0) {
                $sql = 'UPDATE category SET ';
                $numberOfields = count($array);
                $currentIteration = 1;
                foreach ($array as $column => $value) {

                    $sql .= $column.' = "'.$this->secureVar($value).'"';
                    if ($currentIteration < $numberOfields) {
                        $sql .= ", ";
                    } else {
                        $sql .= " ";
                    }
                    $currentIteration++;
                }
                $sql .= 'WHERE id = '.$id;

                $this->dbs->exec($sql);
                return true;
            }
        }
        throw new Exception("Error : wrong parameters");
    }

    public function rm($id) {
        $deleteOffer = 'DELETE FROM category WHERE id = '. $id;

        try {
            $this->dbs->exec($deleteOffer);
        } catch(PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }
}