<?php

class Subcategory extends Model {

    public function listAll() {
        $sql = 'SELECT id,name FROM subcategory ORDER BY name ASC';
        $getSubcategories = $this->dbs->prepare($sql);
        $getSubcategories->execute();
        return $getSubcategories->fetchAll();
    }

    public function findAll()
    {
        $sql = "SELECT subcategory.*, category.name category_name FROM subcategory INNER JOIN category ON category.id = subcategory.category_id";
        return $this->dbs->query($sql)->fetchAll();
    }

    public function get($id)
    {
        $sql = "SELECT subcategory.*, category.name category_name FROM subcategory INNER JOIN category ON category.id = subcategory.category_id WHERE subcategory.id = $id";
        return $this->dbs->query($sql)->fetch();
    }

    public function search($name){
        $sql = 'SELECT id,name FROM subcategory WHERE name LIKE "%'.$name.'%"';
        $categories = $this->dbs->prepare($sql);
        $categories->execute(array());
        return $categories->fetchAll();
    }

    public function insertExisting(){
        $data = "les racines:Carottes, betterave rouge, céleri rave, navets, radis, raifort, salsifis
les feuilles:Chou rouge, chou vert, chou blanc, cresson, épinards, feuille de bette, laitue et salades diverses, oseille
les bulbes:Ail, échalotes, fenouil, oignons, petits oignons, poireaux
les tubercules:Pommes de terre, crosne
les rhizomes:Asperges, endives
les bourgeons et les fleurs:Choux de Bruxelles, brocolis, artichauts, chou-fleur
les fruits légumiers:Aubergines, concombre, courgette, poivrons, potirons et citrouille, pâtisson, haricots verts, tomates
les tiges et les cotes:Céleri branche, cotes de bette
les graines:Petits pois, haricots à égrener, fèves
les pousses de graines germées:Pousse de blé, pousse de soja
les algues:Algues brunes (kumbo, aramé, iziki,...), algues rouges (nori, agar-agar, dulce), algues vertes (nori vert, laitue de mer)
les champignons:Champignons de paris, cèpes, morilles
les fruits:Pommes, poires, pêches, prunes";
        $parsedData = explode("\n",$data);
        $trueData = array();
        foreach($parsedData as $d) {
            $res = explode(":",$d);
            $trueData[$res[0]] = explode(", ",$res[1]);
        }

        foreach ($trueData as $category => $subcategories) {
            $sql= "SELECT id FROM category WHERE name='$category'";
            $request = $this->dbs->prepare($sql);
            $request->execute(array());
            $res = $request->fetchObject();
            foreach ($subcategories as $subcategory) {
                $sql = "INSERT INTO subcategory VALUES (NULL, '$subcategory', 1, ".$res->id.")";
                $request = $this->dbs->prepare($sql);
                $request->execute(array());
            }
        }
    }

    public function insert($title,$categoryId) {
        $title = $this->secureVar($title);
        $sql = "INSERT INTO subcategory VALUES (NULL,'".$title."',1,'".$categoryId."')";
        $this->dbs->exec($sql);
        return true;
    }

    public function set($id, $array) {
        if (is_array($array)) {
            if (count($array)>0) {
                $sql = 'UPDATE subcategory SET ';
                $numberOfields = count($array);
                $currentIteration = 1;
                foreach ($array as $column => $value) {

                    $sql .= $column.' = "'.$this->secureVar($value).'"';
                    if ($currentIteration < $numberOfields) {
                        $sql .= ", ";
                    } else {
                        $sql .= " ";
                    }
                    $currentIteration++;
                }
                $sql .= 'WHERE id = '.$id;

                $this->dbs->exec($sql);
                return true;
            }
        }
        throw new Exception("Error : wrong parameters");
    }

    public function rm($id) {
        $deleteOffer = 'DELETE FROM subcategory WHERE id = '. $id;

        try {
            $this->dbs->exec($deleteOffer);
        } catch(PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }

} 