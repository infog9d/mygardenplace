<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 11/05/15
 * Time: 09:40
 */

class Booking extends Model {

    public function listByUser($userId) {
        $sql = "SELECT * FROM booking WHERE user_id = ".$userId;
        $request = $this->dbs->query($sql);
        if ($request) {
            if ($results = $request->fetchAll()) {
                return $results;
            }
        }
        return array();
    }

    public function getDetails($bookingId) {
        $sql = "SELECT booking_offer.* FROM booking_offer INNER JOIN booking ON booking.id = booking_offer.booking_id WHERE booking.id = $bookingId";
        $request = $this->dbs->query($sql);
        if ($request) {
            if ($results = $request->fetchAll()) {
                return $results;
            }
        }
        return array();
    }

    public function add($total,$userId,$offers){
        $now  = new DateTime();
        $now = $now->format("Y-m-d H:i:s");
        $insertBooking = "INSERT INTO booking VALUES (NULL,:total,:now,:userId,0);";
        $selectLastBookingId = "SELECT id FROM booking WHERE user_id = :userId";
        $insertBookingOffers = "INSERT INTO booking_offer VALUES (NULL,:quantity,:type,:bookingId,:offerId,:exchangeAgainst)";
        $insertBookingRequest = $this->dbs->prepare($insertBooking);
        $insertBookingOffersRequest = $this->dbs->prepare($insertBookingOffers);
        $selectLastBookingIdRequest = $this->dbs->prepare($selectLastBookingId);
        try {
            $insertBookingRequest->execute(array(
                "total" => $total,
                "userId" => $userId,
                "now" => $now
            ));
            $selectLastBookingIdRequest->execute(array(
                "userId" => $userId
            ));
            $result = $selectLastBookingIdRequest->fetch();
            $bookingId = $result["id"];
            foreach($offers as $offer) {
                $parameters = array(
                    "quantity" => $offer["quantity"],
                    "bookingId" => $bookingId,
                    "offerId" => $offer["id"],
                    "exchangeAgainst" => "NULL"
                );
                if (isset($offer["exchangeAgainst"])) {
                    //this offer will be exchanged
                    $parameters["type"] = "exchange";
                    $parameters["exchangeAgainst"] = $offer["exchangeAgainst"];
                } else {
                    //this offer has been bought and will be payed
                    $parameters["type"] = "price";
                }
                $insertBookingOffersRequest->execute($parameters);
                /*

                    UPDATE OFFER'S QUANTITY

                */

                /** @var Offers $offersManager */
                $offersManager = $this->loadModel("Offers");
                $offerQuantity = $offersManager->get($offer["id"])->quantity;
                $offersManager->set($offer["id"],array(
                    "quantity" => $offerQuantity-$offer["quantity"]
                ));
            }



            return true;
        } catch(PDOException $e) {
            return false;
        }
    }

}