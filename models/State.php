<?php

/**
 * Class State
 */
class State extends Model {

    public function listState(){
        $sql = 'SELECT State.name FROM State';
        $state = $this->dbs->prepare($sql);
        $state->execute();
        $state = $state->fetchAll();
        return $state;
    }

} 