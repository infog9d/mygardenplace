<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 06/05/15
 * Time: 11:23
 */

class Response extends Model
{

    public function get($id){
        $sql = "SELECT * FROM response WHERE response.id = :id";
        $request = $this->dbs->prepare($sql);
        $request->execute(array(
            "id" => $id
        ));
        return $request->fetchObject();
    }

    public function set($id, $array) {
        if (is_array($array)) {
            if (count($array)>0) {
                $sql = 'UPDATE response SET ';
                $numberOfields = count($array);
                $currentIteration = 1;
                foreach ($array as $column => $value) {

                    $sql .= $column.' = "'.$this->secureVar($value).'"';
                    if ($currentIteration < $numberOfields) {
                        $sql .= ", ";
                    } else {
                        $sql .= " ";
                    }
                    $currentIteration++;
                }
                $sql .= 'WHERE id = '.$id;

                $this->dbs->exec($sql);
                return true;
            }
        }
        throw new Exception("Error : wrong parameters");
    }

    public function listResponseByPost($postId)
    {
        $user = Session::get("user");
        if ($user["is_admin"]) {
            $sql = "SELECT response.*, user.firstname, user.lastname, user.picture FROM response INNER JOIN user ON user.id = response.user_id WHERE post_id = :postId ";
        } else {
            $sql = "SELECT response.*, user.firstname, user.lastname, user.picture FROM response INNER JOIN user ON user.id = response.user_id WHERE post_id = :postId AND response.valid = 1";
        }
        $request = $this->dbs->prepare($sql);
        $request->execute(array(
            "postId" => $postId
        ));
        return $request->fetchAll();
    }


    public function rm($id) {
        $delete = 'DELETE FROM response WHERE id = '. $id;

        try {
            $this->dbs->exec($delete);
        } catch(PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }

    public function numbResponsesByPost($nbResponse)
    {
        $sql = "SELECT COUNT(id) FROM response WHERE post_id = :id AND response.valid = 1";
        $request = $this->dbs->prepare($sql);
        $request->execute(array(
            "id" => $nbResponse
        ));
        return $request->fetch();

    }

    public function insertResponses($content, $postId, $userId) {
        $sql = "INSERT INTO response(id, posted_on, content, valid, post_id, user_id)  VALUES (NULL , :posted_on , :content, 1, :post_id, :user_id)";
        $now = new DateTime();
        $insertPostsResquest = $this->dbs->prepare($sql);
        $insertPostsResquest->execute(array(
            "posted_on" => $now->format("Y-m-d H:i:s"),
            "content" => $content,
            "post_id" => $postId,
            "user_id" => $userId,
            ));
        return true;
    }

    public function lastResponseByPosts($lastResponse){
        $user = Session::get("user");
        if ($user["is_admin"]) {
            $sql= "SELECT response.content,posted_on FROM response WHERE post_id = :id ORDER BY UNIX_TIMESTAMP(posted_on) DESC ";
        } else {
            $sql= "SELECT response.content,posted_on FROM response WHERE post_id = :id AND response.valid = 1 ORDER BY UNIX_TIMESTAMP(posted_on) DESC ";
        }
        $request = $this->dbs->prepare($sql);
        $request->execute( array(
           "id" => $lastResponse
        ));
        return $request->fetch();

    }

    public function lastResponseByTopics($lastResponseByTopics){
        $user = Session::get("user");
        if ($user["is_admin"]) {
            $sql = "SELECT response.content FROM response
                INNER JOIN post ON post.id = response.post_id
                INNER JOIN topics ON idtopics = topics_id
                WHERE topics_id=".$lastResponseByTopics." ORDER BY UNIX_TIMESTAMP(response.posted_on) DESC";
        } else {
            $sql = "SELECT response.content FROM response
                INNER JOIN post ON post.id = response.post_id
                INNER JOIN topics ON idtopics = topics_id
                WHERE response.valid = 1 AND topics_id=".$lastResponseByTopics." ORDER BY UNIX_TIMESTAMP(response.posted_on) DESC";
        }
        $request = $this->dbs->query($sql);
        return $request->fetch();

    }

}

