<?php

class Faq extends Model {

    public function findAll()
    {
        $sql = "SELECT * FROM FAQ";

        $request = $this->dbs->prepare($sql);
        $request->execute(array());
        return $request->fetchAll();
    }

    public function insert($title,$content) {
        $title = $this->secureVar($title);
        $content = $this->secureVar($content);
        $sql = "INSERT INTO FAQ VALUES (NULL,'".$content."','".$title."')";
        $this->dbs->exec($sql);
        return true;
    }

    public function get($id){
        $sql = "SELECT * FROM FAQ WHERE FAQ.id = :id";
        $request = $this->dbs->prepare($sql);
        $request->execute(array(
            "id" => $id
        ));
        return $request->fetchObject();
    }

    public function set($id, $array) {
        if (is_array($array)) {
            if (count($array)>0) {
                $sql = 'UPDATE FAQ SET ';
                $numberOfields = count($array);
                $currentIteration = 1;
                foreach ($array as $column => $value) {

                    $sql .= $column.' = "'.$this->secureVar($value).'"';
                    if ($currentIteration < $numberOfields) {
                        $sql .= ", ";
                    } else {
                        $sql .= " ";
                    }
                    $currentIteration++;
                }
                $sql .= 'WHERE id = '.$id;

                $this->dbs->exec($sql);
                return true;
            }
        }
        throw new Exception("Error : wrong parameters");
    }

    public function rm($id) {
        $deleteOffer = 'DELETE FROM FAQ WHERE id = '. $id;

        try {
            $this->dbs->exec($deleteOffer);
        } catch(PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }

}