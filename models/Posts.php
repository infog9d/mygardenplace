<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 06/05/15
 * Time: 11:23
 */

class Posts extends Model{


    public function listPostsByTopic($topicId) {
        $user=Session::get("user");
        if ($user["is_admin"]) {
            $sql = "SELECT post.*, user.firstname, user.lastname, user.picture FROM post INNER JOIN user ON user.id = post.user_id WHERE topics_id = :topicId";
        } else {
            $sql = "SELECT post.*, user.firstname, user.lastname, user.picture FROM post INNER JOIN user ON user.id = post.user_id WHERE topics_id = :topicId AND post.valid = 1";
        }
        $request = $this->dbs->prepare($sql);
        $request->execute(array(
            "topicId" => $topicId
        ));
        return $request->fetchAll();
    }

    public function insertPosts ($title,$content, $userId,$topicsId) {
        $sql = "INSERT INTO post(id, posted_on, title, content, valid, topics_id, user_id)  VALUES (NULL , :posted_on , :title, :content, 1, :topics_id, :user_id)";
        $now = new DateTime();
        $insertPostsResquest = $this->dbs->prepare($sql);
        $insertPostsResquest->execute(array(
            "posted_on" => $now->format("Y-m-d H:i:s"),
            "title" => $title,
            "content" => $content,
            "topics_id" => $topicsId,
            "user_id" => $userId
        ));
        return true;
    }

    public function getPost($id)
    {
        $user = Session::get("user");
        if ($user["is_admin"]) {
            $sql = "SELECT * FROM post WHERE id = :identifiant";
        } else {
            $sql = "SELECT * FROM post WHERE id = :identifiant AND post.valid = 1";
        }
        $request = $this->dbs->prepare($sql);
        $request->execute(array(
            "identifiant"=>$id));
        return $request->fetch();
    }

    public function get($id){
        $sql = "SELECT * FROM post WHERE post.id = :id";
        $request = $this->dbs->prepare($sql);
        $request->execute(array(
            "id" => $id
        ));
        return $request->fetchObject();
    }

    public function set($id, $array) {
        if (is_array($array)) {
            if (count($array)>0) {
                $sql = 'UPDATE post SET ';
                $numberOfields = count($array);
                $currentIteration = 1;
                foreach ($array as $column => $value) {

                    $sql .= $column.' = "'.$this->secureVar($value).'"';
                    if ($currentIteration < $numberOfields) {
                        $sql .= ", ";
                    } else {
                        $sql .= " ";
                    }
                    $currentIteration++;
                }
                $sql .= 'WHERE id = '.$id;

                $this->dbs->exec($sql);
                return true;
            }
        }
        throw new Exception("Error : wrong parameters");
    }

    public function rm($id) {
        $delete = 'DELETE FROM post WHERE id = '. $id;

        try {
            $this->dbs->exec($delete);
        } catch(PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }

    public function numberResponsesAppearence($id){
        $sql = "SELECT COUNT(response.id) FROM response
                WHERE response.valid = 1 AND post_id =".$id;
        $request = $this->dbs->query($sql);
        return $request->fetch();
    }

}