<?php
/**
 * Created by PhpStorm.
 * User: jaswinder
 * Date: 14/04/2015
 * Time: 15:08
 */

class ShoppingCart extends Model
{
    public function addOffer($id, $quantity, $userId, $exchange = "")
    {
        $insertShoppingCart = "INSERT INTO shopping_cart VALUES (NULL,:userId);";
        $getShoppingCart = "SELECT id FROM shopping_cart WHERE user_id = :userId";
        $insertShoppingCartRequest = $this->dbs->prepare($insertShoppingCart);
        $getShoppingCartRequest = $this->dbs->prepare($getShoppingCart);

        $id = $this->secureVar($id);
        $quantity = $this->secureVar($quantity);
        $userId = $this->secureVar($userId);

        try {
            $getShoppingCartRequest->execute(array(
                "userId" => $userId
            ));
            if ($getShoppingCartRequest->rowCount()>0) {
                $shoppingCartId = $getShoppingCartRequest->fetchObject()->id;
                if ($exchange != "") {
                    ///offre en echange
                    return $this->insertExchangeOffer($shoppingCartId,$id,$quantity,$userId,$exchange);
                } else {
                    return $this->insertSoldOffer($shoppingCartId,$id,$quantity,$userId);
                }
            } else {
                $insertShoppingCartRequest->execute(array(
                    "userId" => $userId
                ));
                $getShoppingCartRequest->execute(array(
                    "userId" => $userId
                ));
                $shoppingCartId = $getShoppingCartRequest->fetchObject()->id;
                if ($exchange != "") {
                    ///offre en echange
                    return $this->insertExchangeOffer($shoppingCartId,$id,$quantity,$userId,$exchange);
                } else {
                    return $this->insertSoldOffer($shoppingCartId,$id,$quantity,$userId);
                }
            }

        } catch (PDOException $e) {
            return false;
        }
    }

    private function insertExchangeOffer($shoppingChartId, $offerId, $quantity, $userId, $subcategoryId)
    {
        $getOffersInShoppingCart = "SELECT offer_id FROM shopping_cart_offer INNER JOIN shopping_cart ON shopping_cart.id = shopping_cart_offer.shopping_cart_id WHERE offer_id = :offerId AND shopping_cart.user_id = :userId";
        $insertOffer = "INSERT INTO shopping_cart_offer VALUES (NULL,:quantity,:type,:shoppingId,:offerId,:subcategoryId)";
        $insertOfferRequest = $this->dbs->prepare($insertOffer);
        $getOffersInShoppingCartRequest = $this->dbs->prepare($getOffersInShoppingCart);
        try {
            $getOffersInShoppingCartRequest->execute(array(
                "offerId"=>$offerId,
                "userId"=>$userId
            ));
            $offer = $getOffersInShoppingCartRequest->fetch();
            if ($offer) {
                return true;
            } else {

                $insertOfferRequest->execute(array(
                    "quantity" => $quantity,
                    "shoppingId"=>$shoppingChartId,
                    "offerId"=>$offerId,
                    "type" => "exchange",
                    "subcategoryId" => $subcategoryId
                ));
            }
            return true;
        } catch(PDOException $e) {
            return false;
        }
    }

    private function insertSoldOffer($shoppingCartId,$offerId,$quantity,$userId) {
        $getOffersInShoppingCart = "SELECT offer_id FROM shopping_cart_offer INNER JOIN shopping_cart ON shopping_cart.id = shopping_cart_offer.shopping_cart_id WHERE offer_id = :offerId AND shopping_cart.user_id = :userId";
        $insertOffer = "INSERT INTO shopping_cart_offer VALUES (NULL,:quantity,:type,:shoppingId,:offerId,0)";
        $insertOfferRequest = $this->dbs->prepare($insertOffer);
        $getOffersInShoppingCartRequest = $this->dbs->prepare($getOffersInShoppingCart);
        try {
            $getOffersInShoppingCartRequest->execute(array(
                "offerId"=>$offerId,
                "userId"=>$userId
            ));
            $offer = $getOffersInShoppingCartRequest->fetch();
            if ($offer) {
                return true;
            } else {
                $insertOfferRequest->execute(array(
                    "quantity" => $quantity,
                    "shoppingId"=>$shoppingCartId,
                    "offerId"=>$offerId,
                    "type" => "price"
                ));
            }
            return true;
        } catch(PDOException $e) {
            return false;
        }
    }

    public function listOffersId($userId)
    {
        $sql = "SELECT offer_id, quantity, exchange_against, type FROM shopping_cart_offer INNER JOIN shopping_cart ON shopping_cart.id = shopping_cart_offer.shopping_cart_id WHERE shopping_cart.user_id = :userId";
        $request = $this->dbs->prepare($sql);
        $request->execute(array("userId"=>$userId));
        return $request->fetchAll();
    }

    public function clearByUser($userId) {
        $sql = "DELETE FROM shopping_cart WHERE user_id = ".$userId;
        $this->dbs->exec($sql);
        return true;
    }

    public function setOffer($offerId, $quantity) {
        $sql = "UPDATE shopping_cart_offer SET quantity = '".$quantity."' WHERE offer_id = ".$offerId;
        $this->dbs->exec($sql);
        return true;
    }

    public function deleteOffer($offerId) {
        $sql = "DELETE FROM shopping_cart_offer WHERE offer_id = ".$offerId;
        $this->dbs->exec($sql);
        return true;
    }
}

