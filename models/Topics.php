<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 06/05/15
 * Time: 11:22
 */

class Topics extends Model {

    public function listAll() {
        $sql = "SELECT * FROM topics";
        $request = $this->dbs->query($sql);
        return $request->fetchAll();
    }

    public function get($id) {
        $sql = "SELECT * FROM topics WHERE idtopics = :id";
        $request = $this->dbs->prepare($sql);
        $request->execute(array(
            "id"=>$id));
        return $request->fetch();
    }

    public function insert($title) {
        $sql = "INSERT INTO topics VALUES (NULL, '".$title."')";
        $this->dbs->exec($sql);
        return true;
    }

public function numberPostAppearence($id){
    $sql = "SELECT COUNT(post.id) FROM topics
            INNER JOIN post ON topics.idtopics = post.topics_id
            WHERE idtopics = ".$id." AND post.valid = 1";
    $request = $this->dbs->query($sql);
    return $request->fetch();
}

    public function numberResponsesAppearence($id){
        $sql = "SELECT COUNT(response.id) FROM response
  INNER JOIN post ON post.id = response.post_id
  INNER JOIN topics ON idtopics = topics_id
WHERE idtopics =".$id." AND post.valid = 1 AND response.valid = 1";
        $request = $this->dbs->query($sql);
        return $request->fetch();
    }

    public function rm($id) {
        $delete = 'DELETE FROM topics WHERE idtopics = '. $id;

        try {
            $this->dbs->exec($delete);
        } catch(PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }


}