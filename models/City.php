<?php

/**
 * Class city
 */
class City extends Model {

    public function listByDepartment($departmentId){
        $sql = 'SELECT City.name FROM City WHERE City.Department_id = :departmentId';
        $cities = $this->dbs->prepare($sql);
        $cities->execute(array(
            "departmentId" => $departmentId
        ));
        $cities = $cities->fetchAll();
        return $cities;
    }

    public function search($cityName){
        $sql = 'SELECT idCity,name FROM City WHERE name LIKE "%'.$cityName.'%" OR postalcode LIKE "%'.$cityName.'%"';
        $city = $this->dbs->prepare($sql);
        $city->execute(array());
        return $city->fetchAll();
    }

} 