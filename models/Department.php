<?php

/**
 * Class Department
 */
class Department extends Model {

    public function listByState($stateId){
        $sql = 'SELECT Department.name FROM Department WHERE Department.State_id = :stateId';
        $department = $this->dbs->prepare($sql);
        $department->execute(array(
            "stateId" => $stateId
        ));
        $department = $department->fetchAll();
        return $department;
    }

} 