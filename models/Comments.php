<?php
/**
 * Created by IntelliJ IDEA.
 * User: Gaut
 * Date: 02/06/2015
 * Time: 12:43
 */

class Comments extends Model{

    public function listAll(){
        $sql = 'SELECT * FROM Comments';
        $getUsers = $this->dbs->prepare($sql);
        $getUsers->execute(array());
        return $getUsers->fetchAll();
    }

    public function insertComment($grade,$content,$commentedUserId,$ownerId){

        $insertComment = "INSERT INTO comments(id,grade,content,date,commented_user_id,owner_id) VALUES (NULL,:grade,:content,:date,:commentedUserId,:ownerId);";
        $insertCommentRequest=$this->dbs->prepare($insertComment);
        $grade=$this->secureVar($grade);
        $content=$this->secureVar($content);
        $now = new DateTime();

        $insertCommentRequest->execute(array(
            "grade"=>$grade,
            "content"=>$content,
            "date"=>$now->format("Y-m-d\TH:i:sP"),
            "commentedUserId"=>$commentedUserId,
            "ownerId"=>$ownerId
        ));

   return true;
    }




    public function get($id)
    {
        $sql = "SELECT * FROM comments WHERE id = :id";
        $request = $this->dbs->prepare($sql);
        $request->execute(array(
            "id"=>$id));
        return $request->fetch();
    }

    public function score($id){
        $sql = "SELECT COUNT(post.id) FROM comments
            INNER JOIN post ON topics.idtopics = post.topics_id
            WHERE idtopics = ".$id;
        $request = $this->dbs->query($sql);
        return $request->fetch();
    }

    public function nbrScore($id){

    }

}