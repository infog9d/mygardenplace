<?php


class Offers extends Model {

    const SOLD = 0;
    const EXCHANGE = 1;
    const BOTH = 2;

    public function get($id){
        $sql = "SELECT * FROM offer WHERE offer.id = :id";
        $request = $this->dbs->prepare($sql);
        $request->execute(array(
            "id" => $id
        ));
        return $request->fetchObject();
    }

    public function addBooking ($params, $quantity, $idUser, $idSeller){
/*ajouter l'offre au book*/
    }
    public function createOffer($title, $subcategory, $cities, $quantity, $owner, $desc = "", $price = "", $picture = "", $exchanges = ""){
        $exist = "SELECT id FROM offer WHERE title=:title AND owner_id = :owner;";
        $insertOffer = "INSERT INTO offer VALUES (NULL,:title,:description,:quantity,:price,1,:userid,:subcategory,:postedon,:picture);";
        $insertLinkToCities = "INSERT INTO City_has_offer VALUES (:cityid,:offerid);";
        $insertExchanges = "INSERT INTO exchange VALUES (NULL,:quantity,:offerid,:subcategoryid);";
        $existRequest = $this->dbs->prepare($exist);
        $insertOfferRequest = $this->dbs->prepare($insertOffer);
        $insertLinkToCitiesRequest = $this->dbs->prepare($insertLinkToCities);
        $insertExchangesRequest = $this->dbs->prepare($insertExchanges);

        $title = $this->secureVar($title);
        $desc = $this->secureVar($desc);
        $quantity = $this->secureVar($quantity);
        $price = $this->secureVar($price);

        try {
            $existRequest->execute(array(
                "title" => $title,
                "owner" => $owner
            ));

            $results = $existRequest->fetchObject();
            if (!$results) {

                $now = new DateTime();
                $insertOfferRequest->execute(array(
                    "title" => $title,
                    "postedon" => $now->format("Y-m-d"),
                    "description" => $desc,
                    "quantity" => $quantity,
                    "price" => $price,
                    "userid" => $owner,
                    "subcategory" => $subcategory,
                    "picture" => $picture
                ));

                $existRequest->execute(array(
                    "title" => $title,
                    "owner" => $owner
                ));

                $justAdded = $existRequest->fetchObject();
                $offerId = $justAdded->id;

                $cities = explode(",",$cities);
                if (count($cities)>0) {
                    foreach ($cities as $cityId) {
                        if (is_numeric($cityId)) {
                            $insertLinkToCitiesRequest->execute(array(
                                "cityid" => $cityId,
                                "offerid" => $offerId
                            ));
                        }
                    }
                } else {
                    return "erreur : Vous devez entrer au moins une ville où vous livrez votre annonce.";
                }


                $exchanges = explode(",",$exchanges);

                foreach ($exchanges as $exchange) {
                    $data = explode(':',$exchange);//0: subcategory id(int), 1:qtité(float)||int
                    if (is_numeric($data[0])) {
                        $insertExchangesRequest->execute(array(
                            "quantity" => $data[1],
                            "offerid" => $offerId,
                            "subcategoryid" => $data[0]
                        ));
                    }
                }


                return "Votre offre à bien été postée.";

            } else {
                return "erreur : Il semble que vous ayez déjà posté une annonce similaire : <a href='".Router::generateUrl("offers","view",array($results->id))."'>Voir l'annonce</a>";
            }
        } catch(PDOException $e) {
            return "erreur : ".$e;
        }
    }

    public function getFormated($id)
    {
        /**
         * Get offer information
         */
        $sql = "SELECT offer.*, s.name AS category, user.firstname, user.lastname, user.id AS accountId, user.picture AS accountPicture FROM offer INNER JOIN subcategory s ON s.id = offer.subcategory_id INNER JOIN user ON user.id = offer.owner_id WHERE offer.id = ".$id.";";
        $request = $this->dbs->prepare($sql);
        /**
         * Get city by offer id
         */
        $sqlGetCityByOffer = "SELECT City.name AS city FROM offer INNER JOIN City_has_offer ON offer.id = City_has_offer.offer_id INNER JOIN City ON City.idCity = City_has_offer.City_idCity WHERE offer.id = :offerId;";
        $getCityByOffer = $this->dbs->prepare($sqlGetCityByOffer);
        /**
         * Get exchanges by offer id
         */
        $sqlGetExchangeByOffer = "SELECT exchange.quantity, subcategory.name AS category FROM offer INNER JOIN exchange ON offer.id = exchange.offer_id INNER JOIN subcategory ON subcategory.id = exchange.subcategory_id WHERE offer.id = :offerId;";
        $getExchangeByOffer = $this->dbs->prepare($sqlGetExchangeByOffer);


        try {
            $request->execute(array());
            $result = $request->fetch();

            $offerId = $result["id"];
            $getCityByOffer->execute(array(
                "offerId" => $offerId
            ));

            $cities = array();
            foreach ($getCityByOffer->fetchAll() as $city) {
                $cities[] = $city["city"];
            }

            $result["cities"] = $cities;

            $getExchangeByOffer->execute(array(
                "offerId" => $offerId
            ));

            $result["exchanges"] = $getExchangeByOffer->fetchAll();



            return $result;
        } catch (PDOException $e) {
            return null;
        }
    }

    public function getLast($userId = ""){
        /**
         * Get offer information
         */
        if ($userId!=""&&is_numeric($userId)) {
            $sql = "SELECT offer.*, s.name AS category, user.firstname, user.lastname, user.id AS accountId, user.picture AS accountPicture FROM offer INNER JOIN subcategory s ON s.id = offer.subcategory_id INNER JOIN user ON user.id = offer.owner_id WHERE user.id = ".$userId." AND offer.valid = 1 ORDER BY offer.available_on DESC LIMIT 6;";
        } else {
            $sql = "SELECT offer.*, s.name AS category, user.firstname, user.lastname, user.id AS accountId, user.picture AS accountPicture FROM offer INNER JOIN subcategory s ON s.id = offer.subcategory_id INNER JOIN user ON user.id = offer.owner_id WHERE offer.valid = 1 ORDER BY offer.available_on DESC LIMIT 6;";
        }
        $request = $this->dbs->prepare($sql);
        /**
         * Get city by offer id
         */
        $sqlGetCityByOffer = "SELECT City.name AS city FROM offer INNER JOIN City_has_offer ON offer.id = City_has_offer.offer_id INNER JOIN City ON City.idCity = City_has_offer.City_idCity WHERE offer.id = :offerId;";
        $getCityByOffer = $this->dbs->prepare($sqlGetCityByOffer);
        /**
         * Get exchanges by offer id
         */
        $sqlGetExchangeByOffer = "SELECT exchange.quantity, subcategory.name AS category FROM offer INNER JOIN exchange ON offer.id = exchange.offer_id INNER JOIN subcategory ON subcategory.id = exchange.subcategory_id WHERE offer.id = :offerId;";
        $getExchangeByOffer = $this->dbs->prepare($sqlGetExchangeByOffer);


        try {
            $request->execute(array());
            $results = $request->fetchAll();

            foreach ($results as $k => $result) {
                $offerId = $result["id"];
                $getCityByOffer->execute(array(
                    "offerId" => $offerId
                ));

                $cities = array();
                foreach ($getCityByOffer->fetchAll() as $city) {
                    $cities[] = $city["city"];
                }

                $result["cities"] = $cities;

                $getExchangeByOffer->execute(array(
                    "offerId" => $offerId
                ));

                $result["exchanges"] = $getExchangeByOffer->fetchAll();

                $results[$k] = $result;
            }

            return $results;
        } catch (PDOException $e) {
            return array();
        }
    }

    public function find($title="", $category="", $name="", $city="", $quantity="", $type=self::BOTH) {
        /**
         * Get offer information
         */
        $sql = "SELECT DISTINCT offer.*,
                       s.name AS category,
                       user.firstname,
                       user.lastname,
                       user.id AS accountId,
                       user.picture AS accountPicture
                 FROM offer
                 INNER JOIN subcategory s ON s.id = offer.subcategory_id
                 INNER JOIN category c ON c.id = s.category_id
                 INNER JOIN user ON user.id = offer.owner_id
                 INNER JOIN City_has_offer ON offer.id = City_has_offer.offer_id
                 INNER JOIN City ON City.idCity = City_has_offer.City_idCity
                 INNER JOIN exchange ON offer.id = exchange.offer_id
                 WHERE offer.valid = 1 AND ";
        $isFirst = true;
        if (!empty($title)) {
            $sql .= "(offer.title LIKE '%$title%' OR offer.description LIKE '%$title%' OR c.name LIKE '%$title%')";
            $isFirst = false;
        }
        if (!empty($category)||is_array($category)) {
            if (is_array($category)) {
                $isFirstCategory = true;
                foreach($category as $cat) {
                    if (!$isFirst&&$isFirstCategory) {
                        $sql .= " AND (";
                    } else if ($isFirst&&$isFirstCategory) {
                        $sql .= "(";
                    }
                    if (!$isFirstCategory) {
                        $sql .= " OR ";
                    }
                    $sql .= "s.name LIKE '%$cat%'";
                    if (is_numeric($cat)) {
                        $sql .= " OR s.id = $cat";
                    }
                    $isFirst = false;
                    $isFirstCategory = false;
                }
                $sql .= ")";
            } else {
                if (!$isFirst) {
                    $sql .= " AND ";
                }
                $sql .= "s.name LIKE '%$category%'";
                if (is_numeric($category)) {
                    $sql .= " OR s.id = $category";
                }
                $isFirst = false;
            }
        }
        if (!empty($name)) {
            if (!$isFirst) {
                $sql .= " AND ";
            }
            $name = explode(" ",$name);
            if (count($name)>1) {
                $sql .= "(firstname LIKE '%".$name[0]."%' OR lastname LIKE '%".$name[1]."%')";
            } else {
                $sql .= "(firstname LIKE '%".$name[0]."%' OR lastname LIKE '%".$name[0]."%')";
            }
            $isFirst = false;
        }
        if (!empty($city)) {
            if (is_array($city)) {
                foreach($city as $cit) {
                    if (!$isFirst) {
                        $sql .= " AND ";
                    }
                    $sql .= "(City.name LIKE '%$cit%' OR City.zipcode LIKE '%$cit%')";
                    $isFirst = false;
                }
            } else {
                if (!$isFirst) {
                    $sql .= " AND ";
                }
                $sql .= "(City.name LIKE '%$city%' OR City.zipcode LIKE '%$city%')";
                $isFirst = false;
            }
        }
        if (!empty($quantity)) {
            if (!$isFirst) {
                $sql .= " AND ";
            }
            $sql .= "offer.quantity >= $quantity";
            $isFirst = false;
        }
        if ($type != self::BOTH) {
            if ($type == self::EXCHANGE) {
                if ($isFirst) {
                    $sql .= "1=1";
                }
                $sql .= " HAVING count(exchange.id) > 0";
            } else {
                if (!$isFirst) {
                    $sql .= " AND ";
                }
                $sql .= " offer.price != ''";
            }
        }

        //print_r($sql);
        $request = $this->dbs->prepare($sql);
        //print_r($request->queryString);
        /**
         * Get city by offer id
         */
        $sqlGetCityByOffer = "SELECT City.name AS city FROM offer INNER JOIN City_has_offer ON offer.id = City_has_offer.offer_id INNER JOIN City ON City.idCity = City_has_offer.City_idCity WHERE offer.id = :offerId ";
        $getCityByOffer = $this->dbs->prepare($sqlGetCityByOffer);
        /**
         * Get exchanges by offer id
         */
        $sqlGetExchangeByOffer = "SELECT exchange.quantity, subcategory.name AS category FROM offer INNER JOIN exchange ON offer.id = exchange.offer_id INNER JOIN subcategory ON subcategory.id = exchange.subcategory_id WHERE offer.id = :offerId ";
        $getExchangeByOffer = $this->dbs->prepare($sqlGetExchangeByOffer);


        try {
            $request->execute(array());
            $results = $request->fetchAll();

            foreach ($results as $k => $result) {
                $offerId = $result["id"];
                $getCityByOffer->execute(array(
                    "offerId" => $offerId
                ));

                $cities = array();
                foreach ($getCityByOffer->fetchAll() as $city) {
                    $cities[] = $city["city"];
                }

                $result["cities"] = $cities;

                $getExchangeByOffer->execute(array(
                    "offerId" => $offerId
                ));

                $result["exchanges"] = $getExchangeByOffer->fetchAll();

                $results[$k] = $result;
            }

            return $results;
        } catch (PDOException $e) {
            return array();
        }
    }
    public function isExchange($id) {
        $sql = "SELECT Count(*) FROM exchange WHERE offer_id=:id";
        $request = $this->dbs->prepare($sql);
        $request->execute(array("id"=>$id));
        $nb=$request->fetch();
        return $nb[0] > 0;
    }

    public function getPrice($id)
    {
        $sql = "SELECT price FROM offer WHERE id=:id";
        $request = $this->dbs->prepare($sql);
        $request->execute(array("id"=>$id));
        $price=$request->fetch();
        return $price["price"];
    }

    public function isSold($id)
    {
        $price = $this->getPrice($id);
        return (!empty($price))?true:false;
    }

    public function getQuantity($id)
    {
        $sql = "SELECT quantity FROM offer WHERE id=:id";
        $request = $this->dbs->prepare($sql);
        $request->execute(array("id"=>$id));
        $quantity=$request->fetch();
        return $quantity["quantity"];
    }

    public function getExchange($id)
    {
        $sql = "SELECT subcategory.name,exchange.quantity,subcategory.id
                FROM exchange
                INNER JOIN subcategory ON exchange.subcategory_id = subcategory.id
                WHERE offer_id=:id";
        $request = $this->dbs->prepare($sql);
        $request->execute(array("id"=>$id));
        $results=$request->fetchAll();
        return $results;
    }

    public function getByKeywordSince($keyword, DateTime $date) {
        $now = $date->format("Y-m-d");
        $sql = "SELECT DISTINCT offer.id FROM offer WHERE
                  (offer.description LIKE '%$keyword%' OR
                  offer.title LIKE '%$keyword%') AND
                  offer.available_on >= '$now' AND
                  offer.valid = 1";
        $request = $this->dbs->prepare($sql);
        try {
            $request->execute(array());
            return $request->fetchAll();
        } catch (PDOException $e) {
            return array();
        }
    }

    public function getByUserId($userId) {
        $sql = "SELECT DISTINCT offer.id FROM offer WHERE owner_id = $userId";
        $request = $this->dbs->query($sql);
        if ($request) {
            if($results = $request->fetchAll()) {
                return $results;
            }
        }
        return array();
    }

    public function getByGardenerSince($gardener, DateTime $date) {
        $now = $date->format("Y-m-d");
        $gardener = explode(" ",$gardener);
        if (count($gardener)>1) {
            $sql = "SELECT DISTINCT offer.id FROM offer INNER JOIN user ON user.id = offer.owner_id WHERE
                  user.firstname LIKE '%{$gardener[0]}%' AND
                  user.lastname LIKE '%{$gardener[1]}%' AND
                  offer.available_on >= '$now' AND
                  offer.valid = 1";
        } else {
            $sql = "SELECT DISTINCT offer.id FROM offer INNER JOIN user ON user.id = offer.owner_id WHERE
                  (user.firstname LIKE '%{$gardener[0]}%' OR
                  user.lastname LIKE '%{$gardener[0]}%') AND
                  offer.available_on >= '$now' AND
                  offer.valid = 1";
        }
        $request = $this->dbs->prepare($sql);
        try {
            $request->execute(array());
            return $request->fetchAll();
        } catch (PDOException $e) {
            return array();
        }
    }

    public function getBySubCategorySince($subcategory, $date) {
        $now = $date->format("Y-m-d");
        $sql = "SELECT DISTINCT offer.id FROM offer INNER JOIN subcategory ON subcategory.id = offer.subcategory_id WHERE
                  subcategory.name = '$subcategory' AND
                  offer.available_on >= '$now' AND
                  offer.valid = 1";
        $request = $this->dbs->prepare($sql);
        try {
            $request->execute(array());
            return $request->fetchAll();
        } catch (PDOException $e) {
            return array();
        }
    }

    public function getByCategorySince($category, $date) {
        $now = $date->format("Y-m-d");
        $sql = "SELECT DISTINCT offer.id FROM offer INNER JOIN subcategory ON subcategory.id = offer.subcategory_id INNER JOIN category ON category.id = subcategory.category_id WHERE
                  category.name = '$category' AND
                  offer.available_on >= '$now' AND
                  offer.valid = 1";
        $request = $this->dbs->prepare($sql);
        try {
            $request->execute(array());
            return $request->fetchAll();
        } catch (PDOException $e) {
            return array();
        }
    }

    public function set($id, $array) {
        if (is_array($array)) {
            if (count($array)>0) {
                $sql = 'UPDATE offer SET ';
                $numberOfields = count($array);
                $currentIteration = 1;
                foreach ($array as $column => $value) {

                    $sql .= $column.' = "'.$this->secureVar($value).'"';
                    if ($currentIteration < $numberOfields) {
                        $sql .= ", ";
                    } else {
                        $sql .= " ";
                    }
                    $currentIteration++;
                }
                $sql .= 'WHERE id = '.$id;

                $this->dbs->exec($sql);
                return true;
            }
        }
        throw new Exception("Error : wrong parameters");
    }

    public function rm($id) {
        $deleteOffer = 'DELETE FROM offer WHERE id = '. $id;

        try {
            $this->dbs->exec($deleteOffer);
        } catch(PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }
}
