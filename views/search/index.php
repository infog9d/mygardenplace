<?php Controller::addCss("search") ?>
<?php Controller::requireMethod("utils","breadcrumbs",array(
    Router::generateUrl("home") => "Accueil",
    "" => "Résultats de la recherche"
)); ?>
<div class="table">
    <div class="table-6">
        <div class="form-container">
            <form action="" method="get" class="searchForm">
                <div class="table">
                    <label for="name" class="table-6">
                        <input type="text" id="name" name="name" placeholder="Fruit, légumes" value="<?php echo (isset($_GET["name"]))?$_GET["name"]:""; ?>"/>
                    </label>
                    <label for="city" class="table-6">
                        <input type="text" id="city" name="city" placeholder="ville" value="<?php echo (isset($_GET))?$_GET["city"]:""; ?>"/>
                    </label>
                </div>
                <div class="table">
                    <div class="table-6">
                        <label for="quantity">
                            <input type="number" id="quantity" name="quantity" placeholder="quantité minimale" value="<?php echo (isset($_GET["quantity"]))?$_GET["quantity"]:""; ?>"/>
                        </label><br/>
                        <label for="gardener">
                            <input type="text" id="gardener" name="gardener" placeholder="gardener" value="<?php echo (isset($_GET["gardener"]))?$_GET["gardener"]:""; ?>"/>
                        </label>
                    </div>
                    <div class="table-6">
                        <div class="table">
                            <label for="typeSold" class="table-6">
                                <div class="table">
                                    <div class="table-6">En vente</div>
                                    <div class="table-6"><input type="checkbox" id="typeSold" name="sold" value="1" <?php if (Utils::determineExchangeOrSold()==Offers::BOTH||Utils::determineExchangeOrSold()==Offers::SOLD) { ?>checked<?php } ?>/></div>
                                </div>
                            </label>
                            <label for="typeExchange" class="table-6">
                                <div class="table">
                                    <div class="table-6">En echange</div>
                                    <div class="table-6"><input type="checkbox" id="typeExchange" name="exchange" value="1" <?php if (Utils::determineExchangeOrSold()==Offers::BOTH||Utils::determineExchangeOrSold()==Offers::EXCHANGE) { ?>checked<?php } ?>/></div>
                                </div>
                            </label>
                        </div>
                        <label for="categories">
                            <select name="categories[]" id="categories" multiple>
                                <?php foreach($params["categories"] as $category) { ?>
                                    <option value="<?php echo $category["id"]; ?>" <?php if (Utils::checkSelectedCategory($category["name"],$category["id"])) { ?>selected<?php } ?>><?php echo $category["name"]; ?></option>
                                <?php } ?>
                            </select>
                        </label>
                    </div>
                </div>
                <input type="submit" value="rechercher"/>
            </form>
        </div>
        <div class="container">
            <div class="pannels table">
            <?php
                $iterate = 0;
                if (count($params["ads"])>0) {
                    foreach ($params["ads"] as $ad) {
                        $iterate++;
                        Controller::requireMethod("offers", "tileView", array("tileSize" => 6, "ad" => $ad));
                        if ($iterate == 2) {
                            $iterate = 0;
            ?>
            </div>
            <div class="pannels table">
            <?php
                        }
                    }
                    while($iterate!=0&&$iterate<2) {
                        Controller::requireMethod("offers","tileView", array("tileSize" => 6,"ad" => $ad,"blank"=>true));
                        $iterate++;
                    }
                } else {
                    echo "Aucune annonce ne correspond à votre recherche.";
                }
            ?>

            </div>
        </div>

    </div>
    <div class="table-6">
        <div id="maps">
        </div>
    </div>
</div>
<?php Controller::addJs("tileView"); ?>
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMQazW751UazOtA8QF4CQqOwH4KvmqO1A">
    </script>
<?php Controller::addJs("gm"); ?>
<?php Controller::addJs("api"); ?>
<?php Controller::addJs("autocompleteCity"); ?>

<?php Controller::beginJsBlock() ?>
<script type="application/javascript">
    var viewOffer = "<?php echo Router::generateUrl("offers","view") ?>/";
    document.addEventListener("DOMContentLoaded", function() {
        autocompleteCity.init("city","<?php echo Router::generateUrl("city","search"); ?>");
        autocompleteCity.init("gardener","<?php echo Router::generateUrl("user","search"); ?>");
        var maps = document.getElementById("maps");
        maps.style.height = maps.parentNode.offsetHeight+"px";
        gm.cities = [<?php echo implode(",",Utils::quoteEach($params["cities"])); ?>];
        google.maps.event.addDomListener(window, 'load', gm.init("maps"));
    });
</script>
<?php Controller::endJsBlock() ?>