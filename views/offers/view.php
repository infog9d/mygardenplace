<?php Controller::addCss("view.css"); ?>
<?php Controller::addCss("offer")?>
<?php //if(Session:: has("user")){echo Session::get("user")["mail"] ;} ?>
<?php Controller::requireMethod("utils","breadcrumbs",array(
    Router::generateUrl("home") => "Accueil",
    Router::generateUrl("offers","view",array($params["ad"]["id"])) => $params["ad"]["title"]
)); ?>
<div class="table">
    <div class="image_annonce">
        <img class="img_annonce" src="<?php echo ($params["ad"]["picture"] != "" && file_exists(WEBROOT."/assets/uploads/".$params["ad"]["picture"]))?BASE_URL."/assets/uploads/".$params["ad"]["picture"]:BASE_URL."/assets/media/pomme_mystere.png"; ?>" alt=""/>
    </div>
    <div class="annonce">
        <div class="table">
            <div class="table-2">
                <span class="pad5 round">
                    <img id="image<?php echo $params["ad"]["id"]; ?>" data-id="<?php echo $params["ad"]["id"]; ?>" class="imag" src="<?php echo ($params["ad"]["accountPicture"] != "")?BASE_URL."/assets/uploads/".$params["ad"]["accountPicture"]:BASE_URL."/assets/media/unknown.png"; ?>" alt=""/>
                </span>
            </div>
            <div class="info_titre table-10">
                <div class="table">
                    <h1 class="title"><?php echo $params["ad"]["title"];?></h1>
                    <ul class="liste_title">
                        <li id="info1">Mise en ligne par : <a href="<?php echo Router::generateUrl("profileUser","index"); ?>/<?php echo $params["ad"]["owner_id"]; ?>"><?php echo $params["ad"]["firstname"].' '.$params["ad"]["lastname"] ;?></a> </li>
                        <li id="info2">Catégorie : <a href="<?php echo Router::generateUrl("search","index"); ?>?name=<?php echo $params["ad"]["category"]; ?>&gardener=&city="><?php echo $params["ad"]["category"]; ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="info_annonce">
            <div class="table">
                <div class="info_local">
                    <p>Localisation :</p>
                    <ul class="city">
                        <?php foreach($params["ad"]["cities"] as $city){ ?>
                        <li> <a href="<?php echo Router::generateUrl("search","index"); ?>?name=&gardener=&city=<?php echo $city ?>"><?php echo $city ?></a> </li>
                        <?php } ?>
                    </ul>
                    <p><?php echo $params["ad"]["firstname"]; ?> <?php echo $params["ad"]["lastname"]; ?> vend <?php echo lcfirst($params["ad"]["title"]) ?></p>
                </div>
                <div class="form">
                    <p>Quantité restante : <?php echo $params["ad"]["quantity"]?> Kg</p>
                    <!--<form action="<?php /*echo Router::generateUrl("offers","sendMail",array($params["ad"]["id"])); */?>" method="post" >
                        <label for="quantity">
                            <input id="quantity" type="number" step=0.01 min="0.01" max="<?php /*echo $params["ad"]["quantity"]*/?>" name="quantity">
                        </label>
                        <span class="cadd">
                            <img src="<?php /*echo BASE_URL."/assets/media/caddie.PNG" */?>" alt="" class="caddie" data-id="<?php /*echo $params["ad"]["id"]; */?>"/>
                        </span>
                        <a href="#" id="addToChart" data-id="<?php /*echo $params["ad"]["id"]; */?>">Ajouter au panier</a>
                        <input type="submit" value="Réserver">
                        <div>
                        </div>
                    </form>-->
                </div>
            </div>
            <div class="table">
                <div class="table-6">
                    Ajouter au panier :<br>
                    <div class="cadd" id="caddie<?php echo $params["ad"]["id"]; ?>">
                        <img src="<?php echo BASE_URL."/assets/media/caddie.PNG" ?>" alt="" class="caddie" data-id="<?php echo $params["ad"]["id"]; ?>"/>
                    </div>
                </div>
                <div class="table-6">
                    Ou
                    <input id="contactUser" type="button"  value="Contacter <?php echo $params["ad"]["firstname"].' '.$params["ad"]["lastname"]?>">
                </div>
            </div>
        </div>
    </div>
</div>
<div id="info" class="table">
    <div class="comment table-6">
        <p> <?php echo stripslashes($params["ad"]["description"]) ?> </p>
    </div>
    <div class="google_map table-6">
        <div id="maps"></div>
    </div>
</div>
<div id="annonce_similaire">
</div>

<div id="contactForm" class="modal">
    <?php Controller::requireMethod("user", "contactForm",$params); ?>
</div>

<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMQazW751UazOtA8QF4CQqOwH4KvmqO1A">
</script>

<?php Controller::addJs("gm"); ?>
<?php Controller::addJs("api"); ?>
<?php Controller::addJs("modal.js"); ?>
<?php Controller::addJS("addToChart.js");?>

<?php Controller::beginJsBlock(); ?>
<script type="application/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        modal.init("contactForm");
        modal.hide("contactForm");
        var contact = function () {
            modal.toggle("contactForm");
        };
        var element = document.getElementById("contactUser");
        element.onclick = contact;
        gm.cities = [<?php echo implode(",",Utils::quoteEach($params["ad"]["cities"])); ?>];
        google.maps.event.addDomListener(window, 'load', gm.init("maps"));
        var btn = document.getElementById("addToChart");
        btn.onclick = function (evt) {


        }
    });
</script>
<?php Controller::endJsBlock(); ?>
<?php Controller::requireMethod("offers","addToChart"); ?>
