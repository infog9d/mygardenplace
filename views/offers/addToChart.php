<div id="modaladdcart" class="modal">

    <div class="table">
        <div class="tablegrand" id = "priceView">
            <span class="achat">Achat </span>
            <input min="0" type="number" value="0" id="qtity" placeholder="quantité">
            <div class="table">
                <div class="table-6">
                    <div class="prix">
                        Prix : <span id="price">0</span> €
                    </div>
                </div>

            </div>


        </div>
        <div class="table-6" id = "exchangeView">
            Echange
            <select id="listexchange">

            </select>
            <input min="0" type="number" value="0" id="qtityExchange" placeholder="quantité">

        </div>

    </div>
    <div class="button1"><a class="achat add" id="add">Ajouter au panier</a></div>
</div>
<?php echo Controller::requireJs("addToChart"); ?>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        modal.init("modaladdcart");
        urls.redirectToShoppingChart = "<?php echo Router::generateUrl("shoppingchart") ?>";
        urls.addToShoppingChart = "<?php echo Router::generateUrl("offers","add-to-shopping-chart") ?>";
        urls.checkExchange = "<?php echo Router::generateUrl("offers","check-exchange") ?>";
        var images = document.getElementsByClassName("caddie");
        for (var i = 0; i < images.length; i++) {
            images[i].onclick = onclk;
        }

    });
</script>