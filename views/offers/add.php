<?php Controller::requireMethod("utils","breadcrumbs",array(
    Router::generateUrl("home") => "Accueil",
    Router::generateUrl("user","account")=> "Mon profil",
    Router::generateUrl("add")=> "Poster une annonce",
));
if (Session::hasFlashbag('errorPostOffer')) {?>
    <span class="alert error"><?php echo Session::getFlashbag('errorPostOffer'); ?></span>
<?php } ?>
<form action="<?php echo Router::generateUrl("offers", "post") ?>" method="post" enctype="multipart/form-data">
    <div class="table">
        <div class="table-6">
            <label for="title">
                Je vends
                <input type="text" placeholder="Titre de mon annonce" name="title" <?php if (isset($_POST["title"])){ ?>value="<?php echo $_POST["title"]; ?>"<?php } ?> />
            </label><br>
            <label for="subcategory">
                Selectionner une catégorie
                <select name="subcategory" id="subcategory"  <?php if (isset($_POST["subcategory"])){ ?>value="<?php echo $_POST["subcategory"]; ?>"<?php } ?>>
                    <?php foreach ($params["subcategories"] as $subcategory) { ?>
                    <option value="<?php echo $subcategory["id"] ?>"><?php echo $subcategory["name"] ?></option>
                    <?php } ?>
                </select>
            </label><br>
            <label for="city">
                Je me déplace à
                <input id="addCity" type="text" name="city" placeholder="Ville"/>
                <a id="btnAddCity" class="button add">+</a>
            </label>
            <div id="selectedCities">
                <ol>

                </ol>
                <input type="hidden" name="cities" id="cities">
            </div>
            Choisissez de vendre ou d'echanger vos fruits et légumes !<br>
            <label for="quantity">
                Quantité
                <input type="number" step="0.01" min="0.01" name="quantity" id="quantity" placeholder="100" /> Kg
            </label>
            <label for="price">
                Prix au kilos
                <input type="number" step="0.01" min="0.01" name="price" id="price" placeholder="1" /> €/Kg
            </label><br>
            <label for="switchType">
                Echange contre
                <input id="addCategory" type="text" name="category" placeholder="Fruit, Légume"/>
                <input type="number" step="0.01" min="0.01" name="switchQuantity" id="switchQuantity" placeholder="10"> Kg
                <a id="btnAddCategory" class="button add">+</a>
            </label>
            <div id="selectedCategories">
                <ol>

                </ol>
                <input type="hidden" name="categories" id="categories">
            </div>
        </div>
        <div class="table-6" style="text-align: right;">
            <input type="file" id="fileinput" name="picture" style="display: none;">
            <img id="selectImg" style="max-width: 100%;" />
        </div>
    </div>
    <p>
        Ajouter un commentaire :<br>
        <textarea name="description" style="width: 80%;heigth:300px;"></textarea>
    </p>

    <button type="submit" class="button" >envoyer</button>
</form>
<?php Controller::addJs("api"); ?>
<?php Controller::addJs("autocompleteCity"); ?>
<?php Controller::addJs("citiesSelector"); ?>
<?php Controller::addJs("categorySelector"); ?>
<?php Controller::addJs("imagePreview"); ?>
<?php Controller::beginJsBlock(); ?>
<script type="text/javascript">
    autocompleteCity.init("addCity", "<?php echo Router::generateUrl("city","search"); ?>");
    autocompleteCity.init("addCategory", "<?php echo Router::generateUrl("category","search"); ?>");
    imagePreview.init("fileinput","selectImg","","<?php echo BASE_URL . '/assets/media/pomme_mystere.png' ?>","");
    var img = document.getElementById("selectImg");
    var prevSrc = "";
    img.onmouseover = function () {
        if (prevSrc == "" || img.src == prevSrc) {
            prevSrc = img.src;
            img.style.backgroundImage = "url(\"" + img.src + "\")";
            img.style.backgroundRepeat = "no-repeat";
            img.src = "<?php echo BASE_URL . '/assets/media/replace.png' ?>";
        }
    };
    img.onmouseout = function () {
        if (img.src == "<?php echo BASE_URL . '/assets/media/replace.png' ?>") {
            img.src = prevSrc;
        }
        img.style.backgroundImage = "none";
    }
</script>
<?php Controller::endJsBlock(); ?>