<?php Controller::addCss("ShoppingCartIndex.css")?>
<?php Controller::addCss("offer.css")?>
<?php
if (count($params["offers"])>0) { ?>
    <div class="container">
    <div class="pannels table">
    <?php
    $iterator = 0;
foreach($params["offers"] as $offer) {
    $array = array("tileSize"=> 4, "ad" => $offer);
    if (isset($offer["exchangeAgainst"])) {
        $array["shoppingchart"] = true;
        $array["exchangeAgainst"] = $offer["exchangeAgainst"];
    }
    Controller::requireMethod("offers","tileView",$array);
    if ($iterator<2) {

        $iterator++;
    } else {
        $iterator = 0;
        ?>
    </div>
<div class="pannels table">
    <?php
    }


}
    while($iterator!=0&&$iterator<3){
        Controller::requireMethod("offers","tileView",array("tileSize"=>4,"ad"=>$offer,"blank"=>true));
        $iterator++;
    }
    ?>

</div>
    </div>
<div class="prix">
 <div class="ttc">
 total TTC : <?php echo number_format($params["totalTTC"],2); ?> €
 </div>
 <div class="ht">
 total HT : <?php echo number_format($params["totalHT"],2); ?> €
 </div>
    <div class="table">
        <div class="table-6">
            <form action="<?php echo Router::generateUrl("shoppingchart","validate"); ?>">
                <input type="submit" value="Confirmer ma reservation">
            </form>
        </div>
        <div class="table-6">
            <form action="<?php echo Router::generateUrl("shoppingchart","clear"); ?>">
                <input type="submit" value="Vider mon panier">
            </form>
        </div>
    </div>
</div>

    <div id="setOffer" class="modal">
        <form method="post" action="<?php echo Router::generateUrl("shoppingchart","set-shopping-chart") ?>">
            Modifier la quantitée commandée :<br>
            <input type="number" name="quantity" placeholder="quantité"><br>
            <input type="hidden" name="offerId" id="offerId"><br>
            <input type="submit" value="Mettre à jour la quantitée que je souhaite commander"/>
        </form>
        <form method="post" action="<?php echo Router::generateUrl("shoppingchart","delete-offer") ?>">
            <input type="hidden" name="offerId" id="offerIdDEl"><br>
            <input type="submit" value="Rettirer l'offre de mon panier"/>
        </form>
    </div>

    <script type="application/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            modal.init("setOffer");
        });
    </script>

    <?php Controller::addJs("tileViewShoppingChart") ?>

<?php } else { ?>
    Votre panier est vide.
<?php } ?>