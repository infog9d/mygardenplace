<?php Controller::addCss("myAccount.css")?>
<?php Controller::addCss("search")?>

<?php Controller::requireMethod("utils","breadcrumbs",array(
    Router::generateUrl("home") => "Accueil",
    Router::generateUrl("myAccount")=> "Mon profil",
)); ?>

<?php if (Session::hasFlashbag('successPostOffer')) {?>
    <span class="alert success"><?php echo Session::getFlashbag('successPostOffer'); ?></span>
<?php } echo "<br>";
    if (Session::hasFlashbag('errorPostOffer')) {?>
        <span class="alert error"><?php echo Session::getFlashbag('errorPostOffer'); ?></span>
    <?php } ?>
<?php if (Session::hasFlashbag('successAlert')) {?>
    <span class="alert success"><?php echo Session::getFlashbag('successAlert'); ?></span>
<?php } echo "<br>";
if (Session::hasFlashbag('errorAlert')) {?>
    <span class="alert error"><?php echo Session::getFlashbag('errorAlert'); ?></span>
<?php } ?>

<div id="myProfil"  class="table">
    <div id="imageProfil">
        
        <form id="postImage" action="<?php echo Router::generateUrl("user","post-image") ?>" enctype="multipart/form-data" method="post">
            <input type="file" id="fileinput" name="picture" style="display: none;">
            <img id="selectImg" style="max-width: 432px;margin: 0 auto;display: block;" />
            <div id="addImage" class="add" style="width: auto;">Modifier mon image</div>
        </form>
        <div id="map" style="width: 100%; height: 400px;">

        </div>
    </div>
    <div id="profil">
        <h1>My Garden Place !</h1>
        <div id ="head">
            <div><h2 id="text">Mon Profil</h2></div>
            <div> Ma note</div>
            <input id="modifier"  type="button"  value="Modifier">
            <a href="<?php echo Router::generateUrl("offers","add"); ?>">Poster une annonce</a>
        </div>
        <div id="infos">
            <div id = "mesInfos">
                <?php $user = Session::get("user"); ?>
                <ul>
                    <li>Nom : <?php echo $user["lastname"]?></li>
                    <li>Prénom : <?php echo $user["firstname"]?></li>
                    <li>Date de Naissance : <?php echo $user["birthdate"]?></li>
                    <li>Mon lieu principal : <?php echo $user["cityname"]?></li>
                    <li>Mon Mail : <?php echo $user["mail"]?></li>
                    <li>Mon numéro de téléphone : <?php  echo $user["phone"]?></li>
                </ul>

            </div>
            <div id ="form">
                <form id="formulaire" method="post" action="<?php echo Router::generateUrl("profil","reRegister")?>">
                    <label for="lastname" >Nom : <input id="lastname" type="text" value="<?php echo $user["lastname"]?>"name ="lastname"></label></br>
                    <label for="firstname" >Prénom : <input id="firstname" type="text" value="<?php echo $user["firstname"]?>"name="firstname"></label></br>
                    <label for="birthdate">Ma date de naissance : <input id="birthdate" type="text" value="<?php echo $user["birthdate"]?>"name="birthdate"></label></br>
                    <label for="completeCity">Mon lieu principal : <input id="completeCity" type="text" value="<?php echo $user["cityname"]?>"name="cityname"></label></br>
                    <label for="mail" >Mon Mail : <input id="mail" type="email" value="<?php echo $user["mail"]?>"name ="mail"></label></br>
                    <label for="phone" >Mon numéro de téléphone : <input id="phone" type="text" value="<?php  echo $user["phone"]?>"name="phone"></label></br>
                    <input type="submit" value = "Valider">
                </form>
            </div>
            <div id="mesalertes"class="table">
                <h2>Mes alertes </h2>
                <form action="<?php echo Router::generateUrl("user", "post-alert"); ?>" method="post">
                    <input type="text" id="keyword" name="keyword" placeholder="Mot clé"> <input type="submit" value="enregistrer">
                </form>

                <?php if (count($params["myAlerts"])>0) {
                    ?>
                    <ol>
                        <?php foreach ($params["myAlerts"] as $alert) {
                            ?>
                            <li><?php echo $alert["keyword"] ?>
                                <a href="<?php echo Router::generateUrl("user", "rm-alert",array($alert["id"])); ?>">Supprimer</a>
                            </li>
                        <?php
                        } ?>

                    </ol>
                <?php
                }?>

            </div>
        </div>
    </div>
</div>

<div id="myvegetables" class = "table">
    <div class="container">
        <div class="pannels table">

            <?php
            $iterate = 0;

            foreach ($params["offers"] as $ad) {
            $iterate ++;
            Controller::requireMethod("offers","tileView", array("tileSize" => 4,"ad" => $ad,"delete"=>true,"view"=>true));
            if ($iterate == 3) {
            $iterate = 0;
            ?>
        </div>
        <div class="pannels table">
            <?php
            }
            }
            while($iterate!=0&&$iterate<3) {
                Controller::requireMethod("offers","tileView", array("tileSize" => 4,"ad" => $ad,"blank"=>true));
                $iterate++;
            }

            ?>

        </div>
    </div>
</div>


<?php Controller::addJs("api"); ?>
<?php Controller::addJs("autocompleteCity"); ?>
<?php Controller::addJs("imagePreview"); ?>
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMQazW751UazOtA8QF4CQqOwH4KvmqO1A">
</script>
<?php Controller::addJs("gm"); ?>
<?php Controller::beginJsBlock(); ?>
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            autocompleteCity.init("completeCity", "<?php echo Router::generateUrl("city","search"); ?>");
            autocompleteCity.init("keyword","<?php echo Router::generateUrl("category","search-categories-subcatagories-gardener"); ?>");
            gm.cities=["<?php echo $user["cityname"]?>"];
            google.maps.event.addDomListener(window, 'load', gm.init("map"));
            var form = document.getElementById("form");
            var mesInfos = document.getElementById("mesInfos");
            var head = document.getElementById("head");
            var field = document.createElement('input');
            var deleteInputs = document.getElementsByClassName("delete");
            var onSuccess = function (data) {

            };
            var annuler = function(){
                form.style.display = 'none';
                mesInfos.style.display ='block';
                head.removeChild(field);
            };
            var createAnnuler =function(){

                field.value = "Annuler";
                field.type = 'button';
                field.id = "annuler";
                head.appendChild(field);
                field.onclick = annuler;
            };
            var modifier = function () {
                createAnnuler();
                form.style.display = 'block';
                mesInfos.style.display ='none';

            };
            var callback = function () {
                var form = document.getElementById("postImage");
                form.submit();
            };

            var element = document.getElementById("modifier");
            element.onclick = modifier;
            imagePreview.init("fileinput","selectImg","addImage","<?php echo ($user["picture"] != "" && file_exists(WEBROOT."/assets/uploads/".$user["picture"]))?BASE_URL."/assets/uploads/".$user["picture"]:BASE_URL."/assets/media/unknown.png" ?>",callback);

            var img = document.getElementById("selectImg");
            var prevSrc = "";
            img.onmouseover = function () {
                if (prevSrc == "" || img.src == prevSrc) {
                    prevSrc = img.src;
                    img.style.backgroundImage = "url(\"" + img.src + "\")";
                    img.style.backgroundRepeat = "no-repeat";
                    img.src = "<?php echo BASE_URL . '/assets/media/replace.png' ?>";
                }
            };
            img.onmouseout = function () {
                if (img.src == "<?php echo BASE_URL . '/assets/media/replace.png' ?>") {
                    img.src = prevSrc;
                }
                img.style.backgroundImage = "none";
            };
            for (var numberOfUser = 0; numberOfUser < deleteInputs.length; numberOfUser++) {
                var deleteInput = deleteInputs[numberOfUser];

                deleteInput.onclick = function (evt) {
                    evt.preventDefault();
                    var target = evt.target || evt.srcElement;
                    var id = target.dataset.id;
                    if (confirm("voulez vous vraiment supprimer l'entrée " + id + " ?")) {
                        window.location = '<?php echo Router::generateUrl("offers","delete-offer") ?>' + '/' + id;
                    }
                }
            }
        });
            </script>

<?php Controller::endJsBlock(); ?>