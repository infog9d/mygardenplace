<form action="<?php echo Router::generateUrl("user","register") ?>" method="post" autocomplete="off">
    <label>
        <span class="label">Nom</span>
        <input type="text" placeholder="Doe" name="lastname"/>
    </label><br>
    <label>
        <span class="label">Prenom</span>
        <input type="text" placeholder="John" name="firstname"/>
    </label><br>
    <label>
        <span class="label">E-mail</span>
        <input type="email" placeholder="john.doe@example.com" name="mail"/>
    </label><br>
    <label>
        <span class="label">Mot de passe</span>
        <input type="password" placeholder="********" name="password"/>
    </label><br>
    <label>
        <span class="label">Date de naissance</span>
        <input type="text" placeholder="01/10/1980" name="birthdate"/>
    </label><br>
    <label>
        <span class="label">Ville</span>
        <input type="text" id="completeCity" placeholder="Paris" name="city"/>
    </label><br>
    <label>
        <span class="label">Numéro de téléphone</span>
        <input type="text" placeholder="01 01 01 01 01" name="phone"/>
    </label><br>
    <input type="submit" value="S'inscrire">
</form>

<?php echo Controller::requireJs("api"); ?>
<?php echo Controller::requireJs("autocompleteCity"); ?>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        autocompleteCity.init("completeCity", "<?php echo Router::generateUrl("city","search"); ?>");
    });
</script>