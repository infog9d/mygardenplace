<?php Controller::addCss('adminPresentation'); ?>
<?php Controller::requireMethod("utils","breadcrumbs",array(
    Router::generateUrl("home") => "Accueil",
    Router::generateUrl("adminPresentation")=> "Qui-sommes-nous?",
)); ?>
<div id="background" class="table">
<div id="presentation1" class="table">
    <div id="poster_membre_1" class="type_1 table-3">
        <img class="image_A" src="<?php echo BASE_URL."/assets/media/leprefere.jpg"; ?>" alt="" />
    </div>
    <div class="membre_1 table-9">
        <p class="nom_1">Wandrille Okongo </p>
        <p class="description_1">
            Etudiant à l'Institut Supérieur d'Electronique de Paris, où il suit une formation approfondie en informatique
            dans l'apprentissage de plusieurs langages de programmation et developpement web. Il a participé
            à plusieurs projets tels que des jeux vidéos. Par sa bonne humeur Wandrile à su apporter un côté exotique au groupe.
            Son leitmotiv "tou dis" n'a cependant pas encore été compris par tous les membres groupe.
        </p>

    </div>
</div>
<div id="presentation2" class="table">
    <div class="membre_2 table-9">
        <p class="nom_2">Patrick Lechevalier </p>
        <p class="description_2">
            Etudiant à l'Institut Supérieur d'Electronique de Paris, où il suit une formation approfondie en informatique
            dans l'apprentissage de plusieurs langages de programmation et developpement web. Il a participé
            à plusieurs projets tels que des jeux vidéos. D'origine asiatique, il a su observer la façon de travailler de ses collègues afin
            d'en tirer le meilleur et de s'améliorer.
        </p>
    </div>
    <div id="poster_membre_2" class="type_2 table-3">
        <img class="image_B" src="<?php echo BASE_URL."/assets/media/image_1.jpg"; ?>" alt="" />
    </div>
</div>
<div id="presentation3" class="table">
    <div id="poster_membre_3" class="type_3 table-3">
        <img class="image_C" src="<?php echo BASE_URL."/assets/media/image_1.jpg"; ?>" alt="" />
    </div>
    <div class="membre_3 table-9">
        <p class="nom_3">Jaswinder Guru</p>
        <p class="description_3">
            Etudiant à l'Institut Supérieur d'Electronique de Paris, où il suit une formation approfondie en informatique
            dans l'apprentissage de plusieurs langages de programmation et developpement web. Il a participé
            à plusieurs projets tels que des jeux vidéos. Jaswinder est un homme complet qui ne trouve que n'a pas le temps de dormir,
            étudiant de jour, portier de nuit il a su donner un coup de main et partager ses connaisssances en latex et en javascript.
        </p>

    </div>
</div>
<div id="presentation4" class="type table">
    <div class="membre_4 table-9">
        <p class="nom_3">Jean-Baptiste Watenberg</p>
        <p class="description_4">
            Etudiant à l'Institut Supérieur d'Electronique de Paris, où il suit une formation approfondie en informatique
            dans l'apprentissage de plusieurs langages de programmation et developpement web. Membre de junior isep
            ses compétences sont sans limites. Depuis son enfance Jean-Baptiste est passionné par la programmation. Il s'est autoproclamé chef
            du groupe et a rapidement instauré un régime de dictature totalitaire.

    </div>
    <div id="poster_membre_4" class="type_4 table-3">
        <img class="image_D" src="<?php echo BASE_URL."/assets/media/jb.jpg"; ?>" alt="" />
    </div>
</div>

<div id="presentation5" class="type table">
    <div id="poster_membre_1" class="type_5 table-3">
        <img class="image_E" src="<?php echo BASE_URL."/assets/media/image_1.jpg"; ?>" alt="" />
    </div>
    <div class="membre_5 table-9">
        <p class="nom_5">Gauthier Vigouroux</p>
        <p class="description_5">
            Etudiant à l'Institut Supérieur d'Electronique de Paris, où il suit une formation approfondie en informatique
            spécialement dans l'apprentissage de plusieurs langages de programmation et developpement web.Il a participé
            à plusieurs projets tels que des jeux vidéos. Atteint de Narcolepsie Gauthier a su surpasser sa maladie pour réaliser ce projet.

            

        </p>

    </div>
</div>
</div>

