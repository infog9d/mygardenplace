<!DOCTYPE html>
<html>
<head lang="fr">
    <meta charset="UTF-8">
    <style>
        body
        {
            margin: 0;
            padding: 0;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 13px;
            overflow-x: hidden;
        }

        #header {
            width: 100%;
            /*border-bottom: 1px solid black;*/
            /*position: fixed;*/
            top: 0;
            z-index: 1;

        }

        #footer {
            border-top: 1px solid black;
            padding: 20px 0;
            background: #fff;
        }

        #menu {
            margin: 0 auto;
        }

        h1
        {
            font-weight: normal;
        }
        ul
        {

        }

        .table {
            display: table;
            table-layout:fixed;
            width: 100%;
        }

        .table-2 {
            display: table-cell;
            width: 17%;
        }

        .table-3 {
            display: table-cell;
            width: 25%;
        }

        .table-4 {
            display: table-cell;
            width: 33%;
        }

        .table-6 {
            display: table-cell;
            vertical-align: top;
            width: 50%;
        }

        .table-7 {
            display: table-cell;
            width: 58%;
        }

        .table-8 {
            display: table-cell;
            width: 67%;
        }

        .table-9 {
            display: table-cell;
            width: 75%;
        }

        .table-10 {
            display: table-cell;
            width: 83%;
        }

        #title
        {
            font-family:fantasy;
            color: blueviolet;



        }

        .menu li
        {

            list-style: none;
            display: inline;
        }
        .inscription li
        {

            list-style: none;
            display: inline;

        }

        .fixtop {
            position: relative;
            top: -16px;
        }

        .middle {
            vertical-align: middle;
        }

        .nav {
            clear: left;
            float: left;
            list-style: none;
            padding: 0;
            margin: 0;
            position:relative;
            left:50%;
            text-align: center;
        }



        .nav li {
            float: left;
            position:relative;
            right:50%;
        }

        .nav li a {
            display: block;
            padding: 2px 10px;
            border-right: 1px solid black;
        }

        .nav .last a {
            border: none;
        }

        .nav li a:hover {

        }

        #footer {
            width: 100%;
            text-align: center;
        }

        .floatright {
            float: right;
            left: inherit;
        }

        .resetRight {
            right: auto!important;
        }

        .logoContainer {
            max-width: 60px;
        }

        .container {
            padding-right: 0px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        .modal {
            width: 400px;
            text-align: center;
        }

        .modal label {
            display: table;
            width: 100%;
        }

        .modal label .label {
            display: table-cell;
            width: 33%;
            text-align: right;
        }

        .modal label input, .modal label textarea {
            display: table-cell;
            width: 67%;
        }

        .center {
            width: auto;
            margin: 0 auto;
        }

        .txtcenter {
            text-align: center;
        }

        .imag {
            max-width: 100%;
            min-width: 100%;
        }

        .button {
            display: inline-block;
            padding: 5px;
            cursor: pointer;
            box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.7);
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            margin: 10px;
        }

        .add {
            width: 29px;
            border-radius: 25px;
            background: #2d9244;
            padding: 0;
            padding-top: 7px;
            padding-bottom: 7px;
            text-align: center;
            color: white;
        }

        .rm {
            background: red;
        }

        .button:active {
            box-shadow: inset 0px 0px 5px 0px rgba(0, 0, 0, 0.7);
        }

        .imgbord {
            height: 286px;
            background-size: cover!important;
            background-repeat: no-repeat!important;
            background-position: center!important;
        }

        @media (max-width: 1015px) {
            .table-7 {
                display: table-cell;
                width: 50%!important;
            }
            .table-2 {
                display: table-cell;
                width: 25%!important;
            }

            h1 {
                font-size: 22px!important;
            }

            #potager {
                width: 25%!important;
            }

            .proximity {
                width: 50%!important;
            }
        }

        .pannels {
            margin-top: 20px;
            margin-bottom: 20px;
            margin-right: 0px;
            margin-left: 0px;
        }

        .panel {
            padding-right: 20px;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            float: left;
            display: block!important;
        }

    </style>
</head>
<body>
<h1><?php echo $params["sentence"] ?></h1>
<?php
if (count($params["offers"])>0) { ?>
<div class="container">
    <div class="pannels table">
        <?php
        $iterator = 0;
        foreach($params["offers"] as $offer) {
        $array = array("tileSize"=> 4, "ad" => $offer,"base_url"=>"http://mygardenplace.iseplive.fr");
        if (isset($offer["exchangeAgainst"])) {
            $array["shoppingchart"] = true;
            $array["exchangeAgainst"] = $offer["exchangeAgainst"];
        }
        Controller::requireMethod("offers","tileView",$array);
        if ($iterator<2) {

            $iterator++;
        } else {
        $iterator = 0;
        ?>
    </div>
    <div class="pannels table">
        <?php
        }


        }
        while($iterator!=0&&$iterator<3){
            Controller::requireMethod("offers","tileView",array("tileSize"=>4,"ad"=>$offer,"blank"=>true));
            $iterator++;
        }
        ?>

    </div>
</div>
<div class="prix">
    <div class="ttc">
        total TTC : <?php echo $params["totalTTC"]; ?>
    </div>
    <div class="ht">
        total HT : <?php echo $params["totalHT"]; ?>
    </div>
</div>
<?php } ?>
</body>
</html>