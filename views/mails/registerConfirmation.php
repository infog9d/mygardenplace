<!DOCTYPE html>
<html>
<head lang="fr">
    <meta charset="UTF-8">
</head>
<body>
    <h1>Bienvenu(e) sur mygardenplace !</h1>
    <p>
        Bonjour  <?php echo $params["firstname"] ?> <?php echo $params["lastname"] ?> vous venez de vous inscrire sur mygardenplace et nous vous en remercions.
        Pour completer votre inscription merci de cliquer sur le lien ci-dessous :
        <a href="http://<?php echo $_SERVER["SERVER_NAME"].Router::generateUrl("user","check-mail",array($params["token"])); ?>">
            http://<?php echo $_SERVER["SERVER_NAME"].Router::generateUrl("user","check-mail",array($params["token"])); ?>
        </a>
    </p>
</body>
</html>