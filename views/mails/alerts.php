<!DOCTYPE html>
<html>
<head lang="fr">
    <meta charset="UTF-8">
<style>
    body
    {
        margin: 0;
        padding: 0;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13px;
        overflow-x: hidden;
    }

    #header {
        width: 100%;
        /*border-bottom: 1px solid black;*/
        /*position: fixed;*/
        top: 0;
        z-index: 1;

    }

    #footer {
        border-top: 1px solid black;
        padding: 20px 0;
        background: #fff;
    }

    #menu {
        margin: 0 auto;
    }

    h1
    {
        font-weight: normal;
    }
    ul
    {

    }

    .table {
        display: table;
        table-layout:fixed;
        width: 100%;
    }

    .table-2 {
        display: table-cell;
        width: 17%;
    }

    .table-3 {
        display: table-cell;
        width: 25%;
    }

    .table-4 {
        display: table-cell;
        width: 33%;
    }

    .table-6 {
        display: table-cell;
        vertical-align: top;
        width: 50%;
    }

    .table-7 {
        display: table-cell;
        width: 58%;
    }

    .table-8 {
        display: table-cell;
        width: 67%;
    }

    .table-9 {
        display: table-cell;
        width: 75%;
    }

    .table-10 {
        display: table-cell;
        width: 83%;
    }

    #title
    {
        font-family:fantasy;
        color: blueviolet;



    }

    .menu li
    {

        list-style: none;
        display: inline;
    }
    .inscription li
    {

        list-style: none;
        display: inline;

    }

    .fixtop {
        position: relative;
        top: -16px;
    }

    .middle {
        vertical-align: middle;
    }

    .nav {
        clear: left;
        float: left;
        list-style: none;
        padding: 0;
        margin: 0;
        position:relative;
        left:50%;
        text-align: center;
    }



    .nav li {
        float: left;
        position:relative;
        right:50%;
    }

    .nav li a {
        display: block;
        padding: 2px 10px;
        border-right: 1px solid black;
    }

    .nav .last a {
        border: none;
    }

    .nav li a:hover {

    }

    #footer {
        width: 100%;
        text-align: center;
    }

    .floatright {
        float: right;
        left: inherit;
    }

    .resetRight {
        right: auto!important;
    }

    .logoContainer {
        max-width: 60px;
    }

    .container {
        padding-right: 0px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }

    .modal {
        width: 400px;
        text-align: center;
    }

    .modal label {
        display: table;
        width: 100%;
    }

    .modal label .label {
        display: table-cell;
        width: 33%;
        text-align: right;
    }

    .modal label input, .modal label textarea {
        display: table-cell;
        width: 67%;
    }

    .center {
        width: auto;
        margin: 0 auto;
    }

    .txtcenter {
        text-align: center;
    }

    .imag {
        max-width: 100%;
        min-width: 100%;
    }

    .imgbord {
        height: 286px;
        background-size: cover!important;
        background-repeat: no-repeat!important;
        background-position: center!important;
    }

    .button {
        display: inline-block;
        padding: 5px;
        cursor: pointer;
        box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.7);
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        margin: 10px;
    }

    .add {
        width: 29px;
        border-radius: 25px;
        background: #2d9244;
        padding: 0;
        padding-top: 7px;
        padding-bottom: 7px;
        text-align: center;
        color: white;
    }

    .rm {
        background: red;
    }

    .button:active {
        box-shadow: inset 0px 0px 5px 0px rgba(0, 0, 0, 0.7);
    }

    @media (max-width: 1015px) {
        .table-7 {
            display: table-cell;
            width: 50%!important;
        }
        .table-2 {
            display: table-cell;
            width: 25%!important;
        }

        h1 {
            font-size: 22px!important;
        }

        #potager {
            width: 25%!important;
        }

        .proximity {
            width: 50%!important;
        }
    }

    .pannels {
        margin-top: 20px;
        margin-bottom: 20px;
        margin-right: 0px;
        margin-left: 0px;
    }

    .panel {
        padding-right: 20px;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        float: left;
        display: block!important;
    }

</style>
</head>
<body>
<h1>Des membres ont posté des offres susceptible de vous intéresser :</h1>

<div class="container">
        <?php
        if (count($params["offersByKeyWords"])>0) {
            foreach ($params["offersByKeyWords"] as $keyword => $offers) {
                if (count($offers)>0) {
                ?>
                <h2>Annonces correspondantes à votre recherche "<?php echo $keyword ?>" :</h2>
                <div class="pannels table">
                <?php
                    $iterate = 0;
                    foreach ($offers as $offer) {
                        $iterate++;
                        if ($iterate <= 3) {
                            Controller::requireMethod("offers", "tileView", array("tileSize" => 4, "ad" => $offer, "view" => true));
                        }
                    }
                ?>
                </div>
                <a href="<?php echo Router::generateUrl("search","index"); ?>?name=<?php echo ($params["typesByKeyword"][$keyword] != "gardener")?$keyword:""; ?>&gardener=<?php echo ($params["typesByKeyword"][$keyword] == "gardener")?$keyword:""; ?>&city=">Voir plus d'annonces correspondant à cette recherche</a>
    <?php }
         }
        } ?>

</div>

<a href="<?php echo Router::generateUrl("user","account"); ?>">Gerer mes alertes</a>

</body>
</html>