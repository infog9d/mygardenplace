<?php Controller::addCss("profileUser.css"); ?>
<?php Controller::requireMethod("utils","breadcrumbs",array(
    Router::generateUrl("home") => "Accueil",
    Router::generateUrl("profileUser")=> "Profil de ". $params["user"]->firstname ." ".  $params["user"]->lastname,
)); ?>

<div class="container">
    <div class="container-table-100">
        <div class="container-table-25">
            <div class="left-container">
                <div class="inline-profile">
                <img class="img_profile" src="<?php echo ($params["user"]->picture!=""&&file_exists(WEBROOT."/assets/uploads/".$params["user"]->picture))?BASE_URL."/assets/uploads/".$params["user"]->picture:BASE_URL."/assets/media/unknown.png" ?>" alt=""/>
                </div>
                <div class="inline-map">
                    <div id="maps"></div>

                </div>

            </div>
        </div>

        <div class="container-table-75">
            <div class="right-container">
                <h2 class="title"><p>Bienvenue dans le Garden Place de <?php echo $params["user"]->firstname ." ".  $params["user"]->lastname?></h2><br/>

                <div class="container-table-100">

                    <div class="container-table-50">
                        <h3 class="title"> Les ventes courantes de <?php echo $params["user"]->firstname ." ".  $params["user"]->lastname?></h3>
                    </div>

                    <div class="container-table-50">
                        <h3 class="b"> Note globale : <?php echo $params["grade"] ?></h3>
                    </div>
                </div>
                <div class="container-table-33">
                    <div class="container">
                        <div class="pannels table">
                            <?php
                            $iterator = 0;
                            foreach($params["offers"] as $ad) {
                            Controller::requireMethod("profil", "offerView", array("ad" => $ad, "tileSize" => 6));
                            if ($iterator<1) {
                                $iterator++;
                            } else {
                            $iterator = 0;
                            ?>
                        </div>
                        <div class="pannels table">
                            <?php
                            }
                            }
                            while($iterator!=0&&$iterator<2){
                                Controller::requireMethod("offers","tileView",array("tileSize"=>6,"ad"=>$ad,"blank"=>true));
                                $iterator++;
                            }
                            ?>

                        </div>
                    </div>
                    <div class="comments">
                        <?php foreach($params["comments"] as $comment) { ?>
                            <div class="comment table">
                                <div class="table-3">
                                    <img src="<?php echo ($comment["owner_picture"]!=""&&file_exists(WEBROOT."/assets/uploads/".$comment["owner_picture"]))?BASE_URL."/assets/uploads/".$comment["owner_picture"]:BASE_URL."/assets/media/unknown.png" ?>">
                                    <br>
                                    <div class="underPicture">
                                    Par <a href="<?php echo Router::generateUrl("profileUser","index",array($comment["owner_id"])); ?>"><?php echo $comment["owner_firstname"]." ".$comment["owner_lastname"]; ?></a><br>
                                    le <?php echo Utils::formatSqlDate($comment["date"],"d/m/Y") ?> à <?php echo Utils::formatSqlDate($comment["date"],"H:i") ?>
                                    </div>
                                </div>
                                <div class="table-9">
                                    <div class="echoComment">
                                    <?php echo $comment["content"]; ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if(Session::hasFlashbag("errorComment")) {
                        ?>
                        <div class="error">
                            <?php echo Session::getFlashbag("errorComment"); ?>
                        </div>
                    <?php

                    } ?>
                    <form id="myform" action="<?php echo Router::generateUrl("ProfileUser","send") ?>" method="post">
                        Evaluez cet utilisateur :
                        <fieldset>
                            <div class="rating">
                                <input name="grade" id="e5" type="radio" value="5"><label class="labe" for="e5">☆</label>
                                <input name="grade" id="e4" type="radio" value="4"><label class="labe" for="e4">☆</label>
                                <input name="grade" id="e3" type="radio" value="3"><label class="labe" for="e3">☆</label>
                                <input name="grade" id="e2" type="radio" value="2"><label class="labe" for="e2">☆</label>
                                <input name="grade" id="e1" type="radio" value="1"><label class="labe" for="e1">☆</label>
                                <input name="ownerId"  type="hidden" value="<?php echo $params["user"]->id ?>">
                            </div>
                                <label for="message">Commentaire</label><br>
                                <textarea name="message" id="message" placeholder="Message"></textarea><br/>
                            <input type="submit" name="submit"/>
                        </fieldset>
                    </form>

        </div>
    </div>

</div>

    </div>

</div>


<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMQazW751UazOtA8QF4CQqOwH4KvmqO1A">
</script>

<?php Controller::addJS("addToChart.js");?>
<?php Controller::requireMethod("offers","addToChart"); ?>
<?php Controller::addJs("gm"); ?>
<script type="application/javascript">
    document.addEventListener("DOMContentLoaded", function() {
        gm.cities = ["<?php echo$params["city"]['name'] ?>  "];
        google.maps.event.addDomListener(window, 'load', gm.init("maps"));


        var grades = document.getElementsByClassName("labe");

        for (var i = 0; i < grades.length; i++)
        {
            grades[i].dataset.i = i;
            grades[i].onclick = function(evt)
            {
                var element = evt.target || evt.srcElement;
                var j=0;
                if ( !element.classList.contains("selected") )
                {
                    for (j=element.dataset.i; j<grades.length; j++) {
                        grades[j].classList.add("selected");
                    }
                }
                else if ( element.classList.contains("selected") )
                {
                    for (j=0; j<grades.length; j++) {
                        grades[j].classList.remove("selected");
                    }
                    for (j=element.dataset.i; j<grades.length; j++) {
                        grades[j].classList.add("selected");
                    }
                }
            }
        }
    });

</script>


