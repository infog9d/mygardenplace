
<?php Controller::addCss("accueil.css"); ?>
<div id="devise">
    <img class="imag" id="cover" src="<?php echo BASE_URL."/assets/media/panier_in_potager.jpg"; ?>">
    <h1 id="bienvenue">Bienvenue dans votre jardin</h1>
    <h1 id="legend">Troquez, achetez près de chez vous !</h1>

</div>
<div class="backwhite">
<div id="searchreplacement" style="display: none;"></div>
<div id="recherche">
    <div id="formulaire">
        <form action="<?php echo Router::generateUrl("search","index") ?>" method="get">
            <div class="table center">
                <div class="table-7">
                    <label for="potager">Recherche par nom
                        <input type="text" name="name" placeholder="Fruits, Legumes, Catégorie" size="11" id="potager" />
                    </label>
                    <label for="gardener"> ou
                        <input type="text" name="gardener" placeholder="Gardener" size="13" id="gardener" />
                    </label>
                </div>
                <div class="table-3 proximity">
                    <label for="ville"> A proximité de
                        <input type="text" id="searchCity" name="city" size="11" placeholder="ville" />
                    </label>
                    <input type="submit" id="rechercher" value="Rechercher" />
                </div>
            </div>

        </form>

    </div>

</div>
<div class="container">
    <div class="pannels table">

        <?php
            $iterate = 0;
            foreach ($params["lastAds"] as $ad) {
                $iterate ++;
                Controller::requireMethod("offers","tileView", array("tileSize" => 4,"ad" => $ad));
                if ($iterate == 3) {
                    $iterate = 0;
        ?>
    </div>
    <div class="pannels table">
        <?php
                }
            }
            while($iterate!=0&&$iterate<3) {
                Controller::requireMethod("offers","tileView", array("tileSize" => 4,"ad" => $ad,"blank"=>true));
                $iterate++;
            }

        ?>

    </div>
</div>
</div>
<?php Controller::addJs("carousel"); ?>
<?php Controller::addJs("scrollHead"); ?>
<?php Controller::addJs("autocompleteCity"); ?>
<?php Controller::addJs("tileView"); ?>
<?php Controller::beginJsBlock(); ?>
<script type="text/javascript">
    var tab = [
        "<?php echo BASE_URL."/assets/media/panier_in_potager.jpg"; ?>",
        "<?php echo BASE_URL."/assets/media/triple.PNG"; ?>",
        "<?php echo BASE_URL."/assets/media/image1.jpg"; ?>",
        "<?php echo BASE_URL."/assets/media/image2.jpg"; ?>",
        "<?php echo BASE_URL."/assets/media/panier_de_legumes.jpg"; ?>",
    ];
    carousel(tab);
    var viewOffer = "<?php echo Router::generateUrl("offers","view") ?>/";
    autocompleteCity.init("searchCity", "<?php echo Router::generateUrl("city","search"); ?>");
    autocompleteCity.init("gardener", "<?php echo Router::generateUrl("user","search"); ?>");
</script>
<?php Controller::endJsBlock(); ?>