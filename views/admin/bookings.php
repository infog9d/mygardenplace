<?php Controller::addCss("adminuser"); ?>
Voici la liste des transactions de cet utilisateur :
<table>
    <thead>
    <tr>
        <th>
            Id
        </th>
        <th>
            Total
        </th>
        <th>
            Date
        </th>
        <th>
            Payée
        </th>
    </tr>
    </thead>
<?php foreach($params["bookings"] as $bookings) {
    ?>
<tr>
    <td>
        <?php echo $bookings["id"]; ?>
    </td>
    <td>
        <?php echo $bookings["total"]; ?>
    </td>
    <td>
        <?php echo $bookings["date"]; ?>
    </td>
    <td>
        <?php echo $bookings["payed"]?"Oui":"Non"; ?>
    </td>
</tr>
<?php
} ?>

</table>

Voici la liste des offres de cet utilisateur :
<div class="container">
    <div class="pannels table">

        <?php
        $iterate = 0;
        foreach ($params["offers"] as $ad) {
        $iterate ++;
        Controller::requireMethod("offers","tileView", array("tileSize" => 4,"ad" => $ad,"delete"=>true,"ban"=>true,"view"=>true));
        if ($iterate == 3) {
        $iterate = 0;
        ?>
    </div>
    <div class="pannels table">
        <?php
        }
        }
        while($iterate!=0&&$iterate<3) {
            Controller::requireMethod("offers","tileView", array("tileSize" => 4,"ad" => $ad,"blank"=>true));
            $iterate++;
        }

        ?>

    </div>
</div>


<?php Controller::addJs("api"); ?>
<?php Controller::beginJsBlock(); ?>
    <script type="application/javascript">
        var viewOffer = "<?php echo Router::generateUrl("offers","view") ?>/";
        document.addEventListener("DOMContentLoaded", function(event) {
            var setBanInputs = document.getElementsByClassName("setBan");
            var deleteInputs = document.getElementsByClassName("delete");

            var onSuccess = function (data) {

            };

            for (var numberOfUser = 0; numberOfUser < setBanInputs.length; numberOfUser++) {
                var setBanInput = setBanInputs[numberOfUser];
                var deleteInput = deleteInputs[numberOfUser];
                setBanInput.onclick = function (evt) {
                    //evt.preventDefault();
                    var target = evt.target || evt.srcElement;
                    var id = target.dataset.id;
                    api.get("<?php echo Router::generateUrl("admin","ban-offer"); ?>" + "/" + id, {}, onSuccess);
                };

                deleteInput.onclick = function (evt) {
                    evt.preventDefault();
                    var target = evt.target || evt.srcElement;
                    var id = target.dataset.id;
                    if (confirm("voulez vous vraiment supprimer l'entrée " + id + " ?")) {
                        window.location = '<?php echo Router::generateUrl("admin","delete-offer") ?>' + '/' + id;
                    }
                }
            }

        });
    </script>
<?php Controller::endJsBlock(); ?>