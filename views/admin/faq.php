<?php Controller::addCss("adminuser"); ?>

    <a href="#" id="addFaq">Ajouter une question</a>

<table>
    <thead>
    <tr>
        <th>
            Id
        </th>
        <th>
            Titre
        </th>
        <th>
            Contenu
        </th>
    </tr>
    </thead>
    <?php
        foreach($params["faqs"] as $faq) {
        ?>
            <tr class="open" data-id="<?php echo $faq["id"]; ?>">
                <td>
                    <?php echo $faq["id"]; ?>
                </td>
                <td id="title<?php echo $faq["id"]; ?>">
                    <?php echo $faq["title"] ?>
                </td>
                <td id="content<?php echo $faq["id"]; ?>">
                    <?php echo $faq["content"] ?>
                </td>
            </tr>
    <?php
        }
    ?>
</table>

<div class="modal" id="manageFaq">
    Ajouter ou modifier une question :
    <hr>
    <form method="post" action="" id="addOrSet">
        <label>
            Titre
            <input type="text" name="title" id="title"/>
        </label>
        <label>
            Contenu
            <textarea name="content" id="content"></textarea>
        </label>
        <input type="submit" value="envoyer"/>
    </form>

    <form id="delCat" action="<?php echo Router::generateUrl("admin","delete-faq"); ?>/" method="post" style="display: none;">
        <input type="submit" value="supprimer"/>
    </form>
</div>
<?php Controller::beginJsBlock(); ?>
    <script type="application/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            modal.init("manageFaq");
            var addFaqBtn = document.getElementById("addFaq");
            addFaqBtn.onclick = function (evt) {
                document.getElementById("addOrSet").action = "<?php echo Router::generateUrl("admin","add-faq") ?>";
                document.getElementById("delCat").style.display = "none";
                modal.show("manageFaq");
            };
            var lines = document.getElementsByClassName("open");
            for(var i=0;i<lines.length;i++) {
                lines[i].onclick = function (evt) {
                    var target = evt.target||evt.srcElement;
                    var id = utils.getOfferId(target);
                    console.log(id);
                    document.getElementById("title").value = document.getElementById("title"+id).innerHTML.trim();
                    document.getElementById("content").value = document.getElementById("content"+id).innerHTML.trim();
                    document.getElementById("addOrSet").action = "<?php echo Router::generateUrl("admin","set-faq") ?>/"+id;
                    document.getElementById("delCat").style.display = "block";
                    document.getElementById("delCat").action = "<?php echo Router::generateUrl("admin","delete-faq"); ?>/"+id;
                    modal.show("manageFaq");
                }
            }
        });
    </script>
<?php Controller::endJsBlock(); ?>