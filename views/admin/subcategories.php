<?php Controller::addCss("adminuser"); ?>

    <a href="#" id="addSubcategory">Ajouter une sous-categorie</a>

<table>
    <thead>
    <tr>
        <th>
            Id
        </th>
        <th>
            Titre
        </th>
        <th>
            Category
        </th>
    </tr>
    </thead>
    <?php
        foreach($params["subcategories"] as $Subcategory) {
        ?>
            <tr class="open" data-id="<?php echo $Subcategory["id"]; ?>">
                <td>
                    <?php echo $Subcategory["id"]; ?>
                </td>
                <td id="name<?php echo $Subcategory["id"]; ?>">
                    <?php echo $Subcategory["name"] ?>
                </td>
                <td id="category<?php echo $Subcategory["id"]; ?>" data-categoryId="<?php echo $Subcategory["category_id"] ?>">
                    <?php echo $Subcategory["category_name"] ?>
                </td>
            </tr>
    <?php
        }
    ?>
</table>

<div class="modal" id="manageSubcategory">
    Ajouter ou modifier une sous-categorie :
    <hr>
    <form method="post" action="" id="addOrSet">
        <label>
            Nom
            <input type="text" name="name" id="name"/>
        </label>
        <label>
            Catégorie associée :
            <select name="categoryId" id="categoryId">
                <?php foreach($params["categories"] as $category) { ?>
                    <option value="<?php echo $category["id"]; ?>"><?php echo $category["name"]; ?></option>
                <?php } ?>
            </select>
        </label>
        <input type="submit" value="envoyer"/>
    </form>

    <form id="delCat" action="<?php echo Router::generateUrl("admin","delete-cubcategory"); ?>/" method="post" style="display: none;">
        <input type="submit" value="supprimer"/>
    </form>
</div>
<?php Controller::beginJsBlock(); ?>
    <script type="application/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            modal.init("manageSubcategory");
            var addSubcategoryBtn = document.getElementById("addSubcategory");
            addSubcategoryBtn.onclick = function (evt) {
                document.getElementById("addOrSet").action = "<?php echo Router::generateUrl("admin","add-subcategory") ?>";
                document.getElementById("delCat").style.display = "none";
                modal.show("manageSubcategory");
            };
            var lines = document.getElementsByClassName("open");
            for(var i=0;i<lines.length;i++) {
                lines[i].onclick = function (evt) {
                    var target = evt.target||evt.srcElement;
                    var id = utils.getOfferId(target);
                    console.log(id);
                    document.getElementById("name").value = document.getElementById("name"+id).innerHTML.trim();
                    document.getElementById("categoryId").value = document.getElementById("category"+id).dataset.categoryId;
                    document.getElementById("addOrSet").action = "<?php echo Router::generateUrl("admin","set-subcategory") ?>/"+id;
                    document.getElementById("delCat").style.display = "block";
                    document.getElementById("delCat").action = "<?php echo Router::generateUrl("admin","delete-subcategory"); ?>/"+id;
                    modal.show("manageSubcategory");
                }
            }
        });
    </script>
<?php Controller::endJsBlock(); ?>