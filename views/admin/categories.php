<?php Controller::addCss("adminuser"); ?>

    <a href="#" id="addCategory">Ajouter une categorie</a>

<table>
    <thead>
    <tr>
        <th>
            Id
        </th>
        <th>
            Titre
        </th>
    </tr>
    </thead>
    <?php
        foreach($params["categories"] as $category) {
        ?>
            <tr class="open" data-id="<?php echo $category["id"]; ?>">
                <td>
                    <?php echo $category["id"]; ?>
                </td>
                <td id="name<?php echo $category["id"]; ?>">
                    <?php echo $category["name"] ?>
                </td>
            </tr>
    <?php
        }
    ?>
</table>

<div class="modal" id="manageCategory">
    Ajouter ou modifier une categorie :
    <hr>
    <form method="post" action="" id="addOrSet">
        <label>
            Nom
            <input type="text" name="name" id="name"/>
        </label>
        <input type="submit" value="envoyer"/>
    </form>

    <form id="delCat" action="<?php echo Router::generateUrl("admin","delete-category"); ?>/" method="post" style="display: none;">
        <input type="submit" value="supprimer"/>
    </form>
</div>
<?php Controller::beginJsBlock(); ?>
    <script type="application/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            modal.init("manageCategory");
            var addCategoryBtn = document.getElementById("addCategory");
            addCategoryBtn.onclick = function (evt) {
                document.getElementById("addOrSet").action = "<?php echo Router::generateUrl("admin","add-category") ?>";
                document.getElementById("delCat").style.display = "none";
                modal.show("manageCategory");
            };
            var lines = document.getElementsByClassName("open");
            for(var i=0;i<lines.length;i++) {
                lines[i].onclick = function (evt) {
                    var target = evt.target||evt.srcElement;
                    var id = utils.getOfferId(target);
                    console.log(id);
                    document.getElementById("name").value = document.getElementById("name"+id).innerHTML.trim();
                    document.getElementById("addOrSet").action = "<?php echo Router::generateUrl("admin","set-category") ?>/"+id;
                    document.getElementById("delCat").style.display = "block";
                    document.getElementById("delCat").action = "<?php echo Router::generateUrl("admin","delete-category"); ?>/"+id;
                    modal.show("manageCategory");
                }
            }
        });
    </script>
<?php Controller::endJsBlock(); ?>