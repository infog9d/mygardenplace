<?php Controller::addCss("adminuser"); ?>
<table>
    <thead>
        <tr>
            <th>Id</th>
            <th>Image de profil</th>
            <th>Prenom</th>
            <th>Nom</th>
            <th>Mail</th>
            <th>Date d'inscription</th>
            <th>Telephone</th>
            <th>Date de naissance</th>
            <th>Mail verifié</th>
            <th>Compte valide (decocher pour banir)</th>
            <th>Administrateur</th>
            <th>Supprimer</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($params["users"] as $user) { ?>
    <tr class="showUser" data-id="<?php echo $user["id"] ?>">
        <td><?php echo $user["id"] ?></td>
        <td><img class="imag profilImage" src="<?php echo ($user["picture"] != "")?BASE_URL."/assets/uploads/".$user["picture"]:BASE_URL."/assets/media/unknown.png"; ?>" alt=""/></td>
        <td><?php echo $user["firstname"] ?></td>
        <td><?php echo $user["lastname"] ?></td>
        <td><?php echo $user["mail"] ?></td>
        <td><?php echo $user["registred_on"] ?></td>
        <td><?php echo $user["phone"] ?></td>
        <td><?php echo $user["birthdate"] ?></td>
        <?php
        $userc = Session::get("user");
        if ($userc["id"] == $user["id"]) { ?>
            <td colspan="4">Vous ne pouvez pas administrer votre compte.</td>
        <?php } else { ?>
        <td><input class="setMail" data-id="<?php echo $user["id"] ?>" type="checkbox" <?php if ($user["mailvalid"] == 1) { ?>checked value="1" <?php } ?>></td>
        <td><input class="setBan" data-id="<?php echo $user["id"] ?>" type="checkbox" <?php if ($user["valid"] == 1) { ?>checked value="1"<?php } ?>></td>
        <td><input class="setAdmin" data-id="<?php echo $user["id"] ?>" type="checkbox" <?php if ($user["is_admin"] == 1) { ?>checked value="1"<?php } ?>></td>
        <td><input type="submit" class="button delete" data-id="<?php echo $user["id"] ?>" value="delete"></td>
        <?php } ?>
    </tr>
    <?php } ?>
    </tbody>
</table>
<?php Controller::addJs('api'); ?>
<?php Controller::beginJsBlock(); ?>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        var setMailInputs = document.getElementsByClassName("setMail");
        var setBanInputs = document.getElementsByClassName("setBan");
        var setAdminInputs = document.getElementsByClassName("setAdmin");
        var deleteInputs = document.getElementsByClassName("delete");
        var showUsers = document.getElementsByClassName("showUser");

        var onSuccess = function (data) {

        };

        for (var numberOfUser = 0; numberOfUser < showUsers.length; numberOfUser++) {
            var setMailInput = setMailInputs[numberOfUser];
            var setBanInput = setBanInputs[numberOfUser];
            var setAdminInput = setAdminInputs[numberOfUser];
            var deleteInput = deleteInputs[numberOfUser];
            var showUser = showUsers[numberOfUser];

            showUser.onclick = function (evt) {
                var target = evt.target || evt.srcElement;
                var id = utils.getOfferId(target);
                window.location = "<?php echo Router::generateUrl("admin","user-bookings"); ?>/" + id;
            };

            setMailInput.onclick = function (evt) {
                evt.preventDefault();
                var target = evt.target || evt.srcElement;
                var id = target.dataset.id;
                api.get("<?php echo Router::generateUrl("admin","update-user",array("mail")); ?>" + "/" + id, {}, onSuccess);
            };

            setBanInput.onclick = function (evt) {
                evt.preventDefault();
                var target = evt.target || evt.srcElement;
                var id = target.dataset.id;
                api.get("<?php echo Router::generateUrl("admin","update-user",array("banish")); ?>" + "/" + id, {}, onSuccess);
            };

            setAdminInput.onclick = function (evt) {
                evt.preventDefault();
                var target = evt.target || evt.srcElement;
                var id = target.dataset.id;
                api.get("<?php echo Router::generateUrl("admin","update-user",array("admin")); ?>" + "/" + id, {}, onSuccess);
            };

            deleteInput.onclick = function (evt) {
                evt.preventDefault();
                var target = evt.target || evt.srcElement;
                var id = target.dataset.id;
                if (confirm("voulez vous vraiment supprimer l'entrée " + id + " ? Cette action entrainera la suppression des offres, des transactions et des posts de l'utilisateur.")) {
                    window.location = '<?php echo Router::generateUrl("admin","delete-user") ?>' + '/' + id;
                }
            }

        }
    });

</script>
<?php Controller::endJsBlock(); ?>