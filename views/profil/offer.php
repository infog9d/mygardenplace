<?php Controller::addCss("offer.css")?>

<div class="panel bordur table-<?php echo $params["tileSize"]; ?> offer" data-id="<?php echo $params["ad"]["id"]; ?>">
    <div class="imag annonce imgbord bord" style="background: url('<?php echo ($params["ad"]["picture"] != "" && file_exists(WEBROOT."/assets/uploads/".$params["ad"]["picture"]))?BASE_URL."/assets/uploads/".$params["ad"]["picture"]:BASE_URL."/assets/media/pomme_mystere.png"; ?>');">

    </div>
    <!--<img class="imag annonce bord" src="<?php /*echo ($params["ad"]["picture"] != "" && file_exists(WEBROOT."/assets/uploads/".$params["ad"]["picture"]))?BASE_URL."/assets/uploads/".$params["ad"]["picture"]:BASE_URL."/assets/media/pomme_mystere.png"; */?>" alt=""/>-->
    <div class="table bord">
        <div class="table-2 pad10">
                <span class="pad5 round">
                    <img id="image<?php echo $params["ad"]["id"]; ?>" data-id="<?php echo $params["ad"]["id"]; ?>" class="imagi" src="<?php echo ($params["ad"]["accountPicture"] != "")?BASE_URL."/assets/uploads/".$params["ad"]["accountPicture"]:BASE_URL."/assets/media/unknown.png"; ?>" alt=""/>
                </span>
        </div>
        <div class="table-8 coord middle">
                <div class="name" id="coord<?php echo $params["ad"]["id"]; ?>">
                    <?php echo $params["ad"]["firstname"]; ?> <?php echo $params["ad"]["lastname"];?> vous vend les <?php echo $params["ad"]["category"]; ?> de son jardin <br>
                </div>

        </div>
        <div class="cadd" id="caddie<?php echo $params["ad"]["id"]; ?>">
            <img src="<?php echo BASE_URL."/assets/media/caddie.PNG" ?>" alt="" class="caddie" data-id="<?php echo $params["ad"]["id"]; ?>"/>
        </div>
        <div class="prixeuro" id="prix<?php print_r($params["ad"]["id"]);?>"><?php if($params["ad"]["price"] == 0) { echo "en échange"; } else { echo $params["ad"]["price"];?> €/Kg<?php } ?></div>

        <div id="quantity<?php echo $params["ad"]["id"]; ?>" class=" quantite middle txtcenter">
            <span class="quantity"><?php echo $params["ad"]["quantity"]; ?></span><br>
            <span class="labelQuantity">Kg</span>

        </div>
    </div>
</div>


    <script type="text/javascript">
        var viewOffer = "<?php echo Router::generateUrl("offers","view") ?>/";
        document.addEventListener("DOMContentLoaded", function (evt) {
            var image = document.getElementById("image<?php echo $params["ad"]["id"]; ?>");
        //    image.onload = function (ev) {
       //         var image = document.getElementById("image<?php echo $params["ad"]["id"]; ?>");
                var quantity = document.getElementById("quantity"+image.dataset.id);
                quantity.style.position = "relative";
            console.log(image.naturalHeight-75);
                quantity.style.top = - (134+286-20) +"px";
            var caddie = document.getElementById("caddie<?php echo $params["ad"]["id"]; ?>");
            caddie.style.position = "relative";
            caddie.style.top = - (10) +"px";
            var prixeuro = document.getElementById("prix<?php echo $params["ad"]["id"]; ?>");
            prixeuro.style.position = "relative";
            prixeuro.style.top = - (160) +"px";
            var coord = document.getElementById("coord<?php echo $params["ad"]["id"]; ?>");
            coord.style.position = "relative";
            coord.style.top = - (50) +"px";
          //  };
        });
    </script>

<?php Controller::requireMethod("offers","addToChart"); ?>
<?php Controller::addJs("tileView"); ?>