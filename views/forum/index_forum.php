<?php Controller::addCss("forum.css"); ?>
<?php Controller::requireMethod("utils","breadcrumbs",array(
        Router::generateUrl("home") => "Accueil",
        Router::generateUrl("index_forum")=> "Forum",
    ));?>

<div id="index-forum" class="table">

    <div id="name-forum">
        <h1>Forum Mygardenplace</h1>

    </div>


    <div id="list-forum">


        <?php /*echo Session::get("user")["firstname"]." ".Session::get("user")["lastname"];*/ ?>


        <table>
            <thead>
            <tr>
                <th scope="col">Choisissez votre forum</th>
                <th scope="col">Sujets</th>
                <th scope="col">Messages</th>
                <th scope="col">Derniers messages</th>
            </tr>
            </thead>
            <tbody>

    <?php foreach($params["topics"] as $topic) {?>

    <tr>
                <td>

                    <strong class="topics"> <a href="<?php echo Router::generateUrl("forum","post",array("id" => $topic["idtopics"]));?>"> <?php echo $topic["name"]; ?></a></strong></td>

                <td class="number-post"><?php Controller::requireMethod('forum','getNumberOfMessagesByTopics',array('id'=>$topic['idtopics'])); ?></td>
                <td class="number-response"><?php Controller::requireMethod('forum','getNumberOfResponsesByTopics',array('id'=>$topic['idtopics'])); ?></td>
                <td class="last-response"><?php Controller::requireMethod('forum','getLastResponseByTopics', array('id'=>$topic['idtopics']));?></td>
        <?php
        $user = Session::get("user");
        if ($user["is_admin"]) {
        ?>
                <td>
                    <form action="<?php echo Router::generateUrl("forum","delete-topic",array($topic["idtopics"])) ?>">
                        <input type="submit" value="Supprimer">
                    </form>
                </td>
        <?php } ?>
            </tr>
            <?php } ?>

            </tbody>

        </table>

    </div>
    <div id="pagination-forum">

    </div>

<?php
$user = Session::get("user");
if ($user["is_admin"]) {
    ?>
    <form action="<?php echo Router::generateUrl("forum","add-topic") ?>" method="post" style="margin: 50px;display: block;">
        Ajouter des topics :
        <div class="table">
            <div class="table-6">
                <label>
                    Nom du topic :
                    <input type="text" name="topic"/>
                </label>
            </div>
            <div class="table-6">
                <input type="submit" value="Valider" style="width: 100px;float: none;"/>
            </div>
        </div>
    </form>
<?php
} ?>

</div>

