<?php Controller::addCss("forum.css"); ?>


<div id="post-forum" class="table">

    <div id="name-forum">
        <h1>Forum Mygardenplace</h1>

    </div>

    <div id="topic-name">

        <h2><?php echo $params['topic']['name']; ?></h2>

    </div>

    <div id="list-post">
    <table>

        <thead>
        <tr>
            <th scope="col">Auteur</th>
            <th scope="col">Discussion</th>
            <th scope="col">Réponses</th>
            <th scope="col">Derniers messages</th>
            <?php
            $user = Session::get("user");
            if ($user["is_admin"]) { ?>
                <th scope="col">Bannir</th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>



            <?php foreach ($params["posts"] as $posts) {
                ?>
        <tr>
            <td class="number-response">
                <a href="<?php echo Router::generateUrl("profileUser","index",array("id"=> $posts["user_id"]));?>"><?php echo $posts["firstname"]." ".$posts["lastname"];?></a>
            <span><?php echo Controller::requireMethod("forum", "getPictureUser",array("id"=> $posts["id"]))?></span>

            </td>

            <td>
                <strong class="topics"><a href="<?php echo Router::generateUrl("forum","response",array("id"=> $posts["id"]));?>">
                        <?php echo $posts["title"]; ?></a></strong>
            </td>
            <td class="number-post"><?php echo Controller::requireMethod("forum", "getNumberOfResponsesByPosts",array("id"=> $posts["id"]))?></td>
            <td class="last-response"><?php Controller::requireMethod("forum","getLastResponseByPosts", array("id"=> $posts["id"])); ?> </td>
            <?php if ($user["is_admin"]) { ?>
                <td><input class="setBan" data-id="<?php echo $posts["id"] ?>" type="checkbox" <?php if ($posts["valid"] == 0) { ?>checked value="1"<?php } ?>></td>
                <td>
                    <form action="<?php echo Router::generateUrl("forum","delete-post",array($posts["id"])) ?>">
                        <input type="submit" value="Supprimer">
                    </form>
                </td>
            <?php } ?>
        </tr>
            <?php }?>


        </tbody>

    </table>
    </div>
    <div id="pagination-post">
        <div id="post">



            <script language="javascript" type="text/javascript">
                var current = 0;
                function change(what)
                {
                    document.getElementById("show").src = what.value;
                }
            </script>

            <div>


                                                    
                                                    
                                                        
                <form action="<?php echo Router::generateUrl("forum","insertPost") ?>" method="post" enctype="multipart/form-data" id="champ_cache"><br/><br/>
                    <label for="title" onchange="change(this);">Titre:</label>
                    <input type="text" name="title" /><br /><br/>
                    <input type="hidden" name="topicId" value="<?php echo $params["topic"]["idtopics"]; ?>"
                    <label for="content" onchange="change(this);">Votre question:</label>
                    <textarea name="message" cols="15" rows="9"></textarea><br/><br/>
                    <input type="submit" value="postez" id="non" onclick="cacher();" checked="checked"/>
                </form>


                                                            <p id="champ_visible">
                        <input type="button" name="nom" value= "Nouveau sujet de discussion" id="oui" onClick="afficher();" />
                                                            </p>
                                                     
                                                      <script type="text/javascript">
                        document.getElementById("champ_cache").style.display = "none";
                        document.getElementById("champ_visible").style.display = "block";

                        function afficher()
                        {
                            document.getElementById("champ_cache").style.display = "block";
                            document.getElementById("champ_visible").style.display = "none";
                        }

                        function cacher()
                        {
                            document.getElementById("champ_cache").style.display = "none";
                            document.getElementById("champ_visible").style.display = "block";
                        }
                    </script>
                                                        </div>
                                                



        </div>
        <div id="pagination-2"
    </div>



</div>
</div>

<?php if ($user["is_admin"]) { ?>
    <?php Controller::beginJsBlock(); ?>
    <script type="application/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            var setBanInputs = document.getElementsByClassName("setBan");

            var onSuccess = function (data) {

            };

            for (var numberOfUser = 0; numberOfUser < setBanInputs.length; numberOfUser++) {
                var setBanInput = setBanInputs[numberOfUser];
                setBanInput.onclick = function (evt) {
                    //evt.preventDefault();
                    var target = evt.target || evt.srcElement;
                    var id = target.dataset.id;
                    api.get("<?php echo Router::generateUrl("forum","ban-post"); ?>" + "/" + id, {}, onSuccess);
                };
            }

        });
    </script>
    <?php Controller::endJsBlock(); ?>
<?php } ?>