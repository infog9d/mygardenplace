<?php Controller::addCss("forum.css"); ?>
 <div id="response" class="table">




     <div id="name-forum2">
         <h1>Forum Mygardenplace</h1>

     </div>


     <div id="post-name2">
         <h2><?php echo $params['post']['title']; ?></h2>
         <h3><?php echo $params['post']['content']; ?></h3>

     </div>

     <div id="list-response">
         <table>

             <thead>
             <tr>
                 <th scope="col">Auteur</th>
                 <th scope="col">Message</th>
                 <th scope="col">Date</th>
                 <?php
                 $user = Session::get("user");
                 if ($user["is_admin"]) { ?>
                     <th scope="col">Bannir</th>
                 <?php } ?>
             </tr>
             </thead>
             <tbody>



             <?php foreach ($params["response"] as $responses) {
                 ?>
                 <tr>

                     <td>
                         <strong class="topics"><a href="<?php echo Router::generateUrl("profileUser","index",array("id"=> $responses["user_id"]));?>">
                                 <?php echo $responses["firstname"]." ".$responses["lastname"];?></a></strong>
                         <span class="date">Posté le <?php echo $responses["posted_on"]; ?></span>

                     </td>
                     <td class="number-response"><?php echo $responses["content"];?>
                     </td>
                     <td class="number-post"><span class="date">Posté le <?php echo $responses["posted_on"]; ?></span> </td>
                     <?php if ($user["is_admin"]) { ?>
                         <td><input class="setBan" data-id="<?php echo $responses["id"] ?>" type="checkbox" <?php if ($responses["valid"] == 0) { ?>checked value="1"<?php } ?>></td>
                         <td>
                             <form action="<?php echo Router::generateUrl("forum","delete-response",array($responses["id"])) ?>">
                                 <input type="submit" value="Supprimer">
                             </form>
                         </td>
                     <?php } ?>
                 </tr>
             <?php }?>


             </tbody>

         </table>
     </div>
     <div id="pagination-response">
         <div id="response">


             <script language="javascript" type="text/javascript">
                 var current = 0;
                 function change(what)
                 {
                     document.getElementById("show").src = what.value;
                 }
             </script>

             <div>


                                                     
                             
                 <form action="<?php echo Router::generateUrl("forum", "insert-responses") ?>" method="post" enctype="multipart/form-data" id="champ_cache">
                     <input type="hidden" name="postId" value="<?php echo $params["post"]["id"]; ?>">

                     <textarea name="msg" placeholder="Votre message..." required="required"></textarea>
                     <input type="submit" value="Ajouter un message" id="non" onclick="cacher(); " checked="checked"/>
                 </form>                        
                                                     

                                                     <p id="champ_visible">
                     <input type="button" name="nom" value= "Répondre" id="oui" onClick="afficher();" />
                                                         </p>
                                                  
                                                   <script type="text/javascript">
                     document.getElementById("champ_cache").style.display = "none";
                     document.getElementById("champ_visible").style.display = "block";

                     function afficher()
                     {
                         document.getElementById("champ_cache").style.display = "block";
                         document.getElementById("champ_visible").style.display = "none";
                     }

                     function cacher()
                     {
                         document.getElementById("champ_cache").style.display = "none";
                         document.getElementById("champ_visible").style.display = "block";
                     }
                 </script>
                                                     </div>
                                             

         </div>
         <div id="pagination-2"
     </div>


</div>
<?php if ($user["is_admin"]) { ?>
    <?php Controller::beginJsBlock(); ?>
    <script type="application/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            var setBanInputs = document.getElementsByClassName("setBan");

            var onSuccess = function (data) {

            };

            for (var numberOfUser = 0; numberOfUser < setBanInputs.length; numberOfUser++) {
                var setBanInput = setBanInputs[numberOfUser];
                setBanInput.onclick = function (evt) {
                    //evt.preventDefault();
                    var target = evt.target || evt.srcElement;
                    var id = target.dataset.id;
                    api.get("<?php echo Router::generateUrl("forum","ban-responses"); ?>" + "/" + id, {}, onSuccess);
                };
            }

        });
    </script>
    <?php Controller::endJsBlock(); ?>
<?php } ?>