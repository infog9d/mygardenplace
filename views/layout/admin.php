<!DOCTYPE html>
<html>
<head lang="fr">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>mygardenplace.com - <?php echo $title;?></title>
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/css/layout.css" />
    <?php echo Controller::requireCss("modal.css"); ?>
    <?php
    echo $css;
    ?>
</head>
<body>
<div id="header" class="table">
    <div class="table-3">
        <div id="logo" class="table">
            <div class="table-2 logoContainer">
                <img src="<?php echo BASE_URL; ?>/assets/media/logo.png" alt="" title="" class="imag" border="0" />
            </div>
            <h1 class="table-10 middle">My GardenPlace<br>
                <span style="
                    font-size: 13px;
                    text-align: center;
                    display: block;
                ">ADMINISTRATION</span>
            </h1>
        </div>
    </div>
    <div class="table-7 middle">
        <div id="menu">
            <ul class="nav">
                <li><a href="<?php echo Router::generateUrl("home"); ?>">Accueil</a> </li>

                <li><a href="<?php echo Router::generateUrl("admin","users"); ?>">Gestion des utilisateurs</a> </li>

                <li><a href="<?php echo Router::generateUrl("admin","categories"); ?>">Gestion des categories</a></li>
                <li><a href="<?php echo Router::generateUrl("admin","subcategories"); ?>">Gestion des sous-categories</a></li>

                <li class="last"><a href="<?php echo Router::generateUrl("admin","faq"); ?>">Gestion de la FAQ</a></li>

            </ul>

        </div>
    </div>
    <div class="table-2 middle">
        <div id="inscription">
            <ul class="nav floatright">
                <?php if (Session::has("user")) { ?>
                    <li class="resetRight"><a href="<?php echo Router::generateUrl("user","account"); ?>">My garden</a></li>
                    <li class="last resetRight"><a href="<?php echo Router::generateUrl("user","logout") ?>">Se deconnecter</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<div class="content">
    <?php
    echo $content;
    ?>
</div>
<div id="footer">
    <p>Copyright 2015 G9D - <a href="#">Mentions legales</a></p>
</div>
<?php echo Controller::requireJs("modal.js"); ?>
<?php echo Controller::requireJs("utils.js"); ?>
<div id="overlay"></div>
<?php echo $js.$blocks; ?>
</body>
</html>
