<!DOCTYPE html>
<html>
    <head lang="fr">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>mygardenplace.com - <?php echo $title;?></title>
        <link rel="stylesheet" href="<?php echo BASE_URL; ?>/assets/css/layout.css" />
        <link rel="icon" href="<?php echo BASE_URL; ?>/assets/media/logo.png">
        <?php echo Controller::requireCss("modal.css"); ?>
        <?php
        echo $css;
        ?>
    </head>
    <body>
    <div id="header" class="table">
        <div class="table-3">
        <div id="logo" class="table">
            <div class="table-2 logoContainer">
                <img src="<?php echo BASE_URL; ?>/assets/media/logo.png" alt="" title="" class="imag" border="0" />
            </div>
            <h1 class="table-10 middle">My GardenPlace</h1>
        </div>
        </div>
        <div class="table-5 middle">
        <div id="menu">
            <ul class="nav">
                <li><a href="<?php echo Router::generateUrl("home"); ?>">Accueil</a> </li>

                <li><a href="<?php echo Router::generateUrl("whoiam"); ?>">Qui sommes-nous?</a> </li>

                <li><a href="<?php echo Router::generateUrl("forum"); ?>">Forum</a> </li>


                <li><a href="<?php echo Router::generateUrl("faq"); ?>">FAQ</a> </li>
                <li class="last"><a href="<?php echo Router::generateUrl("contact") ?>">Contactez-nous</a> </li>
            </ul>

        </div>
        </div>
        <div class="table-4 middle">
        <div id="inscription">
            <ul class="nav floatright">
                <?php if (Session::has("user")) { ?>
                <li class="resetRight"><a href="<?php echo Router::generateUrl("user","account"); ?>">My garden</a></li>
                <li class="resetRight"><a href="<?php echo Router::generateUrl("shoppingchart"); ?>">Mon panier (<?php Controller::requireMethod("shoppingchart","countOffer") ?>)</a></li>
                <li class="last resetRight"><a href="<?php echo Router::generateUrl("user","logout") ?>">Se deconnecter</a></li>
                <?php } else { ?>
                <li class="resetRight"><a href="#" class="registerAction">S'inscrire</a></li>
                <li class="last resetRight"><a href="#" class="connectionAction">Se connecter</a></li>
                <?php } ?>
            </ul>
        </div>
        </div>
    </div>
    <div class="content">
        <?php
        echo $content;
        ?>
    </div>
    <div id="footer">
        <p>Copyright 2015 G9D - <a href="#">Mentions legales</a></p>
    </div>
    <?php echo Controller::requireJs("modal.js"); ?>
    <?php echo Controller::requireJs("api.js"); ?>
    <?php echo Controller::requireJs("utils.js"); ?>
    <div id="overlay"></div>
    <?php if (!Session::has("user")) { ?>
        <div id="registration" class="modal">
            <div class="head">S'inscrire</div>
            <hr>
            <?php
            $hasRegistrationSuccess = false;
            $hasRegistrationError = false;
            if (Session::hasFlashbag("registrationSuccess")) {
                $hasRegistrationSuccess = true;
                echo "<span class='alert success'>".Session::getFlashbag("registrationSuccess")."</span><br>";
            } else if (Session::hasFlashbag("registrationError")) {
                $hasRegistrationError = true;
                echo "<span class='alert error'>".Session::getFlashbag("registrationError")."</span><br>";
            } ?>
            <?php Controller::requireMethod("user", "registrationForm"); ?>
            <hr>
            Vous avez déjà un compte ? <a href="#" class="connectionAction">Connectez-vous</a>
        </div>
        <div id="connection" class="modal">
            <div class="head">Se connecter</div>
            <hr>
            <?php
            $hasConnectionError = false;
            if (Session::hasFlashbag("connectionError")) {
                $hasConnectionError = true;
                echo "<span class='alert error'>".Session::getFlashbag("connectionError")."</span><br>";
            } ?>
            <?php Controller::requireMethod("user", "connectionForm"); ?>
            <hr>
            Vous n'êtes pas encore inscrit ? <a href="#" class="registerAction">Inscrivez-vous</a>
        </div>
        <script type="text/javascript">
            document.addEventListener("DOMContentLoaded", function(event) {
                modal.init("registration");
                modal.init("connection");
                <?php if ($hasRegistrationError||$hasRegistrationSuccess) { ?>
                modal.show("registration");
                <?php } else if ($hasConnectionError) {?>
                modal.show("connection");
                <?php } ?>
                var elementsRegisterAction = document.getElementsByClassName("registerAction");
                var elementsConnectionAction = document.getElementsByClassName("connectionAction");
                for (var i = 0; i < elementsConnectionAction.length; i++) {
                    elementsConnectionAction[i].onclick = function() {
                        modal.hideAll();
                        modal.toggle("connection");
                    }
                }
                for (i = 0; i < elementsRegisterAction.length; i++) {
                    elementsRegisterAction[i].onclick = function() {
                        modal.hideAll();
                        modal.toggle("registration");
                    }
                }
            });
        </script>
    <?php }?>
    <?php echo $js.$blocks; ?>
    </body>
</html>
