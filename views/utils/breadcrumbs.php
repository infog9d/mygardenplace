<div id="breadcrumbs">
    <ul class="root">
        <?php $index = 0; ?>
        <?php foreach ($params as $link => $name){ ?>
            <?php $index++; ?>
            <li><?php if ($index<count($params)) { ?><a href= "<?php echo $link ?>"><?php echo $name ?></a><?php } else { ?><?php echo $name ?><?php } ?><?php if ($index<count($params)) { ?><span class="arrow"> > </span><?php } ?></li>
        <?php } ?>
    </ul>
</div>