<?php Controller::addCss("contact.css"); ?>
<?php Controller::requireMethod("utils","breadcrumbs",array(
    Router::generateUrl("home") => "Accueil",
    Router::generateUrl("contact")=> "Contactez-nous",
)); ?>

<div class="container">
    <h1 class="title">Contact</h1>
    <p class="subtitle">Votre avis nous intéresse ! Vous rencontrez un problème ?</p><br/>


    <?php
    /** @var Recaptcha $captcha */
    $captcha= $params["captcha"];
    ?>

    <form id="myform" action="<?php echo Router::generateUrl("contact","send") ?>" method="post">
        <fieldset>

            <legend>Contact</legend>

            <?php if(Session::hasFlashbag("contactError")) {
                echo "<span class='error'>".Session::getFlashbag("contactError")."</span>";
            } ?>

            <?php if(Session::hasFlashbag("contactSuccess")) {
                echo "<span class='success'>".Session::getFlashbag("contactSuccess")."</span>";
            } ?>



            <label for="name">Nom</label>
            <input id="name" type="text" name="name" placeholder="Doe" value="<?php if(Session::has("user")){ $user = Session::get("user");echo $user["firstname"];} ?>"/ ><br/>



            <label for="mail">E-mail</label>
            <input id="mail" type="mail" name="mail" placeholder="John.doe@example.com" value="<?php if(Session::has("user")){ $user = Session::get("user");echo $user["mail"];} ?>"/ ><br/>



            <label for="message">Message</label>
            <textarea name="message" id="message" placeholder="Message" ></textarea><br/>


            <?php echo( $captcha->html()) ;?>


            <input type="submit" name="submit"/>



        </fieldset>
        <?php echo $captcha->script() ;?>

    </form>
</div>

<?php
/*
 * Insérer du code PHP
 */

?>
